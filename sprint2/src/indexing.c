#include <string.h>
#include <wchar.h>
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <stdint.h>
#include <sys/stat.h>
#include <stdint.h>
#include <unistd.h>

#include "includes/util.h"

#include "includes/image_types.h"
#include "includes/sound_types.h"
#include "includes/text_types.h"

#include "includes/indexing/image_indexing.h"
#include "includes/indexing/sound_indexing.h"
#include "includes/indexing/text_indexing.h"

static wchar_t* IMAGES_BASE_FILENAME=(wchar_t*) L"img_base.txt";
static wchar_t* SOUNDS_BASE_FILENAME=(wchar_t*)  L"snd_base.txt";
static wchar_t* TEXTS_BASE_FILENAME=(wchar_t*)  L"txt_base.txt";

static wchar_t* IMAGES_ASSOCIATION_FILENAME=(wchar_t*)  L"img_association.txt";
static wchar_t* SOUNDS_ASSOCIATION_FILENAME=(wchar_t*)  L"snd_association.txt";
static wchar_t* TEXTS_ASSOCIATION_FILENAME=(wchar_t*)  L"txt_association.txt";




static dictionnary_t IMAGES_ASSOCIATION;
static dictionnary_t SOUNDS_ASSOCIATION;
static dictionnary_t TEXTS_ASSOCIATION;

static linked_list_t IMAGES_BASE;
static linked_list_t SOUNDS_BASE;
static linked_list_t TEXTS_BASE;

int open_file_in_index(wchar_t* index_path, wchar_t* filename, char* mode, FILE** output_fp){
	wchar_t tmp_filename_wchar[1024];
	char tmp_filename_char[1024];

	swprintf(tmp_filename_wchar, 1024, L"%ls/%ls", index_path, filename);
	wstring_to_string(tmp_filename_wchar, tmp_filename_char);
	// Ouverture de la liste des fichiers à indexer en lecture seule

	*output_fp = fopen(tmp_filename_char, mode);
	wprintf(L"FP=%d\n\n", *output_fp);

	if(*output_fp == NULL){
		wprintf(L"Erreur d'ouverture du fichier %ls\n", tmp_filename_wchar);
		return ERROR_FILE_OPEN;
	}
	return 0;
}



int load_index(wchar_t* index_path){
	int error;
	int found;

	wchar_t filename_wchar[1024];
	char filename_char[1024];


	descriptor_id_t document_id=(wchar_t*) malloc(DESCRIPTOR_ID_LENGTH*sizeof(wchar_t));

	dictionnary_init(&IMAGES_ASSOCIATION);
	dictionnary_init(&SOUNDS_ASSOCIATION);
	dictionnary_init(&TEXTS_ASSOCIATION);



	filename_wchar[0]=L'\0';
	swprintf(filename_wchar, 1024, L"%ls/%ls", index_path, SOUNDS_ASSOCIATION_FILENAME);
	wstring_to_string(filename_wchar, filename_char);
	dictionnary_load(&SOUNDS_ASSOCIATION, filename_char);

	wprintf(L"Indexed sounds : \n", index_path);
	document_id[0]=L'\0';
	filename_wchar[0]=L'\0';
	for(size_t n=0; n<SOUNDS_ASSOCIATION.size; n++){
		error=dictionnary_get_association(&SOUNDS_ASSOCIATION, n, document_id, filename_wchar, &found);
		if(!error && found){
			wprintf(L"%ls=\"%ls\"\n", document_id, filename_wchar);
		}
	}



	return 0;
}

int start_indexing(wchar_t* index_path, char* liste_fichiers){
	int error;
	int found;
	FILE* document_list_fp;
	FILE* document_fp;
	FILE* base_fp;
	FILE* association_fp;

	wchar_t filename_wchar[1024];
	char filename_char[1024];

	wchar_t* file_extension;

	descriptor_id_t document_id;

	sound_descriptor_t* sound_descriptor;

	dictionnary_init(&IMAGES_ASSOCIATION);
	dictionnary_init(&SOUNDS_ASSOCIATION);
	dictionnary_init(&TEXTS_ASSOCIATION);

	// Ouverture de la liste des fichiers à indexer en lecture seule
	document_list_fp = fopen(liste_fichiers,"r");
	if(document_list_fp == NULL){
		perror("Erreur d'ouverture de la liste des fichiers à indexer.\n");
		return ERROR_FILE_OPEN;
	}


	wstring_to_string(index_path, filename_char);
	mkdir(filename_char, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);


	error=open_file_in_index(index_path, SOUNDS_BASE_FILENAME, (char*)"w", &base_fp);
	if(error!=0){
		return error;
	}

	error=open_file_in_index(index_path, SOUNDS_ASSOCIATION_FILENAME, (char*)"w", &association_fp);
	if(error!=0){
		return error;
	}




	// Parcours des paramètres du fichier
	while (fwscanf(document_list_fp, L"%l[^\n]\n", filename_wchar) == 1){
		file_extension = wcsrchr(filename_wchar, '.')+1;

		if(wcscmp(file_extension, L"txt")==0 || wcscmp(file_extension, L"TXT")==0 ){
			generate_descriptor_id((wchar_t*)L"img", &document_id);
			dictionnary_add(&IMAGES_ASSOCIATION, document_id, filename_wchar);

		} else if(wcscmp(file_extension, L"bin")==0 || wcscmp(file_extension, L"BIN")==0 ){


			wstring_to_string(filename_wchar, filename_char);
			document_fp = fopen(filename_char,"r");
			if(document_fp == NULL){
				perror("Erreur d'ouverture du document.\n");
				return ERROR_FILE_OPEN;
			}

			/* Problème d'allocation, obligeant d'allouer plus que nécessaire, la mémoire se corrompt lors d'écritures dans le descripteur.
			Allouer plus que nécessaire règle le problème, il faudra chercher une solution

			Trace Valgrind:

			valgrind: m_mallocfree.c:304 (get_bszB_as_is): Assertion 'bszB_lo == bszB_hi' failed.
			valgrind: Heap block lo/hi size mismatch: lo = 80, hi = 16184.
			This is probably caused by your program erroneously writing past the
			end of a heap block and corrupting heap metadata.  If you fix any
			invalid writes reported by Memcheck, this assertion failure will
			probably go away.  Please try that before reporting this as a bug.

			host stacktrace:
			==396==    at 0x380A48EF: show_sched_status_wrk (m_libcassert.c:319)
			==396==    by 0x380A49E4: report_and_quit (m_libcassert.c:390)
			==396==    by 0x380A4B66: vgPlain_assert_fail (m_libcassert.c:455)
			==396==    by 0x380B170D: get_bszB_as_is (m_mallocfree.c:302)
			==396==    by 0x380B170D: get_bszB (m_mallocfree.c:312)
			==396==    by 0x380B170D: get_pszB (m_mallocfree.c:386)
			==396==    by 0x380B170D: vgPlain_describe_arena_addr (m_mallocfree.c:1532)
			==396==    by 0x3809DC93: vgPlain_describe_addr (m_addrinfo.c:188)
			==396==    by 0x3809C73B: vgMemCheck_update_Error_extra (mc_errors.c:1133)
			==396==    by 0x380A05BA: vgPlain_maybe_record_error (m_errormgr.c:818)
			==396==    by 0x3809BCB2: vgMemCheck_record_address_error (mc_errors.c:753)
			==396==    by 0x803EECCE8: ???
			==396==    by 0x802D95EEF: ???
			==396==    by 0x38072A6F: ??? (mc_malloc_wrappers.c:418)
			==396==    by 0x401959: sound_descriptor_init (sound_types.c:34)
			*/
			sound_descriptor=(sound_descriptor_t*) malloc(1024+sizeof(sound_descriptor));


			error=sound_descriptor_init(sound_descriptor);

			if(error!=0){
				wprintf(L"Error while initialising descriptor for %ls\n", filename_wchar);
				return error;
			}
			sound_descriptor_creation(document_fp, sound_descriptor);

			error=dictionnary_add(&SOUNDS_ASSOCIATION, sound_descriptor->id, filename_wchar);
			error=list_add(&SOUNDS_BASE, sound_descriptor);
			fclose(document_fp);


			wprintf(L"Writing descriptor to disk for %ls (%ls)\n", filename_wchar, sound_descriptor->id);

			sound_descriptor_to_file(base_fp, sound_descriptor);
			fwprintf(association_fp, L"%ls=%ls\n", sound_descriptor->id, filename_wchar);

			sound_descriptor_free(sound_descriptor);


		} else if(wcscmp(file_extension, L"xml")==0 || wcscmp(file_extension, L"XML")==0 ){
			generate_descriptor_id((wchar_t*)L"txt", &document_id);
			dictionnary_add(&TEXTS_ASSOCIATION, document_id, filename_wchar);

		} else {
			wprintf(L"Unknown association : %ls\n", file_extension);
		}
	}
	fclose(document_list_fp);
	fclose(base_fp);
	fclose(association_fp);




	return 0;
}


void usage(int argc, char* argv[]){
	wprintf(L"Usage : \n");
	wprintf(L"\t\"%s config_file documents_list_file\"\n", argv[0]);
}


int main(int argc, char* argv[]){
	setlocale(LC_CTYPE, "fr_FR.UTF-8");

	if(argc!=3){
		usage(argc, argv);
		return ERROR_ARGC;
	}

	int error;
	int found;
	wchar_t index_path[1024];


	wprintf(L"Chargement du fichier de configuration\n");
	error = config_load(argv[1]);
	if(error!=0){
		return error;
	}

	error=config_get((wchar_t*)L"index", index_path, &found);
	if(error){
		wprintf(L"Error while looking for index path\n");
		return error;
	}else if(!found){
		wprintf(L"Index setting not found\n");
		return ERROR_PARAMETER_NOT_FOUND;
	}

	/* On ignore le chargement d'un index existant pour l'instant
	wprintf(L"Chargement de l'index\n");
	load_index(index_path);
	wprintf(L"Chargement de l'index terminé\n");
	*/

	wprintf(L"Démarrage de l'indexation\n");
	start_indexing(index_path, argv[2]);
	wprintf(L"Indexation terminée\n");

	return 0;
}
