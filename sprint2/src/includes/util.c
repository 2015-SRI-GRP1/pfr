#include <stdio.h>
#include <wchar.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"


int AVAILABLE_DOCUMENT_ID=0;

dictionnary_t CONFIGURATION;

/**
* \struct association_t util.h
* \brief Structure permettant d'associer une chaîne de caractères à une autre
*/
typedef struct association_t{
	wchar_t* key;
	wchar_t* value;
} association_t;

int list_init(linked_list_t* list, size_t data_size){
	list->size=0;
	list->data_size=data_size;
	list->head=NULL;
	list->tail=NULL;
	return 0;
}

int list_init_with_size(linked_list_t* list, size_t data_size, size_t size){
	list_init(list, data_size);
	while(list->size != size){
		list_add(list, NULL);
	}

	return 0;
}



int list_add(linked_list_t* list, void* value){

	list_node_t* node = (list_node_t*) malloc(sizeof(list_node_t));
	node->data=value;
	node->next=NULL;

	if(list->tail==NULL){
		list->head=node;
		list->tail=node;
		list->size++;
	} else{
		list->tail->next=node;
		list->tail=node;
		list->size++;
	}
	return 0;
}


int list_get(linked_list_t* list, size_t index, void* value, int* found){
	int n=index;
	list_node_t* node = list->head;

	// Recherche de valeurs de la liste
	while(node!=NULL && n>0){
		node=node->next;
		n--;
	}
	// Vérification du succès de la recherche
	if(node!=NULL){
		memcpy(value, node->data, list->data_size);
		*found=1;
	} else{
		*found=0;
	}
	return 0;
}

int list_free(linked_list_t* list){
	list_node_t* node = list->head;
	list_node_t* tmp_node;

	// Recherche de valeurs de la liste
	while(node!=NULL){
		tmp_node=node;
		node=tmp_node->next;
		free(tmp_node->data);
		free(tmp_node);
	}
	return 0;
}



int dictionnary_init(dictionnary_t* dic){
	list_init(dic, sizeof(association_t));

	return 0;
}



int dictionnary_add(dictionnary_t* dic, wchar_t* key, wchar_t* value){
	association_t* association = (association_t*) malloc(10*sizeof(association_t));
	association->key = wcsdup(key);
	association->value = wcsdup(value);

	list_add(dic, association);


	return 0;
}


int dictionnary_get(dictionnary_t* dic, wchar_t* key, wchar_t* value, int* found){
	list_node_t* node = dic->head;

	// Recherche du paramètre jusqu'à épuisement de la liste des paramètres
	while(node!=NULL && wcscmp(key, ((association_t*)node->data)->key)!=0){
		node=node->next;
	}

	// Vérification du succès de la recherche
	if(node!=NULL){
		// Copie du resultat dans le tampon du resultat
		wcscpy(value, ((association_t*)node->data)->value );
		*found=1;
	} else{
		*found=0;
	}
	return 0;
}

int dictionnary_get_association(dictionnary_t* dic, int index, wchar_t* key, wchar_t* value, int* found){
	int n=index;
	list_node_t* node = dic->head;

	// Recherche de valeurs de la liste
	while(node!=NULL && n>0){
		node=node->next;
		n--;
	}



	// Vérification du succès de la recherche
	if(node!=NULL){
		// Copie du resultat dans le tampon du resultat
		wcscpy(key, ((association_t*)node->data)->key );
		wcscpy(value, ((association_t*)node->data)->value );
		*found=1;
	} else{
		*found=0;
	}
	return 0;
}


int dictionnary_load(dictionnary_t* dic, char* dic_filename){
	wchar_t line[512];
	wchar_t key[1024];
	wchar_t value[1024];

	// Ouverture du fichier en mode lecture seule
	FILE* fp = fopen(dic_filename,"r"); // read mode
	if( fp == NULL ){
		perror("Erreur d'ouverture du dictionnaire.\n");
		return ERROR_FILE_OPEN;
	}

	// Parcours des paramètres du fichier
	while (fwscanf(fp, L"%l[^\n]\n", line) == 1){
		if(line[0]!=L'#'){
			swscanf(line, L"%l[^=]=%l[^\n]\n", key, value);
			dictionnary_add(dic, key, value);
		}
	}

	// Fermeture du fichier
	fclose(fp);

	return 0;

}

int dictionnary_free(linked_list_t* dic){
	return list_free(dic);
}


int config_add(wchar_t* key, wchar_t* value){
	return dictionnary_add(&CONFIGURATION, key, value);
}


int config_get(wchar_t* key, wchar_t* value, int* found){
	return dictionnary_get(&CONFIGURATION, key, value, found);
}



int config_load(char* config_filename){
	int error, found;
	wchar_t key[1024];
	wchar_t value[1024];
	dictionnary_init(&CONFIGURATION);
	dictionnary_load(&CONFIGURATION, config_filename);

	wprintf(L"Configuration : \n");
	for(size_t n=0; n<CONFIGURATION.size; n++){
		error=dictionnary_get_association(&CONFIGURATION, n, key, value, &found);
		if(!error && found){
			wprintf(L"%ls=\"%ls\"\n", key, value);
		}
	}
	wprintf(L"\n");

	return 0;

}



int generate_descriptor_id(wchar_t prefix[3], descriptor_id_t* id){
	*id=(wchar_t*) calloc(DESCRIPTOR_ID_LENGTH+1, sizeof(wchar_t));
	swprintf(*id, DESCRIPTOR_ID_LENGTH, L"%ls%016d", prefix, AVAILABLE_DOCUMENT_ID++);

	return 0;
}

int wstring_to_string(wchar_t* in, char* out){
	wcstombs( out, in, wcslen(in) );
	out[wcslen(in)]='\0';
	return 0;
}

int clean_string(wchar_t* string_wchar_t){
	int i=0;
	int j=0;
	int test=0;

	// test si string_wchar_t est vide
	if (string_wchar_t==NULL){
		return(0);
	}
	else{
		i=0; //sert pour parcourir la chaine de caractère
		//Parcours la chaine de wchar_t
		while (string_wchar_t[i]!='\0'){
			//Traitement des Majuscules
			if(string_wchar_t[i]>='A'&& string_wchar_t[i]<='Z'){
				//pour changer une maj en min il faut lui ajouter 32h soit le code ascii de ' '
				string_wchar_t[i] += ' ';
			}
			else {
				j=0;
				test=0;
				//Parcours le tableau des caracteres spéciaux
				while (carac_spe_wchar_t[j]!='\0'){
					if(carac_spe_wchar_t[j]==string_wchar_t[i]){
						test=1;
					}
					j++;
				}
			    if (test!=1){
			    	//Traitement de la ponctuation
					if(!((string_wchar_t[i]>='a'&& string_wchar_t[i]<='z')||(string_wchar_t[i]>='0'&& string_wchar_t[i]<='9'))){
						//remplace la ponctuation par ' '
						string_wchar_t[i]= ' ';
					}
				}
			}
			i++;
		}
		return(1);
	}
}
