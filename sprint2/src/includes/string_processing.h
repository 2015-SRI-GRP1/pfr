/**
* \File string_processing.h
* \brief .h de string_processing.c
* \author Bastien Veyssiere
* \version 1.0
* \date 25 décembre 2015
*/
#include <stdio.h>
#include <wchar.h>
#include "text_types.h"
#include "util.h"

int string_processing(wchar_t* string_wchar_t, text_descriptor_t* inst_descriptor_t,one_term_t tab_one_term_t[]);
