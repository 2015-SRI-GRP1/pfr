/**
* \File string_processing.c
* \brief taitement de la phrase passée en paramètre pour compter les mots
* \author Bastien Veyssiere
* \version 1.1
* \date 23 décembre 2015
*/
#include <wchar.h>
#include <string.h>
#include "text_types.h"
#include "string_processing.h"
int string_processing(wchar_t* string_wchar_t, text_descriptor_t* inst_descriptor_t, one_term_t tab_one_term_t[]){

	wchar_t * token;
	wchar_t * etat;
	int i=0;
	int test=0;

	token= wcstok(string_wchar_t,L" ",&etat);
printf("1\n"); fflush(stdout);
	while(token!=NULL){
printf("2\n"); fflush(stdout);
		//Augmente le nombre de mots dans le descripteur
		inst_descriptor_t->word_numb_int ++;
printf("3\n"); fflush(stdout);
		//verifie que le mot soit plus grand que SIZE_WORD_RM

		if (!(wcslen(token)<=SIZE_WORD_RM )){
			//
printf("4\n"); fflush(stdout);
			while(tab_one_term_t[i].word_wchar_t!=NULL){
				//si le mot est present dans le tableau tab_one_term_t et rajoute une occurence
printf("5\n"); fflush(stdout);
				if(wcscmp(tab_one_term_t[i].word_wchar_t, token)==0){
printf("6\n"); fflush(stdout);
					tab_one_term_t[i].ocurrence_int ++;
printf("7\n"); fflush(stdout);
					test=1;
				}

				i++;
			}
			//sinon rajoute le mot dans le tableau

			if(test==0){
printf("8\n"); fflush(stdout);
				tab_one_term_t[i].ocurrence_int=1;
printf("9\n"); fflush(stdout);
				tab_one_term_t[i].word_wchar_t = wcsdup(token);
			}
		}
printf("10\n"); fflush(stdout);
		i=0;
		test=0;
		token = wcstok(NULL,L" ",&etat);
printf("11\n"); fflush(stdout);
	}

	return(0);
}
