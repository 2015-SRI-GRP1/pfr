#ifndef SRI_SOUND_TYPES_H
#define SRI_SOUND_TYPES_H
/**
* \File sound_types.h
* \brief Ce fichier définit les structures utilisées dans l'indexation et la recherche de fichiers sons.
* \author Molina Serge
* \version 1.0
* \date 22 Decembre 2015
*/

#include "util.h"
#include "wchar.h"

/**
* \struct descriptor_sound_t sound_types.h
* \brief Structure définissant un descripteur texte
*/
typedef struct {
	descriptor_id_t id;
	linked_list_t histograms;
	int histograms_size;
	int histograms_levels;
}sound_descriptor_t;

int sound_descriptor_init(sound_descriptor_t* descriptor);
int sound_descriptor_from_file(FILE* source, sound_descriptor_t* descriptor);
int sound_descriptor_to_file(FILE* output_fd, sound_descriptor_t* descriptor);
int sound_descriptor_free(sound_descriptor_t* descriptor);


#endif // SRI_SOUND_TYPES_H
