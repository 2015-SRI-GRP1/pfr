/**
* \file image_types.c
* \brief Déclaration des fonctions d'image_types.h
* \author Deguilhem Valentin
* \version 1.2
* \date 30 Décembre 2015
*/

#include"image_types.h"
#include<stdio.h>
#include<string.h>


int quantification_image(int r_int, int g_int, int b_int, int* ptr_quantified_value_int){
	int r1_int;
	int r2_int;
	int g1_int;
	int g2_int;
	int b1_int;
	int b2_int;
	//Rouge
	if(r_int > 127){
			r1_int = 1;
		if((r_int - 128) > 63){
			r2_int = 1;
		}else{
			r2_int = 0;
		}
	}else{
		r1_int = 0;
		if(r_int > 63){
			r2_int = 1;
		}else{
			r2_int = 0;
		}
	}
	
	
	//Vert
	if(g_int > 127){
		g1_int = 1;
		if((g_int - 128) > 63){
			g2_int = 1;
		}else{
			g2_int = 0;
		}
	}else{
		g1_int = 0;
		if(g_int > 63){
			g2_int = 1;
		}else{
			g2_int = 0;
		}
	}
	
	
	//Bleu
	if(b_int > 127){
			b1_int = 1;
		if((b_int - 128) > 63){
			b2_int = 1;
		}else{
			b2_int = 0;
		}
	}else{
		b1_int = 0;
		if(b_int > 63){
			b2_int = 1;
		}else{
			b2_int = 0;
		}
	}
	(*ptr_quantified_value_int) = r1_int*32 + r2_int*16 + g1_int*8 + g2_int*4 + b1_int*2 + b2_int;
	return 0;
}



int init_image_descriptor(image_descriptor_t* ptr_imdes_image_descriptor_t){
	int i;
	for(i = 0; i < 64; i++){
		(*ptr_imdes_image_descriptor_t).histogram_int[i] = 0;
	}
	return 0;
}
