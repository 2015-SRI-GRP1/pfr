
#include "../util.h"
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sound_indexing.h"

uint64_t byteswap64(uint64_t input)
{
    uint64_t output = (uint64_t) input;
    output = (output & 0x00000000FFFFFFFF) << 32 | (output & 0xFFFFFFFF00000000) >> 32;
    output = (output & 0x0000FFFF0000FFFF) << 16 | (output & 0xFFFF0000FFFF0000) >> 16;
    output = (output & 0x00FF00FF00FF00FF) << 8  | (output & 0xFF00FF00FF00FF00) >> 8;
    return output;
}

int sound_descriptor_creation(FILE* sound_fp, sound_descriptor_t* descriptor){
	int error, found, bytes_red;
	uint64_t read_value;

	int window_size = descriptor->histograms_size;
	int window_levels = descriptor->histograms_levels;

	int* histogram=NULL;
	size_t window=0;

	double current_value;
	int current_level;
	int position_in_window=0;
	while( (bytes_red=fread(&read_value, sizeof(read_value), 1, sound_fp)) == 1 ){
		if(position_in_window==0){
			do{
				histogram = (int*) calloc(window_levels, sizeof(int) );
			}while(histogram==NULL);

			memset(histogram, 0, window_levels*sizeof(int));

		}
		read_value=byteswap64(read_value);
		current_value=*(double*)&read_value;

		current_value=(current_value*.5+.5);
		current_level=(uint32_t)(current_value*window_levels);
		histogram[current_level]++;

		position_in_window++;


		if(position_in_window>=window_size){
			window++;
			position_in_window=0;
			list_add(&descriptor->histograms, histogram);
		}

	}
	if(position_in_window!=0){
		list_add(&descriptor->histograms, histogram);
	}


	return 0;
}
