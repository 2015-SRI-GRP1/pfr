/**
* \file image_indexation.c
* \brief Déclarations des fonctions de image_indexing.h
* \author Deguilhem Valentin
* \version 1.1
* \date 30 Décembre 2015
*/

#include<stdio.h>
#include"image_indexing.h"

int image_descriptor_creation(char* adress_file_char,image_descriptor_t* ptr_imdes_image_descriptor_t ){
	FILE* fp = fopen(adress_file_char,"r"); 
	if( fp == NULL ){
		perror("Erreur d'ouverture du fichier.\n");
		return ERROR_FILE_OPEN;
	}
	int cpt_int, l_int, h_int, nbcomp_int;
	fscanf(fp,"%d %d %d", &l_int, &h_int, &nbcomp_int);
	if(nbcomp_int == 3){
		int r_int, g_int, b_int, quantified_value_int;
		for(cpt_int = 0; cpt_int < (l_int * h_int); cpt_int++){
			fscanf(fp,"%d %d %d", &r_int, &g_int, &b_int);
			quantification_image(r_int, g_int, b_int, &quantified_value_int);
			(*ptr_imdes_image_descriptor_t).histogram_int[quantified_value_int] = (*ptr_imdes_image_descriptor_t).histogram_int[quantified_value_int] + 1;
		}
	}
	if(nbcomp_int == 1){
		int comp_int,current_int, quantified_value_int;
		for(cpt_int = 0; cpt_int < (l_int * h_int); cpt_int++){
			fscanf(fp,"%d", &current_int);
			quantification_image(current_int, current_int, current_int, &quantified_value_int);
			(*ptr_imdes_image_descriptor_t).histogram_int[quantified_value_int] = (*ptr_imdes_image_descriptor_t).histogram_int[quantified_value_int] + 1;
		}
	}
	return 0;
}


