/**
* \file text_indexing.h
* \brief Fonctions utilisées pour l'indexation
* \author Bilal El yassem
* \version 1.2
* \date 28 Décembre 2015
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>
#include "../util.h"
#include "../text_types.h"

////////////Fonctions
/**
* \fn int text_descriptor_creation
* \brief fonction de création d'un descripteur texte
* \param  ptr_adress_file_char
* \return int, code d'erreur
*/
int text_descriptor_creation(char* ptr_address_file_char);

/**
* \fn int format_and_clean
* \brief Nétoie et formate une chaîne de caractère pour qu'elle soit traitée.
* \param  substring
* \return int, code d'erreur
*/
int format_and_clean(char* substring);