/**
* \file image_indexing.h
* \brief Fonctions utilisées pour l'indexation des sons
* \author Molina Serge
* \version 1.0
* \date 18 Décembre 2015
*/
#include <wchar.h>
#include <stdio.h>
#include "../util.h"
#include "../sound_types.h"



/**
* \fn int sound_descriptor_creation
* \brief fonction de création d'un descripteur de son
* \param  ptr_adress_file_char
* \return int, code d'erreur
*/
int sound_descriptor_creation(FILE* sound_fp, sound_descriptor_t* descriptor);
