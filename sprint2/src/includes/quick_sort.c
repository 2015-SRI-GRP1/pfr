/**
* \File quick_sort.c
* \brief Fonction de trie rapide des tableau de type one_term_t
* \author Bastien Veyssière
* \version 1.0
* \date 28 Decembre 2015
*/

#include "quick_sort.h"

void exchange (term_t tableau[] , int a, int b) {

    one_term_t buff->ocurrence_int = tableau[a].ocurrence_int;
    wcscpy(buff->word_wchar_t, tableau[a].word_wchar_t);
    tableau[a].ocurrence_int = tableau[b].ocurrence_int;
    wcscpy(tableau[a].word_wchar_t , tableau[b].word_wchar_t);
    tableau[b].ocurrence_int = buff->ocurrence_int;
    wcscpy(tableau[b].word_wchar_t ,buff->word_wchar_t);
}

int quick_sort (term_t tableau[], int start, int end){
    
    int left = start-1;
    int right = end+1;
    const int pivot = tableau[start].ocurrence_int;

    if(start >= end)
        return(0);

    /* On parcourt le tableau, une fois de right à left, et une
       autre de left à right, on permute les  éléments mal placés.
       Si les deux parcours se croisent, on arrête. */
    while(1){
        
        do{
       	 right--; 
        }while(tableau[right].ocurrence_int > pivot);
        
        do{
        	left++;
        }while(tableau[left].ocurrence_int< pivot);

        if(left < right)
            exchange(tableau, left, right);
        else break;
    }

	// Methode recursive pour les deux sous-tableaux
	quick_sort(tableau, start, right);
    quick_sort(tableau, right+1, end);
    return(1);
}
void main(){}

