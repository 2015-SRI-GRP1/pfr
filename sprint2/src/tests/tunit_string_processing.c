/**
* \File tunit_string_processing.c
* \brief test uitaire de la fonction string_processing
* \author Bastien Veyssiere
* \version 1.0
* \date 27 décembre 2015
*/
#include "../includes/string_processing.h"
#include "../includes/text_types.h"
#include <stdlib.h>
int main(){
	wchar_t* id_wchar_t;
        term_t list_term_t;
        int word_file_int;
        int word_numb_int;
	wchar_t phrase[50]=L"le petit petit bonhomme des mousse vert";
	one_term_t tab_stockage[NB_TERM+1];

	text_descriptor_t * descripteur= (text_descriptor_t*) malloc(sizeof(text_descriptor_t));
	descripteur->id_wchar_t=L"id";
	descripteur->list_term_t=(one_term_t*)calloc(NB_TERM_MAX+1, sizeof(one_term_t));
	descripteur->word_file_int=0;
	descripteur->word_numb_int=0;


	for( int j = 0; j < NB_TERM+1 ; j++){
		tab_stockage[j].word_wchar_t=NULL;
		tab_stockage[j].ocurrence_int=0;
	}
	tab_stockage[0].word_wchar_t=L"petit";
	tab_stockage[0].ocurrence_int=1;
	int i=0;
	while(i<10){
		wprintf(L"Mot %ls",tab_stockage[i].word_wchar_t);
		wprintf(L" Occurence %ld \n",tab_stockage[i].ocurrence_int);
		i++;
	}
	wprintf(L"TEST2 ");
	wprintf(L"phrase : %ls \n",phrase);

	string_processing(phrase, descripteur , tab_stockage);

	i=0;
	wprintf(L"le nb de mot %d\n",descripteur->word_file_int);
	while(i<10){
		wprintf(L"Mot %ls",tab_stockage[i].word_wchar_t);
		wprintf(L" Occurence %d \n",tab_stockage[i].ocurrence_int);
		i++;
	}
}
