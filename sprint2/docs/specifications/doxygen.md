# Commentaires Doxygen

## Fichiers sources et headers
```c
/**
* \File nom.c
* \brief {description}
* \author {nom des auteurs}
* \version 1.0
* \date 10 decembre 2015
*/
```

## Struct
```c
/**
* \struct Str_t str.h "Definition"
* \brief {description}
*/
```

## Fonctions
```c
/**
* \fn int main(void)
* \brief {description}
* \param <parameter-name> {description}
* \return {description}
*/
```

## Constante
```c
/**
* \def NOM valeur
* \brief {description}
*/
```