# Convention de nommage : 

- ## Fichiers : nom_du_fichier.c
- ## Constantes : CONSTANTE
- ## Types : nom_du_type_t
- ## Variables : 
    - variables classiques : nom_de_la_variable_type
    - pointeurs : ptr_nom_de_la_variable_type
- ## Fonctions : nom_de_la_fonction(nom_de_la_variable)