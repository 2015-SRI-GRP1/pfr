#include <string.h>
#include <wchar.h>
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <stdint.h>
#include <sys/stat.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>

#include "includes/util.h"

#include "includes/image_types.h"
#include "includes/sound_types.h"
#include "includes/text_types.h"

#include "includes/indexing/image_indexing.h"
#include "includes/indexing/sound_indexing.h"
#include "includes/indexing/text_indexing.h"

#include "includes/querying/image_querying.h"
#include "includes/querying/sound_querying.h"
#include "includes/querying/text_querying.h"

static wchar_t* IMAGES_BASE_FILENAME=(wchar_t*) L"img_base.txt";
static wchar_t* SOUNDS_BASE_FILENAME=(wchar_t*)  L"snd_base.txt";
static wchar_t* TEXTS_BASE_FILENAME=(wchar_t*)  L"txt_base.txt";

static wchar_t* IMAGES_ASSOCIATION_FILENAME=(wchar_t*)  L"img_association.txt";
static wchar_t* SOUNDS_ASSOCIATION_FILENAME=(wchar_t*)  L"snd_association.txt";
static wchar_t* TEXTS_ASSOCIATION_FILENAME=(wchar_t*)  L"txt_association.txt";




static dictionnary_t IMAGES_ASSOCIATION;
static dictionnary_t SOUNDS_ASSOCIATION;
static dictionnary_t TEXTS_ASSOCIATION;

static linked_list_t IMAGES_BASE;
static linked_list_t SOUNDS_BASE;
static linked_list_t TEXTS_BASE;

static char result_filename[1024];
static FILE* result_file_ptr;


int open_file_in_index(wchar_t* index_path, wchar_t* filename, char* mode, FILE** output_fp){
	wchar_t tmp_filename_wchar[1024];
	char tmp_filename_char[1024];

	swprintf(tmp_filename_wchar, 1024, L"%ls/%ls", index_path, filename);
	wstring_to_string(tmp_filename_wchar, tmp_filename_char);
	// Ouverture de la liste des fichiers à indexer en lecture seule

	*output_fp = fopen(tmp_filename_char, mode);

	if(*output_fp == NULL){
		wprintf(L"Erreur d'ouverture du fichier %ls\n", tmp_filename_wchar);
		return ERROR_FILE_OPEN;
	}
	return 0;
}




int load_index(wchar_t* index_path){
	int error;
	int found;


	FILE* base_image_fp;
	FILE* base_sound_fp;
	FILE* base_text_fp;

	FILE* association_image_fp;
	FILE* association_sound_fp;
	FILE* association_text_fp;

	wchar_t* filename_wchar;
	wchar_t line[1024];


	sound_descriptor_t* sound_descriptor;
	image_descriptor_t* image_descriptor;
	text_descriptor_t* text_descriptor;

	descriptor_id_t document_id=(wchar_t*) calloc(DESCRIPTOR_ID_LENGTH, sizeof(wchar_t));

	dictionnary_init(&IMAGES_ASSOCIATION);
	dictionnary_init(&SOUNDS_ASSOCIATION);
	dictionnary_init(&TEXTS_ASSOCIATION);


	error=open_file_in_index(index_path, IMAGES_BASE_FILENAME, (char*)"r", &base_image_fp);
	if(error!=0){
		return error;
	}

	error=open_file_in_index(index_path, SOUNDS_BASE_FILENAME, (char*)"r", &base_sound_fp);
	if(error!=0){
		return error;
	}

	error=open_file_in_index(index_path, TEXTS_BASE_FILENAME, (char*)"r", &base_text_fp);
	if(error!=0){
		return error;
	}

	error=open_file_in_index(index_path, IMAGES_ASSOCIATION_FILENAME, (char*)"r", &association_image_fp);
	if(error!=0){
		return error;
	}

	error=open_file_in_index(index_path, SOUNDS_ASSOCIATION_FILENAME, (char*)"r", &association_sound_fp);
	if(error!=0){
		return error;
	}

	error=open_file_in_index(index_path, TEXTS_ASSOCIATION_FILENAME, (char*)"r", &association_text_fp);
	if(error!=0){
		return error;
	}


	dictionnary_load_from_file(&SOUNDS_ASSOCIATION, association_sound_fp);
	dictionnary_load_from_file(&IMAGES_ASSOCIATION, association_image_fp);
	dictionnary_load_from_file(&TEXTS_ASSOCIATION, association_text_fp);

	do{
		image_descriptor=(image_descriptor_t*) malloc(sizeof(image_descriptor_t));
		//error=image_descriptor_from_file(base_image_fp, image_descriptor);
		error=EOF;
		if(!error){
			error=list_add(&TEXTS_BASE, image_descriptor);
		}
	}while (error!=EOF);

	do{
		sound_descriptor=(sound_descriptor_t*) malloc(sizeof(sound_descriptor_t));
		error=sound_descriptor_from_file(base_sound_fp, sound_descriptor);
		if(!error){
			error=list_add(&SOUNDS_BASE, sound_descriptor);
		}
	}while (error!=EOF);



	do{

		text_descriptor=(text_descriptor_t*) malloc(sizeof(text_descriptor_t));
		error=text_descriptor_from_file(base_text_fp, text_descriptor);
		if(!error){
			error=list_add(&TEXTS_BASE, text_descriptor);
		}
	}while(error!=EOF);



	return 0;
}


int start_querying(char* liste_queries){
	int error, found;

	double score;

	FILE* queries_list_fp;
	FILE* query_document_fp;


	wchar_t query[1024];
	wchar_t* filename_wchar;
	char filename_char[1024];
	wchar_t* document_filename;

	wchar_t* file_extension;


	sound_descriptor_t* sound_descriptor;
	sound_descriptor_t* sound_descriptor_in_index;

	text_descriptor_t* text_descriptor;
	text_descriptor_t* text_descriptor_in_index;

	image_descriptor_t* imdes_image_descriptor_t;
	image_descriptor_t* imdes_in_index_image_descriptor_t;


	// Ouverture de la liste des fichiers à indexer en lecture seule
	queries_list_fp = fopen(liste_queries,"r");
	if(queries_list_fp == NULL){
		perror("Erreur d'ouverture de la liste des fichiers à indexer.\n");
		return ERROR_FILE_OPEN;
	}







	// Parcours des paramètres du fichier
	while (fwscanf(queries_list_fp, L"%l[^\n]\n", query) == 1){

		filename_wchar = wcsrchr(query, ':');
		filename_wchar[0]= L'\0';
		filename_wchar++;

		file_extension = wcsrchr(filename_wchar, '.')+1;

		if(wcscmp(file_extension, L"txt")==0 || wcscmp(file_extension, L"TXT")==0 ){

			wstring_to_string(filename_wchar, filename_char);
			query_document_fp = fopen(filename_char,"r");
			if(query_document_fp == NULL){
				perror("Erreur d'ouverture du document.\n");
				return ERROR_FILE_OPEN;
			}


			imdes_image_descriptor_t = (image_descriptor_t*) malloc(sizeof(image_descriptor_t));


			error = init_image_descriptor(imdes_image_descriptor_t);

			if(error!=0){
				wprintf(L"Error while initialising descriptor for %ls\n", filename_wchar);
				return error;
			}
			image_descriptor_creation(query_document_fp, imdes_image_descriptor_t);

			for(size_t n=0; n<IMAGES_BASE.size; n++){
				error=list_get(&IMAGES_BASE, n, (void**)&imdes_in_index_image_descriptor_t, &found);
				if(!error && found){
					compare_image_descriptor((*imdes_in_index_image_descriptor_t), (*imdes_image_descriptor_t), &score);
					error=dictionnary_get(&IMAGES_ASSOCIATION, (*imdes_in_index_image_descriptor_t).id_image_descriptor_id_t, (void**)&document_filename, &found);

					if(!error && found){
						fwprintf(result_file_ptr, L"%ls 0 %ls 0 %lf sri-pfr\n", query, document_filename, score);
					}
				}
			}
			fclose(query_document_fp);

			image_descriptor_free(imdes_image_descriptor_t);


		} else if(wcscmp(file_extension, L"bin")==0 || wcscmp(file_extension, L"BIN")==0 ){


			wstring_to_string(filename_wchar, filename_char);
			query_document_fp = fopen(filename_char,"r");
			if(query_document_fp == NULL){
				perror("Erreur d'ouverture du document.\n");
				return ERROR_FILE_OPEN;
			}
			wprintf(L"Recherche d'un son semblable à %ls\n", filename_wchar);

			sound_descriptor=(sound_descriptor_t*) malloc(sizeof(sound_descriptor_t));


			error=sound_descriptor_init(sound_descriptor);

			if(error!=0){
				wprintf(L"Error while initialising descriptor for %ls\n", filename_wchar);
				return error;
			}
			sound_descriptor_creation(query_document_fp, sound_descriptor);

			for(size_t n=0; n<SOUNDS_BASE.size; n++){
				error=list_get(&SOUNDS_BASE, n, (void**)&sound_descriptor_in_index, &found);
				if(!error && found){
					error=compare_sound_descriptor_t(sound_descriptor_in_index, sound_descriptor, &score);
					error=dictionnary_get(&SOUNDS_ASSOCIATION, sound_descriptor_in_index->id, (void**)&document_filename, &found);

					if(!error && found){
						fwprintf(result_file_ptr, L"%ls 0 %ls 0 %lf sri-pfr\n", query, document_filename, score);
					}
				}
			}

			fclose(query_document_fp);


			sound_descriptor_free(sound_descriptor);


		} else if(wcscmp(file_extension, L"xml")==0 || wcscmp(file_extension, L"XML")==0 ){

			wstring_to_string(filename_wchar, filename_char);
			query_document_fp = fopen(filename_char,"r");
			if(query_document_fp == NULL){
				perror("Erreur d'ouverture du document.\n");
				return ERROR_FILE_OPEN;
			}
			text_descriptor=(text_descriptor_t*) malloc(sizeof(text_descriptor_t));

			text_descriptor_creation(query_document_fp, NULL, text_descriptor);

			wprintf(L"Recherche d'un texte semblable à %ls\n", filename_wchar);


			for(size_t n=0; n<TEXTS_BASE.size; n++){
				error=list_get(&TEXTS_BASE, n, (void**)&text_descriptor_in_index, &found);
				if(!error && found){
					error=compare_text_descriptor(text_descriptor_in_index, text_descriptor, &score);
					error=dictionnary_get(&TEXTS_ASSOCIATION, text_descriptor_in_index->id_wchar_t, (void**)&document_filename, &found);

					if(!error && found){
						fwprintf(result_file_ptr, L"%ls 0 %ls 0 %lf sri-pfr\n", query, document_filename, score);
					}
				}
			}

			fclose(query_document_fp);


		} else {
			wprintf(L"Unknown association : %ls\n", file_extension);
		}
	}

	fclose(queries_list_fp);




	return 0;
}



void usage(int argc, char* argv[]){
	wprintf(L"Usage : \n");
	wprintf(L"\t\"%s config_file queries_list_filename\"\n", argv[0]);
}


int main(int argc, char* argv[]){
	setlocale(LC_CTYPE, "fr_FR.UTF-8");

	if(argc!=3){
		usage(argc, argv);
		return ERROR_ARGC;
	}

	int error;
	int found;
	wchar_t* index_path;


	wprintf(L"Chargement du fichier de configuration\n");
	error = config_load(argv[1]);
	if(error!=0){
		return error;
	}


	error=config_get((wchar_t*)L"index", (wchar_t**)&index_path, &found);
	if(error){
		wprintf(L"Error while looking for index path\n");
		return error;
	}else if(!found){
		wprintf(L"Index setting not found\n");
		return ERROR_PARAMETER_NOT_FOUND;
	}

	wprintf(L"Chargement de l'index\n\n");
	load_index(index_path);
	wprintf(L"Chargement de l'index terminé\n\n");

	sound_descriptor_t* sound_descriptor_in_index;
	for(size_t n=0; n<SOUNDS_BASE.size; n++){
		error=list_get(&SOUNDS_BASE, n, (void**)&sound_descriptor_in_index, &found);
		if(!error && found){
			wprintf(L"%ls ", sound_descriptor_in_index->id);
			wprintf(L"%d ", sound_descriptor_in_index->histograms_levels);
			wprintf(L"%d ", sound_descriptor_in_index->histograms.size);
			wprintf(L"%d\n", sound_descriptor_in_index->histograms_size);
		}
	}

	wprintf(L"Démarrage de la recherche\n");
	mkdir("results/", S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
	sprintf((char*)result_filename, "results/sri_%d.res", getpid());
	result_file_ptr = fopen(result_filename, "w");

	if(result_file_ptr == NULL){
		wprintf(L"Erreur dans la création du fichier de resultats %s\n", result_filename);
		return ERROR_FILE_OPEN;
	}
	start_querying(argv[2]);
	fclose(result_file_ptr);

	char command[2048];
	sprintf(command, "sort -k1,1n -k5,5nr <%s | awk 'BEGIN { first=true; MAX_RESULTS=%d;}{ if(first || $1!=last_query){first=false; occurence=0; last_query=$1;} occurence += 1; if(occurence<MAX_RESULTS){$4=occurence; print $0;}}' > %s.tmp && mv %s.tmp %s", result_filename, 10, result_filename, result_filename, result_filename);
	system(command);
	wprintf(L"Recherche terminée, resultats stockés dans:\n%s\n", result_filename);

	return 0;
}
