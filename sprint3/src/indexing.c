#include <string.h>
#include <wchar.h>
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <stdint.h>
#include <sys/stat.h>
#include <stdint.h>
#include <unistd.h>

#include "includes/util.h"

#include "includes/image_types.h"
#include "includes/sound_types.h"
#include "includes/text_types.h"

#include "includes/indexing/image_indexing.h"
#include "includes/indexing/sound_indexing.h"
#include "includes/indexing/text_indexing.h"

static wchar_t* IMAGES_BASE_FILENAME=(wchar_t*) L"img_base.txt";
static wchar_t* SOUNDS_BASE_FILENAME=(wchar_t*)  L"snd_base.txt";
static wchar_t* TEXTS_BASE_FILENAME=(wchar_t*)  L"txt_base.txt";

static wchar_t* IMAGES_ASSOCIATION_FILENAME=(wchar_t*)  L"img_association.txt";
static wchar_t* SOUNDS_ASSOCIATION_FILENAME=(wchar_t*)  L"snd_association.txt";
static wchar_t* TEXTS_ASSOCIATION_FILENAME=(wchar_t*)  L"txt_association.txt";




static dictionnary_t IMAGES_ASSOCIATION;
static dictionnary_t SOUNDS_ASSOCIATION;
static dictionnary_t TEXTS_ASSOCIATION;

static linked_list_t IMAGES_BASE;
static linked_list_t SOUNDS_BASE;
static linked_list_t TEXTS_BASE;

int open_file_in_index(wchar_t* index_path, wchar_t* filename, char* mode, FILE** output_fp){
	wchar_t tmp_filename_wchar[1024];
	char tmp_filename_char[1024];

	swprintf(tmp_filename_wchar, 1024, L"%ls/%ls", index_path, filename);
	wstring_to_string(tmp_filename_wchar, tmp_filename_char);
	// Ouverture de la liste des fichiers à indexer en lecture seule

	*output_fp = fopen(tmp_filename_char, mode);

	if(*output_fp == NULL){
		wprintf(L"Erreur d'ouverture du fichier %ls\n", tmp_filename_wchar);
		return ERROR_FILE_OPEN;
	}
	return 0;
}

int start_indexing(wchar_t* index_path, char* liste_fichiers){
	int error;

	FILE* document_list_fp;
	FILE* document_fp;

	FILE* base_image_fp;
	FILE* base_sound_fp;
	FILE* base_text_fp;

	FILE* association_image_fp;
	FILE* association_sound_fp;
	FILE* association_text_fp;

	wchar_t filename_wchar[1024];
	char filename_char[1024];

	wchar_t* file_extension;

	descriptor_id_t document_id;

	sound_descriptor_t* sound_descriptor;
	image_descriptor_t* ptr_imdes_image_descriptor_t;
	text_descriptor_t* ptr_text_descriptor;

	dictionnary_init(&IMAGES_ASSOCIATION);
	dictionnary_init(&SOUNDS_ASSOCIATION);
	dictionnary_init(&TEXTS_ASSOCIATION);

	// Ouverture de la liste des fichiers à indexer en lecture seule
	document_list_fp = fopen(liste_fichiers,"r");
	if(document_list_fp == NULL){
		perror("Erreur d'ouverture de la liste des fichiers à indexer.\n");
		return ERROR_FILE_OPEN;
	}


	wstring_to_string(index_path, filename_char);
	mkdir(filename_char, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);


	error=open_file_in_index(index_path, IMAGES_BASE_FILENAME, (char*)"w", &base_image_fp);
	if(error!=0){
		return error;
	}

	error=open_file_in_index(index_path, SOUNDS_BASE_FILENAME, (char*)"w", &base_sound_fp);
	if(error!=0){
		return error;
	}

	error=open_file_in_index(index_path, TEXTS_BASE_FILENAME, (char*)"w", &base_text_fp);
	if(error!=0){
		return error;
	}

	error=open_file_in_index(index_path, IMAGES_ASSOCIATION_FILENAME, (char*)"w", &association_image_fp);
	if(error!=0){
		return error;
	}

	error=open_file_in_index(index_path, SOUNDS_ASSOCIATION_FILENAME, (char*)"w", &association_sound_fp);
	if(error!=0){
		return error;
	}

	error=open_file_in_index(index_path, TEXTS_ASSOCIATION_FILENAME, (char*)"w", &association_text_fp);
	if(error!=0){
		return error;
	}




	// Parcours des paramètres du fichier
	while (fwscanf(document_list_fp, L"%l[^\n]\n", filename_wchar) == 1){
		file_extension = wcsrchr(filename_wchar, '.');
		if(file_extension!=NULL){
			file_extension = file_extension+1;
			if(wcscmp(file_extension, L"txt")==0 || wcscmp(file_extension, L"TXT")==0 ){

				wstring_to_string(filename_wchar, filename_char);
				document_fp = fopen(filename_char,"r");
				if(document_fp == NULL){
					perror("Erreur d'ouverture du document.\n");
					return ERROR_FILE_OPEN;
				}

				ptr_imdes_image_descriptor_t = (image_descriptor_t*) malloc(sizeof(image_descriptor_t));


				error = init_image_descriptor(ptr_imdes_image_descriptor_t);

				if(error!=0){
					wprintf(L"Error while initialising descriptor for %ls\n", filename_wchar);
					return error;
				}
				image_descriptor_creation(document_fp, ptr_imdes_image_descriptor_t);


				error=dictionnary_add(&IMAGES_ASSOCIATION, ptr_imdes_image_descriptor_t->id_image_descriptor_id_t, filename_wchar);
				error=list_add(&IMAGES_BASE, ptr_imdes_image_descriptor_t);
				fclose(document_fp);


				wprintf(L"Writing descriptor to disk for %ls (%ls)\n", filename_wchar, ptr_imdes_image_descriptor_t->id_image_descriptor_id_t);

				image_descriptor_to_file(base_image_fp, ptr_imdes_image_descriptor_t);
				fwprintf(association_image_fp, L"%ls=%ls\n", ptr_imdes_image_descriptor_t->id_image_descriptor_id_t, filename_wchar);
				dictionnary_add(&IMAGES_ASSOCIATION, ptr_imdes_image_descriptor_t->id_image_descriptor_id_t, filename_wchar);

				image_descriptor_free(ptr_imdes_image_descriptor_t);

			} else if(wcscmp(file_extension, L"bin")==0 || wcscmp(file_extension, L"BIN")==0 ){


				wstring_to_string(filename_wchar, filename_char);
				document_fp = fopen(filename_char,"r");
				if(document_fp == NULL){
					perror("Erreur d'ouverture du document.\n");
					return ERROR_FILE_OPEN;
				}

				sound_descriptor=(sound_descriptor_t*) malloc(sizeof(sound_descriptor_t));


				error=sound_descriptor_init(sound_descriptor);

				if(error!=0){
					wprintf(L"Error while initialising descriptor for %ls\n", filename_wchar);
					return error;
				}
				sound_descriptor_creation(document_fp, sound_descriptor);

				error=dictionnary_add(&SOUNDS_ASSOCIATION, sound_descriptor->id, filename_wchar);
				error=list_add(&SOUNDS_BASE, sound_descriptor);
				fclose(document_fp);


				wprintf(L"Writing descriptor to disk for %ls (%ls)\n", filename_wchar, sound_descriptor->id);

				sound_descriptor_to_file(base_sound_fp, sound_descriptor);
				fwprintf(association_sound_fp, L"%ls=%ls\n", sound_descriptor->id, filename_wchar);

				sound_descriptor_free(sound_descriptor);


			} else if(wcscmp(file_extension, L"xml")==0 || wcscmp(file_extension, L"XML")==0 ){
				wstring_to_string(filename_wchar, filename_char);
				document_fp = fopen(filename_char,"r");
				if(document_fp == NULL){
					perror("Erreur d'ouverture du document.\n");
					return ERROR_FILE_OPEN;
				}
				ptr_text_descriptor=(text_descriptor_t*) malloc(sizeof(text_descriptor_t));

				text_descriptor_creation(document_fp, base_text_fp, ptr_text_descriptor);
				//On ajoute le descripteur à l'index
				wprintf(L"Writing descriptor to disk for %ls (%ls)\n", filename_wchar, ptr_text_descriptor->id_wchar_t);
				text_descriptor_to_file(base_text_fp,  ptr_text_descriptor);
				fclose(document_fp);
				fwprintf(association_text_fp, L"%ls=%ls\n", ptr_text_descriptor->id_wchar_t, filename_wchar);

				error=dictionnary_add(&TEXTS_ASSOCIATION, ptr_text_descriptor->id_wchar_t, filename_wchar);

				// TODO régler le free
				//free_text_descriptor(ptr_text_descriptor);
			} else {
				wprintf(L"Extension inconnue pour : '%ls'\n", filename_wchar);
			}
		} else {
			wprintf(L"Le fichier ne comporte pas d'extension : '%ls'\n", filename_wchar);
		}
	}

	fclose(document_list_fp);
	fclose(base_image_fp);
	fclose(base_sound_fp);
	fclose(base_text_fp);
	fclose(association_image_fp);
	fclose(association_sound_fp);
	fclose(association_text_fp);




	return 0;
}


void usage(int argc, char* argv[]){
	wprintf(L"Usage : \n");
	wprintf(L"\t\"%s config_file documents_list_filename\"\n", argv[0]);
}

void test(){
	int found, error;
	dictionnary_t dic_principal;
	dictionnary_t* tmp_dic;
	int* tmp_occurence;

	wchar_t* mot=(wchar_t*)malloc(1024*sizeof(wchar_t));
	descriptor_id_t id_descripteur = (wchar_t*) calloc(DESCRIPTOR_ID_LENGTH, sizeof(descriptor_id_t));

	dictionnary_init(&dic_principal);


	tmp_dic=(dictionnary_t*)malloc(sizeof(dictionnary_t));
	dictionnary_init(tmp_dic);
	dictionnary_add(&dic_principal, (wchar_t*)L"hola", tmp_dic);

	dictionnary_add_primitive(tmp_dic, (wchar_t*) L"doc1", &(int){1}, sizeof(int));
	dictionnary_add_primitive(tmp_dic, (wchar_t*) L"doc2", &(int){3}, sizeof(int));
	dictionnary_add_primitive(tmp_dic, (wchar_t*) L"doc3", &(int){2}, sizeof(int));

	tmp_dic=(dictionnary_t*)malloc(sizeof(dictionnary_t));
	dictionnary_init(tmp_dic);
	dictionnary_add(&dic_principal, (wchar_t*) L"de", tmp_dic);

	dictionnary_add_primitive(tmp_dic, (wchar_t*) L"doc1", &(int){2}, sizeof(int));
	dictionnary_add_primitive(tmp_dic, (wchar_t*) L"doc2", &(int){1}, sizeof(int));
	dictionnary_add_primitive(tmp_dic, (wchar_t*) L"doc3", &(int){4}, sizeof(int));

	tmp_dic=(dictionnary_t*)malloc(sizeof(dictionnary_t));
	dictionnary_init(tmp_dic);
	dictionnary_add(&dic_principal, (wchar_t*) L"la", tmp_dic);

	dictionnary_add_primitive(tmp_dic, (wchar_t*) L"doc1", &(int){5}, sizeof(int));
	dictionnary_add_primitive(tmp_dic, (wchar_t*) L"doc2", &(int){2}, sizeof(int));
	dictionnary_add_primitive(tmp_dic, (wchar_t*) L"doc3", &(int){3}, sizeof(int));
	dictionnary_add_primitive(tmp_dic, (wchar_t*) L"doc4", &(int){1}, sizeof(int));





	int taille_principale, taille_sous;

	taille_principale=dic_principal.associations.size;

	for(int i=0; i<taille_principale; i++){
		error=dictionnary_get_association(&dic_principal, i, mot, (void**)&tmp_dic, &found);
		if(found && !error){
			taille_sous=tmp_dic->associations.size;
			wprintf(L"%ls (%d)\n", mot, taille_sous);

			for(int j=0; j<taille_sous; j++){
				error=dictionnary_get_association(tmp_dic, j, id_descripteur, (void**)&tmp_occurence, &found);
				if(found && !error){
					wprintf(L"\t%ls:%d\n", id_descripteur, *tmp_occurence);
				} else {
					wprintf(L"\t%d(%d, %d)", j, found, error);
				}
			}
		}
	}
}


int main(int argc, char* argv[]){
	setlocale(LC_CTYPE, "fr_FR.UTF-8");

	if(argc!=3){
		usage(argc, argv);
		return ERROR_ARGC;
	}

	int error;
	int found;
	wchar_t* index_path;




	wprintf(L"Chargement du fichier de configuration\n");
	error = config_load(argv[1]);
	if(error!=0){
		return error;
	}

	error=config_get((wchar_t*)L"index", &index_path, &found);
	if(error){
		wprintf(L"Error while looking for index path\n");
		return error;
	}else if(!found){
		wprintf(L"Index setting not found\n");
		return ERROR_PARAMETER_NOT_FOUND;
	}

	/* On ignore le chargement d'un index existant pour l'instant
	wprintf(L"Chargement de l'index\n");
	load_index(index_path);
	wprintf(L"Chargement de l'index terminé\n");
	*/

	wprintf(L"Démarrage de l'indexation\n");
	start_indexing(index_path, argv[2]);
	wprintf(L"Indexation terminée\n");

	return 0;
}
