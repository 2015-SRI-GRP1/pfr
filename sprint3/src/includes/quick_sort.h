/**
* \File quick_sort.h
* \brief .h de quick_sort.c
* \author Bastien Veyssière
* \version 1.0
* \date 28 Decembre 2015
*/
#include "text_types.h"

/**
* \fn void exchange(term_t tableau[], int a, int b)
* \brief Echange les cases d'un tableau de type one_term_t
* \param term_t tableau[] : tableau dont on doit echanger les cases
* \		 int a : indice 1er case à echanger 
* \		 int b : indice 2eme case à echanger 
* \return void
*/
void exchange(term_t tableau[], int a, int b);

/**
* \fn int quick_sort(term_t tableau[], int start, int end)
* \brief Trie un tableau de type one_term_t
* \param term_t tableau[] : tableau à trier
* \		 int a : indice debut 
* \		 int b : indice fin 
* \return codes d'erreurs
* \		  0 : tableau vide
* \       1 : traitement effectué
*/
int quick_sort(term_t tableau[], int start, int end)
