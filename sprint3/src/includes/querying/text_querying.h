/**
* \File text_querying.h
* \brief Ce fichier d�finit les fonctions utilis�es dans la recherche de fichiers texte.
* \author Bilal El yassem
* \version 1.0
* \date 12 Janvier 2016
*/
#ifndef SRI_TEXT_QUERYING_H
#define SRI_TEXT_QUERYING_H
#include <wchar.h>
#include <locale.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "../text_types.h"
#include "../util.h"


/**
* \fn int compare_text_descriptor(text_descriptor_t * descriptor1,text_descriptor_t * descriptor2, double* score)
* \brief Compare deux descripteurs texte et renvoie un taux de ressemblance.
* \param text_descriptor_t * descriptor1,text_descriptor_t * descriptor2, double* score
* \return Code d'erreur
*/
int compare_text_descriptor(text_descriptor_t * descriptor1,text_descriptor_t * descriptor2, double* score);
#endif // SRI_TEXT_TYPES