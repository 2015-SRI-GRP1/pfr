/**
* \file image_querying.c
* \brief Déclaraiton des fonctions décrites dans image_querying.h
* \author Deguilhem Valentin
* \version 1.0
* \date 8 Janvier 2016
*/
#include <stdio.h>
#include <stdlib.h>
#include "../indexing/image_indexing.h"
#include "image_querying.h"


int compare_image_descriptor(image_descriptor_t imdes1_image_descriptor_t,image_descriptor_t imdes2_image_descriptor_t, float* similarity_float){
  int i,j,k;
  float s1_float = 0, s2_float = 0, s3_float = 0;
  float diff_float[IMAGE_QUANT_LVL];
  for(i = 0; i < IMAGE_QUANT_LVL;i++){
    s1_float = s1_float + imdes1_image_descriptor_t.histogram_int[i];
    s2_float = s2_float + imdes2_image_descriptor_t.histogram_int[i];
    printf("%d %d\n",imdes1_image_descriptor_t.histogram_int[i], imdes2_image_descriptor_t.histogram_int[i] );
  }
  for(j = 0; j < IMAGE_QUANT_LVL;j++){
    if(((imdes1_image_descriptor_t.histogram_int[j]/s1_float) - (imdes2_image_descriptor_t.histogram_int[j]/s2_float)) < 0){
      diff_float[j] = ((imdes1_image_descriptor_t.histogram_int[j]/s1_float) - (imdes2_image_descriptor_t.histogram_int[j]/s2_float))*(-1);
    }else{
      diff_float[j] = ((imdes1_image_descriptor_t.histogram_int[j]/s1_float) - (imdes2_image_descriptor_t.histogram_int[j]/s2_float));
    }
    printf("%f\n", diff_float[j]);
  }
  printf("\n");
  for(k = 0; k < IMAGE_QUANT_LVL; k++){
    s3_float = s3_float + (diff_float[k]*100);
    printf("%d %f\n", k, s3_float);
  }
  (*similarity_float) = (s3_float/IMAGE_QUANT_LVL)*10;
  return 0;
}
