/**
* \file image_querying.h
* \brief Définition des fonctions de recherche pour les fichiers image
* \author Deguilhem Valentin
* \version 1.0
* \date 8 Janvier 2016
*/
#include <stdio.h>
#include "../util.h"
#include "../image_types.h"

/**
* \fn int compare_image_descriptor
* \brief fonction de comparaison de deux descripteurs image.
* \param imdes1_image_descriptor_id_t et imdes2_image_descriptor_id_t descripteurs à comparer, similarity_int score de similarité
* \return int, code d'erreur
*/
int compare_image_descriptor(image_descriptor_t imdes1_image_descriptor_t,image_descriptor_t imdes2_image_descriptor_t, float* similarity_float);
