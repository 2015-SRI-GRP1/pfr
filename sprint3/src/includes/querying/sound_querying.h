#ifndef SRI_SOUND_QUERYING_H
#define SRI_SOUND_QUERYING_H
/**
* \file sound_indexing.h
* \brief Fonctions utilisées pour la recherche sur les sons des sons
* \author Molina Serge
* \version 1.0
* \date 18 Décembre 2015
*/


#include "../util.h"
#include "../sound_types.h"
int compare_sound_descriptor_t(sound_descriptor_t* descriptor_a, sound_descriptor_t* descriptor_b, double* score);


#endif
