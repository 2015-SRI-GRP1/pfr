#include <math.h>
#include "../util.h"
#include "../sound_types.h"
#include "sound_querying.h"

double max(double val1, double val2){
	return val1>val2 ? val1 : val2;
}

int compare_sound_descriptor_t(sound_descriptor_t* descriptor_a, sound_descriptor_t* descriptor_b, double* score){
	if(descriptor_a->histograms.size<descriptor_b->histograms.size){
		compare_sound_descriptor_t(descriptor_b, descriptor_a, score);
	}

	int error, found;

	int histograms_amount_a = descriptor_a->histograms.size;
	int histograms_amount_b = descriptor_a->histograms.size;

	int window_size = descriptor_a->histograms_size;
	int window_levels = descriptor_a->histograms_levels;

	int* tmp_histogram_a;
	int* tmp_histogram_b;

	double max_diff=0;
	for(int i=0; i<=histograms_amount_a-histograms_amount_b; i++){
		for(int j=0; j<histograms_amount_b; j++){
			error=list_get(&descriptor_a->histograms, i+j, (void**)&tmp_histogram_a, &found);
			error=list_get(&descriptor_b->histograms, j, (void**)&tmp_histogram_b, &found);
			if(error || !found){
				break;
			}
			long diff_sum=0;
			for(int n=0; n<window_levels; n++){
				diff_sum+=abs(tmp_histogram_a[n]-tmp_histogram_b[n]);
			}
			max_diff=max(max_diff, diff_sum/(double)(window_levels*window_size));
		}
	}
	*score=(1.0-max_diff)*100.0;
	return 0;
}
