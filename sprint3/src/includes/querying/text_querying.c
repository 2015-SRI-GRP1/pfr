/*
* \File text_querying.c
* \brief Fichier contenant les fonction pour la recherche de texte
* \author Bilal El yassem
* \version 1.0
* \date 12 Janvier 2016
*/

#include "text_querying.h"
//#define DEBUG


int compare_text_descriptor(text_descriptor_t* descriptor1, text_descriptor_t* descriptor2, double* ptr_score_double) {
	int i,j;
	float cpt_float = 0;

	//On teste si les descripteurs sont définis
	if (descriptor1 == NULL) {
		return 1;
	}

	if (descriptor2 == NULL) {
		return 2;
	}

	//On teste en suite si de la mémoire a été allouée pour le score de ressemblance
	if (ptr_score_double == NULL) {
		ptr_score_double = (double*)malloc(sizeof(double));
	}

	//On calcule en suite le nombre de termes en commun des deux descripteurs
	for (i = 0; i < NB_TERM_MAX; i++) {
		for (j = 0; j < NB_TERM_MAX; j++) {
			if (wcscmp(descriptor1 -> list_term_t[i].word_wchar_t, descriptor2 -> list_term_t[j].word_wchar_t) == 0) { //Segfault
				cpt_float++;
			}
		}
	}

	//On calcule le score de similarité
	*ptr_score_double = cpt_float/NB_TERM_MAX;

	*ptr_score_double = *ptr_score_double * 100;

	return 0;
}

#ifdef DEBUG
int main(void) {
	setlocale(LC_CTYPE, "fr_FR.UTF-8");

	double* ptr_similarity;
	text_descriptor_t* ptr_descriptor1 = (text_descriptor_t*)malloc(sizeof(text_descriptor_t));
	text_descriptor_t* ptr_descriptor2 = (text_descriptor_t*)malloc(sizeof(text_descriptor_t));
	text_descriptor_t* ptr_descriptor3 = (text_descriptor_t*)malloc(sizeof(text_descriptor_t));
	text_descriptor_t* ptr_descriptor4 = (text_descriptor_t*)malloc(sizeof(text_descriptor_t));

	FILE* input_fd = fopen("index.txt","r");

	text_descriptor_from_file(input_fd, ptr_descriptor1);
	text_descriptor_from_file(input_fd, ptr_descriptor2);
	text_descriptor_from_file(input_fd, ptr_descriptor3);
	text_descriptor_from_file(input_fd, ptr_descriptor4);

	compare_text_descriptor(ptr_descriptor1, ptr_descriptor2, ptr_similarity);
	wprintf(L"\n%lf", *ptr_similarity); //Identiques

	compare_text_descriptor(ptr_descriptor1, ptr_descriptor3, ptr_similarity);
	wprintf(L"\n%lf", *ptr_similarity); //Disjoints

	compare_text_descriptor(ptr_descriptor3, ptr_descriptor4, ptr_similarity);
	wprintf(L"\n%lf", *ptr_similarity); //Similaires

	free(ptr_descriptor1);
	free(ptr_descriptor2);
	free(ptr_descriptor3);
	free(ptr_descriptor4);

	return 0;
}
#endif
