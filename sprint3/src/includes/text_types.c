/*
* \File text_types.c
* \brief Fichier contenant les fonction pour l'indexiation
* \author Veyssiere Bastien
* \version 1.0
* \date 7 décembre 2015
*/


#include <wchar.h>
#include <stdio.h>
#include <stdlib.h>
#include "text_types.h"


/**
* \fn int text_descriptor_to_file(FILE * output_fd, text_descriptor_t * descriptor)
* \brief Ajoute un descripteur texte dans le fichier liste descripteur
* \param FILE *  : fichier dans lequel on écrit les descripteur
* \		 text_descriptor_t * :  descripteur a ecrire dans le fichier
* \return rien
*/
int text_descriptor_to_file(FILE * output_fd, text_descriptor_t * descriptor){
	int i;
	fwprintf(output_fd, L"%ls %d %d\n", descriptor->id_wchar_t, descriptor->word_file_int,descriptor->word_numb_int);
	for (i =0 ; i < NB_TERM_MAX ; i++ ){
		fwprintf(output_fd, L"%ls %d  ", descriptor->list_term_t[i].word_wchar_t,descriptor->list_term_t[i].ocurrence_int);

	}
	wprintf(L"%ls ", descriptor->id_wchar_t);
	fwprintf(output_fd, L"\n");
	fflush(output_fd);
	return(0);
}

/**
* \fn int free_text_descriptor(text_descriptor_t * descriptor)
* \brief free un descripteur
* \param text_descriptor_t * :  descripteur a free
* \return rien
*/
int free_text_descriptor(text_descriptor_t * descriptor){
	int i;
	free(descriptor->id_wchar_t);
	for(i=0;i< NB_TERM_MAX;i++){
		free(descriptor->list_term_t[i].word_wchar_t);
		free(descriptor->list_term_t);
	}
	free(descriptor);
	return(0);
}

/**
* \fn int text_descriptor_from_file(FILE* ptr_input_fd, text_descriptor_t* ptr_returned_descriptor_text_descriptor_t)
* \brief extrait un descripteur d'un fichier
* \param FILE* ptr_input_fd, text_descriptor_t* ptr_returned_descriptor_text_descriptor_t
* \return Code d'erreur
*/
int text_descriptor_from_file(FILE* ptr_input_fd, text_descriptor_t* ptr_returned_descriptor_text_descriptor_t) {

	//Déclaration des variables
	int read;
	wchar_t* ptr_descriptor_id_wchar_t;			//Chaine de caractères contenant l'identifiant du descripteur
	wchar_t* ptr_word_wchar_t;					//Chaine de caractères contenant le mot courant
	one_term_t* ptr_descriptor_tab_one_term_t;	//Tableau contenant les termes du descripteur retourné
	int* ptr_nb_mots_int;						//Entier contenant le nombre de mots du descripteur
	int* ptr_nb_termes_int;						//Entier contenant le nombre de termes du descripteur
	int* ptr_nb_occurences_int;					//Entier contenant le nombre d'occurences du mot courant


	//Allocation de la mémoire
	ptr_descriptor_id_wchar_t = (wchar_t*)calloc(20+1, sizeof(wchar_t));
	ptr_word_wchar_t = (wchar_t*)calloc(200+1, sizeof(wchar_t));
	ptr_descriptor_tab_one_term_t = (one_term_t*)calloc(NB_TERM_MAX, sizeof(one_term_t));
	ptr_nb_mots_int = (int*)malloc(sizeof(int));
	ptr_nb_termes_int = (int*)malloc(sizeof(int));
	ptr_nb_occurences_int = (int*)malloc(sizeof(int));



	//Tests
	//On vérifie si le file descriptor est bien initialisé
	if (ptr_input_fd == NULL){
		return 1;
	}

	//On vérifie si le descripteur de texte a été initialisé
	if (ptr_returned_descriptor_text_descriptor_t == NULL){
		return 2;
	}

	//On tente de lire l'identifiant du descripteur
	read = fwscanf(ptr_input_fd,  L"%ls", ptr_descriptor_id_wchar_t);
	if(read != 1){
		return EOF;
	}
	//On vérifie si le descripteur a le bon format
	if (!(ptr_descriptor_id_wchar_t[0] == L't') || !(ptr_descriptor_id_wchar_t[1] == L'x') || !(ptr_descriptor_id_wchar_t[2] == L't')) {
		return 3;
	}



	//Traitement
	//On ajoute l'identifiant en mémoire
	ptr_returned_descriptor_text_descriptor_t -> id_wchar_t = ptr_descriptor_id_wchar_t;

	//On tente de lire le nombre de mots du descripteur
	fwscanf(ptr_input_fd,  L"%d", ptr_nb_mots_int);

	//On tente en suite de l'ajouter en mémoire
	ptr_returned_descriptor_text_descriptor_t -> word_file_int = *ptr_nb_mots_int;

	//On tente de lire le nombre de termes du descripteur
	fwscanf(ptr_input_fd,  L"%d", ptr_nb_termes_int);

	//On tente en suite de l'ajouter en mémoire
	ptr_returned_descriptor_text_descriptor_t -> word_numb_int = *ptr_nb_termes_int;

	//On ajoute ensuite un à un les termes en mémoire
	for (int i = 0; i < NB_TERM_MAX; i++) {
		fwscanf(ptr_input_fd,  L"%ls", ptr_word_wchar_t);
		fwscanf(ptr_input_fd,  L"%d", ptr_nb_occurences_int);

		ptr_descriptor_tab_one_term_t[i].word_wchar_t = wcsdup(ptr_word_wchar_t);
		ptr_descriptor_tab_one_term_t[i].ocurrence_int = *ptr_nb_occurences_int;
	}

	//On ajoute le tableau de termes au descripteur
	ptr_returned_descriptor_text_descriptor_t -> list_term_t = ptr_descriptor_tab_one_term_t;

	return 0;
}
