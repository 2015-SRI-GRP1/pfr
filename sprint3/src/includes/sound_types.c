#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include "util.h"
#include "wchar.h"
#include "sound_types.h"


int sound_descriptor_init(sound_descriptor_t* descriptor){
	int error;
	int found=1;
	wchar_t* parameter_wchar=(wchar_t*)"\0";
	char parameter_char[1024];


	generate_descriptor_id((wchar_t*)L"snd", &descriptor->id);


	error = config_get((wchar_t*)L"window_size", &parameter_wchar, &found);

	if(!found || error){
		wprintf(L"window_size not found\n");
		return ERROR_PARAMETER_NOT_FOUND;
	}
	wstring_to_string(parameter_wchar, parameter_char);
 	descriptor->histograms_size = atoi(parameter_char);


	error = config_get((wchar_t*)L"window_levels", &parameter_wchar, &found);
	if(!found || error){
		wprintf(L"window_levels not found\n");
		return ERROR_PARAMETER_NOT_FOUND;
	}
	wstring_to_string(parameter_wchar, parameter_char);
 	descriptor->histograms_levels = atoi(parameter_char);

	list_init(&descriptor->histograms, descriptor->histograms_levels*sizeof(int));

	return 0;
}

int read_header(FILE* descriptor_fp, sound_descriptor_t* descriptor, int* histograms_amount){
	int read=fwscanf(descriptor_fp, L"%ls %d %d %d\n", descriptor->id, &descriptor->histograms_size, &descriptor->histograms_levels, histograms_amount);
	if(read!=4){
		return EOF;
	}
	return 0;
}

int sound_descriptor_from_file(FILE* descriptor_fp, sound_descriptor_t* descriptor){
	int error;

	descriptor->id=(wchar_t*) calloc(1024, sizeof(wchar_t));
	list_init(&descriptor->histograms, sizeof(int*));

	int histograms_amount=0;

	error=read_header(descriptor_fp, descriptor, &histograms_amount);
	if(error==EOF){
		return EOF;
	}
	int current_value;
	int* histogram=NULL;
	for(int current_histogram=0; current_histogram<histograms_amount; current_histogram++){
		histogram = (int*) calloc(descriptor->histograms_levels, sizeof(int));
		for(int current_level=0; current_level<descriptor->histograms_levels; current_level++){
			fwscanf(descriptor_fp, L"%d", &current_value);
			histogram[current_level]=current_value;
		}
		list_add(&descriptor->histograms, histogram);

	}
	return 0;
}

int sound_descriptor_to_file(FILE* output_fd, sound_descriptor_t* descriptor){
	int error, found;
	int window_size = descriptor->histograms_size;
	int window_levels = descriptor->histograms_levels;

	fwprintf(output_fd, L"%ls %d %d %d\n", descriptor->id, window_size, window_levels, descriptor->histograms.size);

	int* tmp_histogram;
	for(size_t n=0; n<descriptor->histograms.size; n++){

		error=list_get(&descriptor->histograms, n, (void**)&tmp_histogram, &found);
		if(error || !found){
			break;
		}
		for(int i=0; i<window_levels; i++){
			fwprintf(output_fd, L"%d ", tmp_histogram[i]);
		}
		fwprintf(output_fd, L"\n");
	}
	fwprintf(output_fd, L"\n");

	return 0;
}


int sound_descriptor_free(sound_descriptor_t* descriptor){
	free(descriptor->id);
	return list_free(&descriptor->histograms);
}
