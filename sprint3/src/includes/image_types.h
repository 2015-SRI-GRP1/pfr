/**
* \file image_types.h
* \brief Description des types et des fonctions liées au fichiers de type image
* \author Deguilhem Valentin
* \version 1.3
* \date 30 Décembre 2015
*/


#ifndef SRI_IMAGE_TYPES_H
#define SRI_IMAGE_TYPES_H
#include "util.h"
#include <stdio.h>
#include <string.h>
#define IMAGE_QUANT_LVL 64
/**
* \struct descripteur_image_t image_types.h
* \brief Structure décrivant la composition des descripteurs d'image
*/
typedef struct image_descriptor_t{
	descriptor_id_t id_image_descriptor_id_t;
	int* histogram_int;
}image_descriptor_t;


/**
* \fn int quantification_image
* \brief fonction de quantification.
* \param r_int, g_int, b_int : les trois valeurs lues dans le fichier image.txt, int* ptr_quantified_value_int : valeur quantifiée.
* \return int, code d'erreur
*/
int quantification_image(int r_int, int g_int, int b_int, int* ptr_quantified_value_int);

/**
* \fn int init_image_descriptor
* \brief fonction d'initialisation descripteur.
* \param  ptr_imdes_image_descriptor_t descripteur à initialiser.
* \return int, code d'erreur
*/
int init_image_descriptor(image_descriptor_t* ptr_imdes_image_descriptor_t);



/**
* \fn int image_descriptor_to_file
* \brief fonction de création de fichier descripteur.
* \param  output_fd fichier créé, descripteur écrit dans le fichier.
* \return int, code d'erreur
*/
int image_descriptor_to_file(FILE* output_fd, image_descriptor_t* descriptor);

/**
* \fn int image_descriptor_free
* \brief libération de la mémoire allouée au descripteur.
* \param  descripteur.
* \return int, code d'erreur
*/
int image_descriptor_free(image_descriptor_t* ptr_imdes_image_descriptor_t);
#endif
