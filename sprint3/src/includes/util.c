#include <stdio.h>
#include <wchar.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"


static int AVAILABLE_DOCUMENT_ID=0;

static dictionnary_t CONFIGURATION;

/**
* \struct association_t util.h
* \brief Structure permettant d'associer une chaîne de caractères à une autre
*/
typedef struct association_t{
	wchar_t* key;
	void* value;
} association_t;

int list_init(linked_list_t* list, size_t data_size){
	list->size=0;
	list->data_size=data_size;
	list->head=NULL;
	list->tail=NULL;
	return 0;
}

int list_init_with_size(linked_list_t* list, size_t data_size, size_t size){
	list_init(list, data_size);
	while(list->size != size){
		list_add(list, NULL);
	}

	return 0;
}

void* memdup(const void* d, size_t s) {
   void* p;
   return ((p = malloc(s))?memcpy(p, d, s):NULL);
}


int list_add(linked_list_t* list, void* value){

	list_node_t* node = (list_node_t*) malloc(sizeof(list_node_t));
	node->data=value;
	node->next=NULL;

	if(list->tail==NULL){
		list->head=node;
		list->tail=node;
		list->size++;
	} else{
		list->tail->next=node;
		list->tail=node;
		list->size++;
	}
	return 0;
}


int list_get(linked_list_t* list, size_t index, void** value, int* found){
	int n=index;
	list_node_t* node = list->head;

	// Recherche de valeurs de la liste
	while(node!=NULL && n>0){
		node=node->next;
		n--;
	}
	// Vérification du succès de la recherche
	if(node!=NULL){
		*value=node->data;
		*found=1;
	} else{
		*found=0;
	}
	return 0;
}

int list_free(linked_list_t* list){
	list_node_t* node = list->head;
	list_node_t* tmp_node;

	// Recherche de valeurs de la liste
	while(node!=NULL){
		tmp_node=node;
		node=tmp_node->next;
		free(tmp_node->data);
		free(tmp_node);
	}
	return 0;
}



int dictionnary_init(dictionnary_t* dic){
	list_init(&dic->associations, sizeof(association_t));
	dic->size=dic->associations.size;
	return 0;
}



int dictionnary_add(dictionnary_t* dic, wchar_t* key, void* value){
	association_t* association = (association_t*) malloc(sizeof(association_t));
	association->key = wcsdup(key);
	association->value = value;

	list_add(&dic->associations, association);
	dic->size=dic->associations.size;


	return 0;
}

int dictionnary_add_primitive(dictionnary_t* dic, wchar_t* key, const void* value, size_t primitive_size){
	void* value_ptr=malloc(primitive_size);
	memcpy(value_ptr, value, primitive_size);
	return dictionnary_add(dic, key, value_ptr);
}


int dictionnary_get(dictionnary_t* dic, wchar_t* key, void** value, int* found){
	list_node_t* node = dic->associations.head;

	// Recherche du paramètre jusqu'à épuisement de la liste des paramètres
	while(node!=NULL && wcscmp(key, ((association_t*)node->data)->key)!=0){
		node=node->next;
	}

	// Vérification du succès de la recherche
	if(node!=NULL){
		// Copie du resultat dans le tampon du resultat
		*value=((association_t*)node->data)->value;
		*found=1;
	} else{
		*found=0;
	}
	return 0;
}

int dictionnary_get_association(dictionnary_t* dic, int index, wchar_t* key, void** value, int* found){
	int n=index;
	list_node_t* node = dic->associations.head;

	// Recherche de valeurs de la liste
	while(node!=NULL && n>0){
		node=node->next;
		n--;
	}



	// Vérification du succès de la recherche
	if(node!=NULL){
		// Copie du resultat dans le tampon du resultat
		wcscpy(key, ((association_t*)node->data)->key );
		*value=((association_t*)node->data)->value;
		*found=1;
	} else{
		*found=0;
	}
	return 0;
}


int dictionnary_load(dictionnary_t* dic, char* dic_filename){
	wchar_t line[512];
	wchar_t key[1024];
	wchar_t value[1024];

	// Ouverture du fichier en mode lecture seule
	FILE* fp = fopen(dic_filename,"r"); // read mode
	if( fp == NULL ){
		perror("Erreur d'ouverture du dictionnaire.\n");
		return ERROR_FILE_OPEN;
	}

	// Parcours des paramètres du fichier
	while (fwscanf(fp, L"%l[^\n]\n", line) == 1){
		if(line[0]!=L'#'){
			swscanf(line, L"%l[^=]=%l[^\n]\n", key, value);
			dictionnary_add(dic, wcsdup(key), wcsdup(value));
		}
	}

	// Fermeture du fichier
	fclose(fp);

	return 0;

}


int dictionnary_load_from_file(dictionnary_t* dic, FILE* dic_fp){
	wchar_t line[512];
	wchar_t key[1024];
	wchar_t value[1024];

	// Parcours des paramètres du fichier
	while (fwscanf(dic_fp, L"%l[^\n]\n", line) == 1){
		if(line[0]!=L'#'){
			swscanf(line, L"%l[^=]=%l[^\n]\n", key, value);
			dictionnary_add(dic, wcsdup(key), wcsdup(value));
		}
	}

	return 0;
}

int dictionnary_free(dictionnary_t* dic){
	return list_free(&dic->associations);
}


int config_add(wchar_t* key, wchar_t* value){
	return dictionnary_add(&CONFIGURATION, key, value);
}


int config_get(wchar_t* key, wchar_t** value, int* found){
	return dictionnary_get(&CONFIGURATION, key, (void**)value, found);
}



int config_load(char* config_filename){
	int error, found;
	wchar_t key[1024];
	wchar_t* value=L"";
	dictionnary_init(&CONFIGURATION);
	dictionnary_load(&CONFIGURATION, config_filename);

	wprintf(L"Configuration : \n");
	for(size_t n=0; n<CONFIGURATION.associations.size; n++){
		error=dictionnary_get_association(&CONFIGURATION, n, key, (void**)&value, &found);
		if(!error && found){
			wprintf(L"%ls=\"%ls\"\n", key, value);
		}
	}
	wprintf(L"\n");

	return 0;

}



int generate_descriptor_id(wchar_t prefix[3], descriptor_id_t* id){
	*id=(wchar_t*) calloc(DESCRIPTOR_ID_LENGTH+1, sizeof(wchar_t));
	swprintf(*id, DESCRIPTOR_ID_LENGTH, L"%ls%016d", prefix, AVAILABLE_DOCUMENT_ID++);
	return 0;
}

int wstring_to_string(wchar_t* in, char* out){
	wcstombs( out, in, wcslen(in) );
	out[wcslen(in)]='\0';
	return 0;
}

int clean_string(wchar_t* string_wchar_t){
	int i=0;
	int j=0;
	int test=0;

	// test si string_wchar_t est vide
	if (string_wchar_t==NULL){
		return(0);
	}
	else{
		i=0; //sert pour parcourir la chaine de caractère
		//Parcours la chaine de wchar_t
		while (string_wchar_t[i]!='\0'){
			//Traitement des Majuscules
			if(string_wchar_t[i]>='A'&& string_wchar_t[i]<='Z'){
				//pour changer une maj en min il faut lui ajouter 32h soit le code ascii de ' '
				string_wchar_t[i] += ' ';
			}
			else {
				j=0;
				test=0;
				//Parcours le tableau des caracteres spéciaux
				while (carac_spe_wchar_t[j]!='\0'){
					if(carac_spe_wchar_t[j]==string_wchar_t[i]){
						test=1;
					}
					j++;
				}
			    if (test!=1){
			    	//Traitement de la ponctuation
					if(!((string_wchar_t[i]>='a'&& string_wchar_t[i]<='z')||(string_wchar_t[i]>='0'&& string_wchar_t[i]<='9'))){
						//remplace la ponctuation par ' '
						string_wchar_t[i]= ' ';
					}
				}
			}
			i++;
		}
		return(1);
	}
}

/**
* \Fonction quick_sort
* \brief Fonction de trie rapide des tableau de type one_term_t
* \author Bastien Veyssière
* \version 1.0
* \date 28 Decembre 2015
*/


void exchange (term_t tableau , int a, int b) {

	one_term_t buff;
	buff.word_wchar_t=tableau[a].word_wchar_t;
	buff.ocurrence_int = tableau[a].ocurrence_int;

	tableau[a].word_wchar_t = tableau[b].word_wchar_t;
	tableau[a].ocurrence_int = tableau[b].ocurrence_int;

	tableau[b].word_wchar_t = buff.word_wchar_t;
	tableau[b].ocurrence_int = buff.ocurrence_int;


}

int quick_sort (term_t tableau, int start, int end){

    int left = start-1;
    int right = end+1;
    const int pivot = tableau[start].ocurrence_int;

    if(start >= end)
        return(0);

    /* On parcourt le tableau, une fois de right à left, et une
       autre de left à right, on permute les  éléments mal placés.
       Si les deux parcours se croisent, on arrête. */
    while(1){

        do{
       	 right--;
        }while(tableau[right].ocurrence_int > pivot);

        do{
        	left++;
        }while(tableau[left].ocurrence_int< pivot);

        if(left < right)
            exchange(tableau, left, right);
        else break;
    }

	// Methode recursive pour les deux sous-tableaux
	quick_sort(tableau, start, right);
    quick_sort(tableau, right+1, end);
    return(1);
}

int cocktail_sort (one_term_t* ptr_tab_one_term_t, int end){

	int switched = 1;	//Booléen permettant de savoir si des variables ont étés échangées
	int i;				//Variable d'itération

	//On teste si le traitement est possible
	if (end < 0) { //Tableau de taille négative
		return 1;
	}
	if (end < 2) { //Tableau trop petit pour être trié
		return 2;
	}


	do {
		switched = 0;

		for (i = 0; i < end -1; i++) {
			if (ptr_tab_one_term_t[i].ocurrence_int < ptr_tab_one_term_t[i+1].ocurrence_int) {
				fflush(stdout);
				exchange(ptr_tab_one_term_t , i, i+1);
				switched = 1;
			}
		}

		for (i = (end -2); i >= 0; i--) {
			if (ptr_tab_one_term_t[i].ocurrence_int < ptr_tab_one_term_t[i+1].ocurrence_int) {
				fflush(stdout);
				exchange(ptr_tab_one_term_t , i, i+1);
				switched = 1;
			}
		}
	}
	while(switched);
	return 0;
}
