#ifndef SRI_UTIL_H
#define SRI_UTIL_H
/**
* \file util.h
* \brief Entêtes de fonctions et types utiles pour le SRI
* \author Serge Molina, Bastien Veyssière
* \version 1.2
* \date 21 Décembre 2015
*/

#include <stdio.h>
#include <wchar.h>

#include "text_types.h"

#define ERROR_NULL_POINTER 1
#define ERROR_FILE_OPEN 2
#define ERROR_PARAMETER_NOT_FOUND 3
#define ERROR_ARGC 4
#define ERROR_INVALID_DATA 5


#define DESCRIPTOR_ID_PREFIX_LENGTH 3
#define DESCRIPTOR_ID_NUMBER_LENGTH 16
#define DESCRIPTOR_ID_LENGTH (DESCRIPTOR_ID_PREFIX_LENGTH+DESCRIPTOR_ID_NUMBER_LENGTH+1)

#define SIZE_TAB_SPE 9
#define SIZE_TAB_WOR 51
#define SIZE_TAB_REMOVE 9

/**
* \struct list_node_t util.h
* \brief Structure permettant de stocker un élément d'une liste chaînée
*/
typedef struct list_node_t{
	void* data;
	struct list_node_t* next;
} list_node_t;


/**
* \struct linked_list_t util.h
* \brief Structure permettant de stocker un liste chaînée
*/
typedef struct linked_list_t{
	size_t size;
	size_t data_size;
	list_node_t* head;
	list_node_t* tail;
} linked_list_t;



typedef struct dictionnary_t{
	size_t size;
	linked_list_t associations;
} dictionnary_t;

typedef wchar_t* descriptor_id_t;

/**
* \Constante static wchar_t carac_spe_wchar_t[SIZE_TAB_SPE] util.h
* \brief Tableau contenant les caractères spéciaux
*/
static const wchar_t carac_spe_wchar_t[SIZE_TAB_SPE]={L'é',L'è',L'ê',L'ë',L'à',L'ù',L'ï',L'ç',L'\0'};

/**
* \Constante static wchar_t word_remove_wchar_t[SIZE_TAB_SPE] util.h
* \brief Tableau contenant des mots à supprimer faissant plus de 3 lettres
*/
static const wchar_t* word_remove_wchar_t[SIZE_TAB_WOR]={L"alors",L"aussi",L"autre",L"autres",L"avait",L"avaient",L"avec",L"avoir",L"cela",L"cette",L"chaque",L"chez",L"comme",L"dans",L"depuis",
L"devrais",L"devrait",L"donc",L"dont",L"elle",L"était",L"étaient",L"etre",L"fait",L"faire",L"faut",L"leur",L"leurs",L"lors",L"mais",L"moins",
L"notre",L"nous",L"parce",L"pendant",L"peut",L"plus",L"pour",L"quand",L"quelque",L"quelques",L"sans",L"selon",L"sont",L"soit",L"soient",
L"surtout",L"tous",L"tout",L"votre",L"vous"};


/**
* \fn int list_init
* \brief Fonction d'initialisation de liste.
* \param list: liste à initialiser, data_size: taille d'un élément de la liste
* \return int, code d'erreur
*/
int list_init(linked_list_t* list, size_t data_size);
int list_init_with_size(linked_list_t* list, size_t data_size, size_t length);
int list_add(linked_list_t* list, void* value);
int list_get(linked_list_t* list, size_t index, void** value, int* found);
int list_set(linked_list_t* list, size_t index, void* value);
int list_free(linked_list_t* list);

int dictionnary_init(dictionnary_t* dic);
int dictionnary_load(dictionnary_t* dic, char* config_filename);
int dictionnary_load_from_file(dictionnary_t* dic, FILE* dic_fp);
int dictionnary_add(dictionnary_t* dic, wchar_t* key, void* value);
int dictionnary_add_primitive(dictionnary_t* dic, wchar_t* key, const void* value, size_t primitive_size);
int dictionnary_get(dictionnary_t* dic, wchar_t* key, void** value, int* found);
int dictionnary_get_association(dictionnary_t* dic, int index, wchar_t* key, void** value, int* found);
int dictionnary_free(dictionnary_t* dic);


int config_load(char* config_filename);
int config_get(wchar_t* key, wchar_t** value, int* found);
int config_add(wchar_t* key, wchar_t* value);

int generate_descriptor_id(wchar_t prefix[3], descriptor_id_t* id);


/**
* \fn int clean_string
* \brief Fonction qui supprime les caractères indesirables d'une string
* \param <string_wchar_t>: chaine de caractère en wchar_t
* \return int,
* \	          0 : pointeur NULL
* \	          1 : traitement effectué
*/
int clean_string(wchar_t* string_wchar_t);

int wstring_to_string(wchar_t* in, char* out);

/**
* \fn int clean_string
* \brief Fonction qui echange les cases d'un tableau de types term_t
* \param <term_t tableau[]>: tableau de types term_t
* \ <a>: indice d'un tableau de types term_t
* \ <b>: indice d'un tableau de types term_t
* \return void
*/
void exchange (term_t tableau, int a, int b);

/**
* \fn int quick_sort(term_t tableau[], int start, int end)
* \brief Trie un tableau de type one_term_t
* \param term_t tableau[] : tableau à trier
* \		 int a : indice debut
* \		 int b : indice fin
* \return codes d'erreurs
* \		  0 : tableau vide
* \       1 : traitement effectué
*/
int quick_sort (term_t tableau, int start, int end);

/**
* \fn int cocktail_sort(term_t tableau[], int end)
* \brief Trie un tableau de type one_term_t
* \param term_t tableau[] : tableau à trier
* \		 int end : indice fin
* \return codes d'erreurs
* \		  0 : tableau vide
* \       1 : traitement effectué
*/
int cocktail_sort (one_term_t* ptr_tab_one_term_t, int end);

#endif
