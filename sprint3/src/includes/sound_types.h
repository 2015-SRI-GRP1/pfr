#ifndef SRI_SOUND_TYPES_H
#define SRI_SOUND_TYPES_H
/**
* \File sound_types.h
* \brief Ce fichier définit les structures utilisées dans l'indexation et la recherche de fichiers sons.
* \author Molina Serge
* \version 1.0
* \date 22 Decembre 2015
*/

#include "util.h"
#include "wchar.h"

/**
* \struct descriptor_sound_t sound_types.h
* \brief Structure définissant un descripteur texte
*/
typedef struct {
	descriptor_id_t id;
	linked_list_t histograms;
	int histograms_size;
	int histograms_levels;
}sound_descriptor_t;


int sound_descriptor_init(sound_descriptor_t* descriptor);

/**
* \fn int sound_descriptor_from_file
* \brief fonction de chargement d'un descripteur de son depuis un fichier
* \param  sound_fp: pointeur vers le fichier contenant le descripteur
* \param  descriptor: descripteur à remplir
* \return int, code d'erreur
*/
int sound_descriptor_from_file(FILE* descriptor_fp, sound_descriptor_t* descriptor);
int sound_descriptor_to_file(FILE* output_fd, sound_descriptor_t* descriptor);
int sound_descriptor_free(sound_descriptor_t* descriptor);


#endif // SRI_SOUND_TYPES_H
