/**
* \file image_indexation.h
* \brief Fonctions utilisées pour l'indexation
* \author Deguilhem Valentin
* \version 1.2
* \date 30 Décembre 2015
*/

#include "../util.h"
#include "../image_types.h"
#include <stdio.h>
#include <string.h>


/**
* \fn int descriptor_image_creation
* \brief fonction de création d'un descripteur image
* \param  ptr_adress_file_char
* \return int, code d'erreur
*/

int image_descriptor_creation(FILE* fp, image_descriptor_t* ptr_imdes_image_descriptor_t);
