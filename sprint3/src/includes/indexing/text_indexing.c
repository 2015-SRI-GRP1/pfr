/**
* \file text_indexing.c
* \brief Fonctions utilis�es pour l'indexation
* \author Bilal El yassem
* \version 1.0
* \date 28 D�cembre 2015
*/

#include <stdio.h>
#include <locale.h>
#include "text_indexing.h"
#include "../util.h"
#include "../text_types.h"

//#define DEBUG

/**
* \fn int text_descriptor_creation
* \brief fonction de cr�ation d'un descripteur texte
* \param  ptr_adress_file_char, returned_descriptor_text_descriptor_t
* \return int, code d'erreur
*/
int text_descriptor_creation(FILE* ptr_file_to_describe_file, FILE* ptr_output_file_file, text_descriptor_t* ptr_returned_descriptor_text_descriptor_t) {

	//D�claration des variables
	wchar_t* ptr_xml_wchar_t;			//Chaine de caract�res contenant le format du fichier
	wchar_t* ptr_phrase_wchar_t;		//Chaine de caract�res contenant la phrase en cours de traitement
	wchar_t* ptr_traitement_wchar_t;	//Chaine de caract�res contenant la phrase en cours de traitement
	int scan_return_code_int;			//Entier contenant le code de retour de la fonction de lecture
	one_term_t* ptr_tab_one_term_t;		//Tableau contenant les termes du fichier � indexer
	one_term_t* ptr_descriptor_tab_one_term_t;		//Tableau contenant les termes du fichier � indexer
	descriptor_id_t ptr_id_descriptor_id_t;		//Identificateur du nouveau descripteur



	//Initialisation des variables
	ptr_xml_wchar_t = (wchar_t*)calloc(100+1, sizeof(wchar_t));
	ptr_phrase_wchar_t = (wchar_t*)calloc(K_MAX_NB_WCHAR_PER_STRING+1, sizeof(wchar_t));
	ptr_traitement_wchar_t = (wchar_t*)calloc(K_MAX_NB_WCHAR_PER_STRING+1, sizeof(wchar_t));
	ptr_tab_one_term_t = (one_term_t*)calloc(NB_TERM, sizeof(one_term_t));
	ptr_descriptor_tab_one_term_t = (one_term_t*)calloc(NB_TERM_MAX, sizeof(one_term_t));
	ptr_id_descriptor_id_t = (wchar_t*)calloc(DESCRIPTOR_ID_LENGTH, sizeof(wchar_t));




	//On teste si le pointeur vers le fichier est initialis�
	if (ptr_file_to_describe_file == NULL) {
		return 1;
	}

	//On teste si le pointeur vers le descripteur est initialis�
	if (ptr_returned_descriptor_text_descriptor_t == NULL) {
		return 3;
	}

	ptr_returned_descriptor_text_descriptor_t->word_numb_int=0;
	ptr_returned_descriptor_text_descriptor_t->word_file_int =0;




	//On tente de lire la premi�re balise du fichier qui contient l'encodage pour v�rifier qu'il s'agit du bon type de fichier
	scan_return_code_int = fwscanf(ptr_file_to_describe_file, L"<?%l[^?>]?>\n", ptr_xml_wchar_t);

	//On teste si la lecture a correctement eu lieu
	if (scan_return_code_int == EOF) {	//Test de fin de fichier
		return 4;
	}
	if (scan_return_code_int == 0) {	//Test d'echec de lecture
		return 5;
	}

	//On teste si le fichier correspond bien au standard XML pour ce projet
	if (wcscmp(ptr_xml_wchar_t, L"xml version=\"1.0\" encoding=\"iso-8859-1\"") != 0) {
		return 6;
	}





	//On lit d�sormais le fichier phrase par phrase pour cr�er le descripteur
	while((scan_return_code_int = fwscanf(ptr_file_to_describe_file,  L"%l[^\n]\n", ptr_phrase_wchar_t)) !=-1) {
		if(scan_return_code_int==1){
			//On n�toie le d�but et la fin de chaque phrase
			swscanf(ptr_phrase_wchar_t, L"<phrase>%l[^<]</phrase>", ptr_traitement_wchar_t);
			clean_string(ptr_traitement_wchar_t);
			string_processing(ptr_traitement_wchar_t, ptr_returned_descriptor_text_descriptor_t, ptr_tab_one_term_t);
		}
	}


	//On trie la liste de mots du descripteur
	cocktail_sort(ptr_tab_one_term_t, ptr_returned_descriptor_text_descriptor_t -> word_numb_int);

	//On extrait de la liste de mots les NB_TERM_MAX plus fr�quents.
	for (int i = 0; i < NB_TERM_MAX; i++) {
		ptr_descriptor_tab_one_term_t[i] = ptr_tab_one_term_t[i];
	}

	//On ajoute la liste des mots les plus fr�quents au descripteur
	ptr_returned_descriptor_text_descriptor_t -> list_term_t = ptr_descriptor_tab_one_term_t;

	//On g�n�re un identifiant pour le descripteur
	generate_descriptor_id(L"txt", &ptr_id_descriptor_id_t);

	//On teste si l'identifiant cr�� est valide
	if (ptr_id_descriptor_id_t == NULL) {
		return 7;
	}

	//S'il est valide on l'ajoute au descripteur
	ptr_returned_descriptor_text_descriptor_t -> id_wchar_t = ptr_id_descriptor_id_t;



	#ifdef DEBUG
	//Tests
	//On affiche l'identifiant
	wprintf(L"%ls\n", ptr_returned_descriptor_text_descriptor_t -> id_wchar_t);

	//On affiche les termes les plus courants
	for (int i = 0; i < NB_TERM_MAX; i++) {
		wprintf(L"%ls", ptr_returned_descriptor_text_descriptor_t -> list_term_t[i].word_wchar_t);
		wprintf(L" : %d\n" ,ptr_returned_descriptor_text_descriptor_t -> list_term_t[i].ocurrence_int);
	}

	//On affiche le nombre de mots dans le texte
	wprintf(L"Nb mots %d\n", ptr_returned_descriptor_text_descriptor_t -> word_file_int);

	//On affiche le nombre de termes dans le texte
	wprintf(L"Nb termes %d\n", ptr_returned_descriptor_text_descriptor_t -> word_numb_int);
	#endif





	free(ptr_xml_wchar_t);
	free(ptr_traitement_wchar_t);
	free(ptr_phrase_wchar_t);
	free(ptr_tab_one_term_t);
	return 0;
}

int string_processing(wchar_t* string_wchar_t, text_descriptor_t* inst_descriptor_t, one_term_t tab_one_term_t[]){

	wchar_t * token;
	wchar_t * etat;
	int i=0;
	int j=0;
	int test=0;

	token= wcstok(string_wchar_t,L" ",&etat);

	while(token!=NULL){

		//Augmente le nombre de mots contenu dans le fichier
		inst_descriptor_t->word_file_int ++;

		//verifie que le mot soit plus grand que SIZE_WORD_RM
		if (!(wcslen(token)<=SIZE_WORD_RM )){

			while(tab_one_term_t[i].word_wchar_t!=NULL){
				//si le mot est present dans le tableau tab_one_term_t rajoute une occurence
				if(wcscmp(tab_one_term_t[i].word_wchar_t, token)==0){
					tab_one_term_t[i].ocurrence_int ++;
					test=1;
				}

				i++;
			}
			//sinon rajoute le mot dans le tableau tab_one_term_t si ce n'est pas un mot a supprimer
			if(test==0){
			    //test si token n'est pas un mot interdit
				for (j=0;j<SIZE_TAB_WOR;j++){
					if(wcscmp(word_remove_wchar_t[j], token)==0){
						test=1;
					}
				}
			}
			//rajoute le mot dans le tableau tab_one_term_t si test vaut tjrs 0
			if(test==0){
				inst_descriptor_t->word_numb_int ++;
				tab_one_term_t[i].ocurrence_int=1;
				tab_one_term_t[i].word_wchar_t = wcsdup(token);
			}
		}
		i=0;
		test=0;
		token = wcstok(NULL,L" ",&etat);
	}
	return 0;
}

int word_dico(text_descriptor_t* descriptor, dictionnary_t* dic_principal){
	int found;
	int i;
	dictionnary_t* tmp_dic;

	//wchar_t* mot=(wchar_t*)malloc(1024*sizeof(wchar_t));


	for(i=0;i<NB_TERM_MAX;i++){
		dictionnary_get(dic_principal, descriptor->list_term_t[i].word_wchar_t, (void**)&tmp_dic, &found);
		if(found==0){
			tmp_dic=(dictionnary_t*)malloc(sizeof(dictionnary_t));
			dictionnary_init(tmp_dic);
			dictionnary_add(dic_principal, (wchar_t*)descriptor->list_term_t[i].word_wchar_t, tmp_dic);
		}

		dictionnary_add_primitive(tmp_dic,(wchar_t*) descriptor->id_wchar_t, &(int){descriptor->list_term_t[i].ocurrence_int}, sizeof(int));
		wprintf(L"%ls=>%ls=%d\n", descriptor->list_term_t[i].word_wchar_t,  descriptor->id_wchar_t, descriptor->list_term_t[i].ocurrence_int);
	}
	return 0;

}

#ifdef DEBUG
int main(void){
	dictionnary_t dico_inverse;
	dictionnary_t* tmp_dic;
	wchar_t mot[1024];
	wchar_t id[1024];
	int occurence, found;

	setlocale(LC_CTYPE, "fr_FR.UTF-8");
	text_descriptor_t descriptor_a;

	FILE* ptr_file_to_describe_file = fopen("data/Textes/Textes_UTF8/03-Des_chercheurs_parviennent__E0_r_E9g_E9n_E9rer_utf8.xml", "r");
	FILE* ptr_output_file_file = fopen("./index.txt", "w");

	text_descriptor_creation(ptr_file_to_describe_file, ptr_output_file_file, &descriptor_a);
	dictionnary_init(&dico_inverse);
	word_dico(&descriptor_a, &dico_inverse);
	/*word_dico(&descriptor_b, &dico_inverse);
	word_dico(&descriptor_c, &dico_inverse);
	word_dico(&descriptor_d, &dico_inverse);
	word_dico(&descriptor_e, &dico_inverse);
*/
	for(int i=0; i<dico_inverse.size; i++){
		dictionnary_get_association(&dico_inverse, i, mot, (void**)&tmp_dic, &found);
		if(found==1){
			wprintf(L"%ls\n", mot);
			for(int j=0; j<tmp_dic->size; j++){
				dictionnary_get_association(tmp_dic, j, id, (void**)&occurence, &found);

				if(found==1){
					wprintf(L"\t(%ls)=%d\n", id, occurence);
				}
			}
		}

	}

}
#endif
