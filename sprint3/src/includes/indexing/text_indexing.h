/**
* \file text_indexing.h
* \brief Fonctions utilisées pour l'indexation
* \author Bilal El yassem
* \version 1.2
* \date 28 Décembre 2015
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>
#include "../util.h"
#include "../text_types.h"


#define K_MAX_NB_WCHAR_PER_STRING 5000

////////////Fonctions
/**
* \fn int text_descriptor_creation
* \brief fonction de création d'un descripteur texte
* \param  ptr_file_to_describe_file, ptr_output_file_file, ptr_returned_descriptor_text_descriptor_t
* \return codes d'erreurs
*/
int text_descriptor_creation(FILE* ptr_file_to_describe_file, FILE* ptr_output_file_file, text_descriptor_t* ptr_returned_descriptor_text_descriptor_t);



int string_processing(wchar_t* string_wchar_t, text_descriptor_t* inst_descriptor_t,one_term_t tab_one_term_t[]);
