/**
* \File text_types.h
* \brief Ce fichier définit les structures utilisées dans l'indexation et la recherche de fichiers texte.
* \author Bastien Veyssière
* \version 1.1
* \date 17 Decembre 2015
*/
#ifndef SRI_TEXT_TYPES_H
#define SRI_TEXT_TYPES_H
#include <wchar.h>
#include <stdio.h>

////////////Constante
/**
* \Const NB_TERM 2000
* \brief Nombre de termes dans le texte
*/
#define NB_TERM 2000
/**
* \Const NB_TERM_MAX 20
* \brief Nombre max de termes dans le descripteur
*/
#define NB_TERM_MAX 10

/**
* \Const SIZE_WORD_RM 3
* \brief Taille des mots à enlever
*/
#define SIZE_WORD_RM 3

////////////Structures
/**
* \struct one_term_t text_types.h
* \brief Structure pour le stockage du corps du descripteur texte
*/
typedef struct {
    wchar_t* word_wchar_t;
    int ocurrence_int;
}one_term_t;

/**
* \Tab term_t text_types.h
* \brief Tableau de structure pour le stockage du corps du descripteur texte
*/
typedef one_term_t* term_t;
/**
* \struct descriptor_t text_types.h
* \brief Structure pour le stockage du descripteur texte
*/
typedef struct {
    wchar_t* id_wchar_t;
    term_t list_term_t;
    int word_file_int;
    int word_numb_int;
} text_descriptor_t;

/*
* \Fonction text_descriptor_to_file
* \brief Fonction qui ajoute un descripteur dans le fichier base_descripteur
* \author Veyssiere Bastien
* \version 1.0
* \date 7 janvier 2016
*/
int text_descriptor_to_file(FILE * output_fd, text_descriptor_t * descriptor);

/**
* \fn int free_text_descriptor(text_descriptor_t * descriptor)
* \brief free un descripteur
* \param text_descriptor_t * :  descripteur a free
* \return rien
*/
int free_text_descriptor(text_descriptor_t * descriptor);

/**
* \fn int text_descriptor_from_file
* \brief Extrait le premier descripteur de texte du fichier donné en paramètre
* \param ptr_input_fd, ptr_returned_descriptor_text_descriptor_t
* \return codes d'erreurs
*/
int text_descriptor_from_file(FILE* input_fd, text_descriptor_t* ptr_returned_descriptor_text_descriptor_t);

/**
* \fn int text_descriptor_from_file(FILE* ptr_input_fd, text_descriptor_t* ptr_returned_descriptor_text_descriptor_t)
* \brief extrait un descripteur d'un fichier
* \param FILE* ptr_input_fd, text_descriptor_t* ptr_returned_descriptor_text_descriptor_t
* \return Code d'erreur
*/
int text_descriptor_from_string(wchar_t* ptr_descriptor_wchar_t, text_descriptor_t* ptr_returned_descriptor_text_descriptor_t);
#endif // SRI_TEXT_TYPES
