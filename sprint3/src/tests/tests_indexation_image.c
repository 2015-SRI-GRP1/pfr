/**
* \file tests_indexation_image.c
* \brief test sur les fonctions d'indecation image
* \author Deguilhem Valentin
* \version 1.2
* \date 30 Décembre 2015
*/
#include"../includes/indexing/image_indexing.h"
#include<stdio.h>

void main(){
	image_descriptor_t imdes1;
	init_image_descriptor(&imdes1);
	image_descriptor_creation(" ", &imdes1);//Entrer le chemin d'un fichier pour tester
	int i,j;
	j = 0;
	for(i = 0; i < 64; i++){
		printf("%d  :  %d\n",i,imdes1.histogram_int[i]);
	}
	
}
