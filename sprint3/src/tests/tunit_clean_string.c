/**
* \File tunit_clean_string.c
* \brief Test unitaire de la fonction formating.c
* \author Bastien Veyssière
* \version 1.1
* \date 21 Decembre 2015
*/

/**
\codes d'erreurs
\	0 : chaine vide en paramètre
\	1 : traitement effectué
*/

#include "../includes/util.h"
//locale.h et setlocale servent à afficher les caractere speciaux dans le terminal
#include <locale.h>
void main(){
	setlocale(LC_CTYPE,"fr_FR.UTF-8");
	int ret1,ret2;
	wchar_t phrase1[100]=L"Laé Phr/Ase !a chèéàç ch,znov'(-è' ";
	wchar_t phrase2[100];
	//affichage de la phrase1
	wprintf(L"affichage de la phrase1: %ls\n",phrase1 );
	//traitement phrase1
	ret1=clean_string(phrase1);
	if (ret1==0){
		printf("ret==0: ERREUR\n");
	}else{
		printf("ret==1: \n");
	}
	wprintf(L"affichage de la phrase1 traduite: %ls\n",phrase1 );
	//traitement phrase2
	ret2=clean_string(phrase2);
	if (ret2==1){
		printf("ret==1: ERREUR\n");
	}else{
		printf("ret==0: \n");
	}



}
