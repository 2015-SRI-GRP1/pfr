﻿# Commentaires Doxygen

## Fichiers sources et headers
```c
/**
* \File nom.c
* \brief {brief description}
* \author Nom de l'auteur ou {noms des auteurs}
* \version 1.0
* \date 10 décembre 2015
*/
```

### Struct
```c
/**
* \struct Str_t str.h 
* \brief {brief description}
*/
```

### Fonction
```c
/**
* \fn int main (void)
* \brief {brief description}
* \param <parameter-name> { parameter description }
* \return { description of the return value }
*/
```

### Constante
```c
/**
* \def NOM valeur
* \brief {brief description}
*/
```

### Constante
```c
/**
* \def NOM valeur
* \brief {brief description}
*/
```
