#!/bin/bash

#
# Script permettant une gestion plus facile de la recherche et l'indexation
#

indexing_program="sri_rs/target/release/indexing"
querying_program="sri_rs/target/release/querying"
index_path="index/"
config_file="config.properties"
document_list_file="documents.txt"
query_list_file="queries.txt"


function quit {
	echo "Fermeture du programme"
	exit 0
}


function wait_enter {
	echo "Appuyez sur entrée pour continuer..."
	read
}


function dedup_in_file {
	sort -u $1 -o $1
}

function read_valid_input {
	local value=""
	local  __resultvar=$1
	while [ 1 ]; do
		read value
		if [ "${#value}" -gt "0" ]; then
			eval $__resultvar="'$value'"
	break;
		fi
		echo "$len"
	done
}

function config_management_menu {
	while [ 1 -eq 1 ]; do
		clear
		echo "Gestion de la configuration"
		echo "----------------------------------------------------"
		echo "1) Afficher le fichier de configuration"
		echo "2) Créer/réinitialiser le fichier de configuration"
		echo "3) Charger un fichier de configuration"
		echo "4) Ajouter une option au fichier de configuration"
		echo "5) Retourner au menu précédent"
		echo -n ":"
		read_valid_input option
		case $option in
		1)
			cat $config_file

			wait_enter
			;;
		2)
			echo -n "">$config_file

			echo "Fichier de configuration initialisé"
			wait_enter
			;;
		3)
			echo "Entrez le chemin absolu ou relatif vers le fichier à charger"
			read_valid_input tmp_config_file
			if [ -f $tmp_config_file ]; then
				cp $tmp_config_file $config_file
				echo "Fichier de configuration chargé"
			else
				echo "Erreur, ce fichier n'existe pas"
			fi

			wait_enter
			;;
		4)
			echo "Entrez le nom de l'option"
			read_valid_input config_option

			echo "Entrez la valeur pour l'option"
			read_valid_input config_value

			echo "$config_option=$config_value">>$config_file
			dedup_in_file $config_file

			echo "Option ajoutée"
			wait_enter
			;;
		5|q)
			break
			;;
		*)
			echo "Option invalide"
			wait_enter
			;;
		esac
	done
}


function indexing_menu {
	while [ 1 -eq 1 ]; do
		clear
		echo "Gestion de l'indexation"
		echo "----------------------------------------------------"
		echo "1) Afficher la liste des documents à indexer"
		echo "2) Réinitialiser la liste des documents à indexer"
		echo "3) Charger une liste des documents à indexer"
		echo "4) Ajouter un document"
		echo "5) Ajouter des documents depuis un répertoire"
		echo "6) Lancer l'indexation"
		echo "7) Retourner au menu précédent"
		echo -n ":"
		read_valid_input option
		case $option in
		1)
			cat $document_list_file

			wait_enter
			;;
		2)
			echo -n "">$document_list_file

			echo "Fichier de configuration initialisé"
			wait_enter
			;;
		3)
			echo "Entrez le chemin absolu ou relatif vers le fichier à charger"
			read_valid_input tmp_document_list_file
			if [ -f $tmp_document_list_file ]; then
				cp $tmp_document_list_file $document_list_file
				echo "Fichier chargé"
			else
				echo "Erreur, ce fichier n'existe pas"
			fi

			wait_enter
			;;
		4)
			echo "Entrez le chemin vers le document"
			read_valid_input document_path
			document_path=`realpath $document_path`
			echo "$document_path">>$document_list_file
			dedup_in_file $document_list_file

			echo "Document ajouté"
			wait_enter
			;;
		5)
			echo "Entrez le chemin du repertoire contenant les documents"
			read_valid_input document_folder_path
			document_folder_path=`readlink -f $document_folder_path`
			if [ -d $document_folder_path ]; then
				find "$document_folder_path/"* -type f>>$document_list_file
				dedup_in_file "$document_list_file"

				echo "Documents ajoutés"
			else
				echo "Erreur, ce répertoire n'existe pas"
			fi
			wait_enter
			;;
		6)
			$indexing_program $config_file $document_list_file
			wait_enter
			;;
		7|q)
			break
			;;
		*)
			echo "Option invalide"
			wait_enter
			;;
		esac
	done
}

function querying_menu {
	while [ 1 -eq 1 ]; do
		clear
		echo "Gestion de la recherche"
		echo "----------------------------------------------------"
		echo "1) Afficher la liste des recherches à exécuter"
		echo "2) Réinitialiser la liste des documents à rechercher"
		echo "3) Charger une liste des documents à rechercher"
		echo "4) Ajouter un document"
		echo "5) Lancer la recherche"
		echo "6) Retourner au menu précédent"
		echo -n ":"
		read_valid_input option
		case $option in
		1)
			cat $query_list_file

			wait_enter
			;;
		2)
			echo -n "">$query_list_file

			echo "Fichier de configuration initialisé"
			wait_enter
			;;
		3)
			echo "Entrez le chemin absolu ou relatif vers le fichier à charger"
			read_valid_input tmp_query_list_file
			if [ -f $tmp_query_list_file ]; then
				cp $tmp_query_list_file $query_list_file
				echo "Fichier chargé"
			else
				echo "Erreur, ce fichier n'existe pas"
			fi

			wait_enter
			;;
		4)
			echo "Entrez le chemin vers le document à comparer dans le système"
			read_valid_input document_path
			document_path=`realpath $document_path`
			echo "$(( ( RANDOM % 1000000 )  + 1 )):$document_path">>$query_list_file
			dedup_in_file $query_list_file

			echo "Document ajouté"
			wait_enter
			;;
		5)
			$querying_program $config_file $query_list_file
			wait_enter
			;;
		6)
			break;
			;;
		*)
			echo "Option invalide"
			wait_enter
			;;
		esac
	done
}



function main_menu {
	while [ 1 -eq 1 ]; do
		clear
		echo "Menu"
		echo "----------------------------------------------------"
		echo "1) Configurer"
		echo "2) Indexer"
		echo "3) Rechercher"
		echo "4) Quitter"
		echo -n ":"
		read_valid_input option
		case $option in
		1)
			config_management_menu
			;;
		2)
			indexing_menu
			;;
		3)
			querying_menu
			;;
		4|q)
			break
			;;
		*)
			echo "Option invalide"
			;;
		esac
	done
}


main_menu
quit
