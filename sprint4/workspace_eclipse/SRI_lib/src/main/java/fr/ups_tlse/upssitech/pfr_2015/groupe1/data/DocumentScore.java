package fr.ups_tlse.upssitech.pfr_2015.groupe1.data;

import java.text.ParseException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class DocumentScore implements Comparable<DocumentScore>{
	private final String queryID;
	private final String queryRun;
	private final String documentID;
	private String documentName;
	private final int rank;
	private final double score;
	private final String engine;

	public DocumentScore(String queryID, String queryRun, String documentID, int rank, double score, String engine){
		this.queryID=queryID;
		this.queryRun=queryRun;
		this.documentID=documentID;
		this.rank=rank;
		this.score=score;
		this.engine=engine;
	}
	public static DocumentScore fromString(String textFormat) throws ParseException{
		String[] fields = textFormat.split(" ");
		if(fields.length!=6){
			for(String field: fields){
				System.out.println(field);
				
			}
			throw new ParseException("Error, the given string doesn't match the trec result format", 0);
		}
		String queryID=fields[0];
		String queryRun=fields[1];
		String document=fields[2];
		String rank=fields[3];
		String score=fields[4];
		String engine=fields[5];
		
		
		try{
			return new DocumentScore(queryID, queryRun, document, Integer.parseInt(rank), Double.parseDouble(score), engine);
		} catch(NumberFormatException e){
			throw e;
		}
	}

	public int compareTo(DocumentScore otherDocScore) {
		return ((Double)score).compareTo(otherDocScore.score);
	}
	
	
	
	
	/**
	 * @return the queryID
	 */
	public synchronized String getQueryID() {
		return queryID;
	}
	
	
	/**
	 * @return the queryRun
	 */
	public synchronized String getQueryRun() {
		return queryRun;
	}
	

	/**
	 * @return the documents id
	 */
	public synchronized String getDocumentID() {
		return documentID;
	}

	/**
	 * @return the documents name
	 */
	public synchronized String getDocumentName() {
		return documentName;
	}
	
	/**
	 * @param name: the name of the document
	 */
	public synchronized void setDocumentName(String name) {
		this.documentName = name;
	}
	
	
	/**
	 * @return the rank
	 */
	public synchronized int getRank() {
		return rank;
	}
	
	
	/**
	 * @return the score
	 */
	public synchronized double getScore() {
		return score;
	}
	
	
	/**
	 * @return the engine
	 */
	public synchronized String getEngine() {
		return engine;
	}
	
	
	public void toJson(ObjectNode node){
		node.put("query_id", this.queryID);
		node.put("query_run", this.queryRun);
		node.put("document_id", this.documentID);
		if(this.documentName !=null){
			node.put("document_name", this.documentName);
		}
		node.put("rank", this.rank);
		node.put("score", this.score);
		node.put("engine", this.engine);
	}
	
	public static DocumentScore fromJson(JsonNode scoreAsJson){
		JsonNode queryID = scoreAsJson.get("query_id");
		JsonNode queryRun = scoreAsJson.get("query_run");
		JsonNode document = scoreAsJson.get("document_id");
		JsonNode documentName = scoreAsJson.get("document_name");
		JsonNode rank = scoreAsJson.get("rank");
		JsonNode score = scoreAsJson.get("score");
		JsonNode engine = scoreAsJson.get("engine");

		if(queryID==null){
			throw new IllegalStateException("Json object doesn't contain a query_id field");
		}
		if(queryRun==null){
			throw new IllegalStateException("Json object doesn't contain a query_run field");
		}
		if(document==null){
			throw new IllegalStateException("Json object doesn't contain a document field");
		}
		if(rank==null){
			throw new IllegalStateException("Json object doesn't contain a rank field");
		}
		if(score==null){
			throw new IllegalStateException("Json object doesn't contain a score field");
		}
		if(engine==null){
			throw new IllegalStateException("Json object doesn't contain a engine field");
		}
		
		
		DocumentScore result = new DocumentScore(queryID.asText(), queryRun.asText(), document.asText(), Integer.parseInt(rank.asText()), Double.parseDouble(score.asText()), engine.asText());

		if(documentName !=null){
			result.setDocumentName(documentName.asText());
		}
		return result;
	}
	
}
