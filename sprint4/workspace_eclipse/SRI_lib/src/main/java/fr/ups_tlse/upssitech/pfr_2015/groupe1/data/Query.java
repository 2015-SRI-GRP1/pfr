package fr.ups_tlse.upssitech.pfr_2015.groupe1.data;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.UUID;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class Query {
	private final String id;
	private final String query;
	private final File exampleFile;
	
	public Query(String id, String query){
		if(id==null){
			throw new IllegalArgumentException("Query ID can't be null");
		}
		if(query==null){
			throw new IllegalArgumentException("Query text can't be null");
		}
		this.id=id;
		this.query=query;
		this.exampleFile=null;
	}


	public Query(String id, File file) {
		if(id==null){
			throw new IllegalArgumentException("Query ID can't be null");
		}
		if(file==null){
			throw new IllegalArgumentException("File can't be null");
		}
		this.id=id;
		this.exampleFile = file;
		this.query = this.exampleFile.getAbsolutePath();
	}


	public String getID(){
		return this.id;
	}
	public String getQuery(){
		return this.query;
	}
	
	
	public void toJson(ObjectNode node) throws IOException{
		node.put("id", this.id);
		node.put("text", this.query);

		if( exampleFile != null ){
			byte[] data = Files.readAllBytes(exampleFile.toPath());
			node.put("example-file", data);
		}
	}
	
	public static Query fromJson(JsonNode data) throws IOException{
		JsonNode id = data.get("id");
		JsonNode text = data.get("text");
		JsonNode exampleFile = data.get("example-file");

		if(id==null){
			throw new IllegalStateException("Json object doesn't contain a query id field");
		}
		if(text!=null){
			if( exampleFile == null ){
				return new Query(id.asText(), text.asText());
			}
		
			File queryFile = new File(new File(System.getProperty("java.io.tmpdir")), UUID.randomUUID().toString() + "_" + new File(text.asText()).getName());
			Files.write(queryFile.toPath(), exampleFile.binaryValue());
			
			return new Query(id.asText(), queryFile);
			
		}
		
		throw new IllegalStateException("Json object doesn't contain a query text field");
		
		
	}
	

}
