package fr.ups_tlse.upssitech.pfr_2015.groupe1.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.UUID;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class Document {
	private final String id;
	private final String filename;
	private final File documentFile;
	
	public Document(String id, File file) {
		if(id==null){
			throw new IllegalArgumentException("Document ID can't be null");
		}
		if(file==null){
			throw new IllegalArgumentException("File can't be null");
		}
		this.id=id;
		this.documentFile = file;
		this.filename = this.documentFile.getAbsolutePath();
	}


	public String getID(){
		return this.id;
	}
	public String getFilename(){
		return this.filename;
	}
	
	
	public void toJson(ObjectNode node) throws IOException{
		node.put("id", this.id);
		node.put("filename", this.filename);
		node.put("document-data", Files.readAllBytes(documentFile.toPath()));
	}

	public InputStream getContentStream() throws FileNotFoundException{
		return new FileInputStream(this.documentFile);
	}
	
	
	public static Document fromJson(JsonNode data) throws IOException{
		JsonNode id = data.get("id");
		JsonNode filename = data.get("filename");
		JsonNode documentData = data.get("document-data");

		if(id==null){
			throw new IllegalStateException("Json object doesn't contain a query id field");
		}
		if( filename == null ){
			throw new IllegalStateException("Json object doesn't contain a filename field");
		}
		if( documentData == null ){
			throw new IllegalStateException("Json object doesn't contain a document-data field");
		}
	
		File queryFile = new File(new File(System.getProperty("java.io.tmpdir")), UUID.randomUUID().toString() + "_" + new File(filename.asText()).getName());
		Files.write(queryFile.toPath(), documentData.binaryValue());
		
		return new Document(id.asText(), queryFile);
		
		
	}
	
	

}
