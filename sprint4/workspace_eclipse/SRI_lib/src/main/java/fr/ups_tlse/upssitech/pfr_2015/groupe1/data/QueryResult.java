package fr.ups_tlse.upssitech.pfr_2015.groupe1.data;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

public class QueryResult {
	private final String queryID;
	private final List<DocumentScore> scores;
	
	public QueryResult(String id){
		this.queryID=id;
		this.scores=new LinkedList<DocumentScore>();
	}
	
	
	
	
	
	public Iterable<DocumentScore> getOrderedResults(){
		Collections.sort(this.scores, Collections.reverseOrder());
		return this.scores;
	}

	public String getQueryID(){
		return this.queryID;
	}


	public void add(DocumentScore documentScore) {
		this.scores.add(documentScore);
	}
	
	public void toJson(ArrayNode resultAsJson) {
		for(DocumentScore score: this.scores){
			score.toJson(resultAsJson.addObject());
			
		}
	}




	

	public static QueryResult fromJson(String id, ArrayNode data){
		QueryResult result = new QueryResult(id);
		for(JsonNode scoreAsJson : data){
			result.add(DocumentScore.fromJson(scoreAsJson));
		}
		
		return result;
	}





	public boolean isEmpty() {
		return this.scores.isEmpty();
	}
	
}
