#!/bin/bash
mvn -T 4 install -q -DskipTests -f ../SRI_lib/pom.xml
mvn -T 4 clean package assembly:single -q -DskipTests
exit 0
