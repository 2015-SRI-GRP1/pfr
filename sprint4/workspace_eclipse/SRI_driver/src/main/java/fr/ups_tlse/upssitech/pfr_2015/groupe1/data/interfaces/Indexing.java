package fr.ups_tlse.upssitech.pfr_2015.groupe1.data.interfaces;

import java.io.IOException;

import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.Context;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.Document;

public interface Indexing {


	boolean index(Context indexingContext) throws IOException;
	
	boolean indexSingleDocument(Context indexingContext, Document document) throws IOException;
}
