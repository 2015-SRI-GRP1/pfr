package fr.ups_tlse.upssitech.pfr_2015.groupe1.data;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Context {

	private final Map<String, Object> parameters;
	private final List<Document> documents;
	private final List<Query> queries;
	private final String id;
	
	
	public Context(String id){
		if(id==null){
			throw new IllegalArgumentException("Context id can't be null");
		}
		if(id.isEmpty()){
			throw new IllegalArgumentException("Context id can't be empty");
		}
		this.id=id;
		this.parameters=new HashMap<String, Object>();
		this.documents=new LinkedList<Document>();
		this.queries=new LinkedList<Query>();
	}
	
	public String getID(){
		return this.id;
	}
	
	public void setParameter(String key, Object value) {
		if(key==null){
			throw new IllegalArgumentException("Parameter key can't be null");
		}
		if(key.isEmpty()){
			throw new IllegalArgumentException("Key can't be empty");
		}
		if(value==null){
			throw new IllegalArgumentException("Parameter value can't be null");
		}
		
		this.parameters.put(key, value);
	}

	
	public Object getParameter(String key) {
		if(key==null){
			throw new IllegalArgumentException("Parameter key can't be null");
		}
		return this.parameters.get(key);
	}
	
	public Set<Entry<String, Object>> getParameters() {
		return this.parameters.entrySet();
	}

	
	public void removeParameter(String key) {
		if(key==null){
			this.parameters.remove(key);
		}
	}
	
	public void clearParameters(){
		this.parameters.clear();
	}
	
	

	public void addQuery(Query query) {
		if(query==null){
			throw new IllegalArgumentException("Query can't be null");
		}
		
		this.queries.add(query);
		
	}
	
	public void addQueries(List<Query> queries) {
		if(queries==null){
			throw new IllegalArgumentException("Query list can't be null");
		}
		
		for(Query query: queries){
			if(query==null){
				throw new IllegalArgumentException("Query list can't contain null queries");
			}
		}
		
		this.queries.addAll(queries);
		
	}
	
	public List<Query> getQueries() {
		return this.queries;
	}

	public void removeQuery(String query) {
		if(query==null){
			this.queries.remove(query);
		}
	}
	public void removeQueries() {
		this.queries.clear();
	}
	



	public void addDocument(Document document) {
		if(document==null){
			throw new IllegalArgumentException("Document can't be null");
		}
		
		this.documents.add(document);
	}
	
	public void addDocuments(List<Document> documents) {
		if(documents==null){
			throw new IllegalArgumentException("Document list can't be null");
		}
		
		for(Document path: documents){
			if(path==null){
				throw new IllegalArgumentException("Document list can't contain null documents");
			}
		}
		
		this.documents.addAll(documents);
		
	}
	
	public List<Document> getDocuments() {
		return this.documents;
	}
	
	public void removeDocument(String document) {
		if(document!=null){
			this.documents.remove(document);
		}
	}

	public void removeDocuments() {
		this.documents.clear();
	}
}
