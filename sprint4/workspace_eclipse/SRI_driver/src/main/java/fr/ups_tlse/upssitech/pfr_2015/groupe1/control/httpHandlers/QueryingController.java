package fr.ups_tlse.upssitech.pfr_2015.groupe1.control.httpHandlers;

import java.util.LinkedList;
import java.util.List;

import javax.management.InvalidAttributeValueException;

import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.Context;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.ContextManager;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.DocumentScore;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.Query;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.QueryResult;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.drivers.groupe1.SRIDriver;

public class QueryingController extends HttpHandler {
		



		
		
		
		@Override
		public void service(Request request, Response response) throws Exception {
	    	ObjectMapper mapper = new ObjectMapper();
	    	mapper.enable(SerializationFeature.INDENT_OUTPUT);
	    	
	    	ObjectNode responseNode = mapper.createObjectNode();

    		String contextID = request.getParameter("context-id");
    		Context currentContext = null;

    		try {
    			currentContext = ContextManager.getInstance().getContext(contextID);
    		}catch(InvalidAttributeValueException e){
    			e.printStackTrace();
    		}
    		
	    	if(contextID == null || contextID.isEmpty() || currentContext == null){
	    		responseNode.put("error", "Invalid context id, you may need to use 'http://server-adress/context?action=create' to create one");
	    		
	    	} else {
		    	String body = Util.getBody(request);
		    	ObjectNode bodyAsJson = null;
		    	
		    	if(!body.isEmpty()){
			    	bodyAsJson = (ObjectNode) mapper.readTree(body);
		    	}
		    	
		    	
		    	
		    	
		    	switch(request.getParameter("action")){
		    	case "add":
		    		for(JsonNode query : bodyAsJson.get("queries")){
		    			currentContext.addQuery(Query.fromJson(query));
		    		}
		    		responseNode.put("message", "Queries added");
		    		
		    		break;

		    	case "executeAll": {
			    	ObjectNode queriesResults = responseNode.putObject("results");
		    		try{
		    			List<QueryResult> finalResults = new LinkedList<>();
		    			if(ContextManager.getInstance().getContext(contextID).getParameter("use_sri_rs").equals("1")){
		    				List<QueryResult> results = SRIDriver.getInstance().query(currentContext);
		    				if(finalResults.isEmpty()){
		    					finalResults.addAll(results);
		    				} else {
		    					for(QueryResult result: results){
			    					for(QueryResult finalResult: finalResults){
			    						if(finalResult.getQueryID().equals(result.getQueryID())){
			    							for(DocumentScore score: result.getOrderedResults()){
			    								finalResult.add(score);
			    							}
			    							break;
			    						}
			    					}
		    					}
		    				}
		    			}
		    			
			    		for(QueryResult result : finalResults){
					    	result.toJson(queriesResults.putArray(result.getQueryID()));
			    		}
			    	}catch(Exception e){
			    		responseNode.put("error", "Error while Querying (" + e.getMessage() + ")");
				    	e.printStackTrace();
			    	}
		    	
		    		break;
		    	}

		    	case "removeQueries": {
			    	currentContext.removeQueries();
		    	
		    		break;
		    	}
		    	case "removeQuery": {
			    	currentContext.removeQuery((String)request.getAttribute("query"));
		    	
		    		break;
		    	}
		    		
		    	case "executeSingleQuery": {
		    		if(bodyAsJson!=null){
				    	ObjectNode queriesResults = responseNode.putObject("results");
				    	try{
				    		Query query = Query.fromJson(bodyAsJson);
				    		
			    			QueryResult finalResult = null;
			    			Object useSRIrs = ContextManager.getInstance().getContext(contextID).getParameter("use_sri_rs");
			    			if(useSRIrs!=null && useSRIrs.equals("1")){

					    		try{
						    		QueryResult result = SRIDriver.getInstance().query(currentContext, query);
						    		finalResult = result;
				    				
			    				}catch(Exception e){
						    		responseNode.put("error", "Error while Querying (" + e.getMessage() + ")");
							    	e.printStackTrace();
						    	}
			    			}
			    			Object useSRIrsAlt = ContextManager.getInstance().getContext(contextID).getParameter("use_sri_rs_alt");
			    			if(useSRIrsAlt!=null && useSRIrsAlt.equals("1")){

					    		try{
						    		QueryResult result = SRIDriver.getInstance().query(currentContext, query);
						    	
				    				if(finalResult == null){
				    					finalResult = result;
				    				} else {
		    							for(DocumentScore score: result.getOrderedResults()){
		    								finalResult.add(score);
		    							}
					    					
				    				}
			    				}catch(Exception e){
						    		responseNode.put("error", "Error while Querying (" + e.getMessage() + ")");
							    	e.printStackTrace();
						    	}
			    			}
			    			
				    		
				    		
				    		
				    		if(finalResult!=null){
				    			finalResult.toJson(queriesResults.putArray(finalResult.getQueryID()));
				    		}
			    		}catch(Exception e){
				    		responseNode.put("error", "Invalid query (" + e.getMessage() + ")");
					    	e.printStackTrace();
				    	}
				    	
		    		}
		    		
		    		
		    		break;
		    	}
		    		
	    		default: 
		    		responseNode.put("error", "Invalid mod parameter");
		    		
		    	}
	    	}

	    	Util.buildCompressedResponse(response, mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseNode));

		}
		
		
		
}
