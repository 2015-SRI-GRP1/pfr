package fr.ups_tlse.upssitech.pfr_2015.groupe1.data.interfaces;

import java.io.IOException;
import java.util.List;

import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.Context;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.Query;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.QueryResult;


public interface Querying {
	
	
	List<QueryResult> query(Context indexingContext) throws IOException;
	QueryResult query(Context indexingContext, Query query) throws IOException;
	
	
}
