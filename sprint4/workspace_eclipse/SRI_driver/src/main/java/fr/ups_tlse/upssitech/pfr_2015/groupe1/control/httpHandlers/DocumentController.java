package fr.ups_tlse.upssitech.pfr_2015.groupe1.control.httpHandlers;

import java.io.File;

import javax.management.InvalidAttributeValueException;

import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.Context;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.ContextManager;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.Document;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.drivers.groupe1.SRIDriver;

public class DocumentController extends HttpHandler {


	@Override
	public void service(Request request, Response response) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		mapper.enable(SerializationFeature.INDENT_OUTPUT);

		ObjectNode responseNode = mapper.createObjectNode();

		String contextID = request.getParameter("context-id");
		Context currentContext = null;

		try {
			currentContext = ContextManager.getInstance().getContext(contextID);
		}catch(InvalidAttributeValueException e){
			e.printStackTrace();
		}
    	if(contextID == null || contextID.isEmpty() || currentContext == null){
    		responseNode.put("error", "Invalid context id, you may need to use 'http://server-adress/context?action=create' to create one");
    		
    	} else {
			switch (request.getParameter("action")) {
	
			

			case "get-name": {
				String id = request.getParameter("document-id");

				if (id == null || id.isEmpty()) {
					responseNode.put("error", "Invalid document id");
				}
				
				Object value = SRIDriver.getInstance().getDocumentNameForID(ContextManager.getInstance().getContext(contextID), id);
				responseNode.put("value", value.toString());
				
				break;
			}
			case "get-content": {
				String id = request.getParameter("document-id");

				if (id == null || id.isEmpty()) {
					responseNode.put("error", "Invalid document id");
				}
				
				Document document = new Document(id, new File(SRIDriver.getInstance().getDocumentNameForID(ContextManager.getInstance().getContext(contextID), id)));
				document.toJson(responseNode.putObject("document"));
				
				break;
			}
			
			default :

				responseNode.put("error", "Unknown parameter '" + request.getParameter("action") + "'");
			}
		}

    	Util.buildCompressedResponse(response, mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseNode));

	}

}
