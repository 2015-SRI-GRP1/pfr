package fr.ups_tlse.upssitech.pfr_2015.groupe1.control.httpHandlers;

import javax.management.InvalidAttributeValueException;

import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.Context;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.ContextManager;

public class ContextController extends HttpHandler {

	private Context createContext() {
		String contextID = ContextManager.getInstance().createContext();
		try{
			Context newContext = ContextManager.getInstance().getContext(contextID);
	
			newContext.setParameter("index", "default");
			newContext.setParameter("window_levels", "64");
			newContext.setParameter("window_size", "100000");
			
			return newContext;
		} catch(InvalidAttributeValueException e){
			e.printStackTrace();;
		}
		return null;
	}

	@Override
	public void service(Request request, Response response) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		mapper.enable(SerializationFeature.INDENT_OUTPUT);

		ObjectNode responseNode = mapper.createObjectNode();
		switch (request.getParameter("action")) {

		case "create":
			responseNode.put("context-id", createContext().getID());
			break;

		case "delete":
			String contextID = request.getParameter("context-id");
			if (contextID == null || contextID.isEmpty()) {
				responseNode.put("error", "Invalid context id");

			}
			ContextManager.getInstance().deleteContext(contextID);
			responseNode.put("message", "Context deleted");
			break;

		case "get":
			responseNode.put("error", "Not implemented");
			break;

		}


    	Util.buildCompressedResponse(response, mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseNode));

	}

}
