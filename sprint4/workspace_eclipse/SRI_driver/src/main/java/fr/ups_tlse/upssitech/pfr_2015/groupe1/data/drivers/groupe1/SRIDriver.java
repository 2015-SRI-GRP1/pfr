package fr.ups_tlse.upssitech.pfr_2015.groupe1.data.drivers.groupe1;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import fr.ups_tlse.upssitech.pfr_2015.groupe1.ControlServer;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.Context;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.Document;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.DocumentScore;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.Query;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.QueryResult;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.interfaces.Indexing;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.interfaces.Querying;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.parsers.groupe1.TrecRunParser;

public class SRIDriver implements Indexing, Querying{
	private File indexingProgramPath=null;
	private File queryingProgramPath=null;

	
	private static class SRIDriverHolder{
		private static SRIDriver instance = new SRIDriver();
	}
	
	
	private SRIDriver(){
		super();
		
		String indexingProgramFilename = System.getProperty("sriGroupe1IndexingProgram");
		if(indexingProgramFilename == null){
			indexingProgramFilename = "../../sri_rs/target/release/indexing";
		}
		String queryingProgramFilename = System.getProperty("sriGroupe1QueryingProgram");
		if(queryingProgramFilename == null){
			queryingProgramFilename = "../../sri_rs/target/release/querying";
		}
		this.indexingProgramPath=new File(indexingProgramFilename);
		this.queryingProgramPath=new File(queryingProgramFilename);
		
	}
	
	
	public static synchronized SRIDriver getInstance(){
		return SRIDriverHolder.instance;
	}

	
	public List<QueryResult> query(Context context) throws IOException {
		return this.query(context, context.getQueries());
	}
	
	
	private void writeConfig(Context context, PrintWriter configFileWriter){
		configFileWriter.println("index="+System.getProperty("pfrPath", ".") + "/indexes/" + context.getParameter("index") + "/sri_rs/");
		configFileWriter.println("window_levels=" + context.getParameter("window_levels"));
		configFileWriter.println("window_size=" + context.getParameter("window_size"));
	}

	
	private List<QueryResult> query(Context context, List<Query> queries) throws IOException {
		String queryingID=String.valueOf(System.currentTimeMillis());
		
		String tmpConfigurationFilename = "sri_groupe1_search_config_"+queryingID+".properties";
		String tmpQueriesFilename = "sri_groupe1_search_queries_"+queryingID+".properties";

		File tmpConfigurationFile = new File(ControlServer.tmpPath, tmpConfigurationFilename);
		PrintWriter configFileWriter = new PrintWriter(tmpConfigurationFile);
		writeConfig(context, configFileWriter);
		configFileWriter.flush();
		configFileWriter.close();

		File tmpQueriesFile = new File(ControlServer.tmpPath, tmpQueriesFilename);
		PrintWriter queriesFileWriter = new PrintWriter(tmpQueriesFile);
		for(Query query : queries){
			queriesFileWriter.println(query.getID()+":"+query.getQuery());
		}
		queriesFileWriter.flush();
		queriesFileWriter.close();

		
        String s;
        String lastLine=null;
		Process p = Runtime.getRuntime().exec(this.queryingProgramPath + " " + tmpConfigurationFile.getAbsolutePath() + " " + tmpQueriesFile.getAbsolutePath());
        
        BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
        BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
       

        System.out.println("Stdout: ");
        while ((s = stdInput.readLine()) != null) {
            System.out.println(s);
            lastLine=s;
        }

        System.out.println("Stderr: ");
        while ((s = stdError.readLine()) != null) {
            System.err.println(s);
        }
        
        File resultFile = new File(lastLine);
        if(resultFile.exists()){
            System.out.println("Fichier généré : " + lastLine);
			List<QueryResult> results = TrecRunParser.parseRunFile(resultFile);
	        
			for(QueryResult result: results){
				for(DocumentScore score: result.getOrderedResults()){
					score.setDocumentName(this.getDocumentNameForID(context, score.getDocumentID()));
				}
				return results;
			}
        }
        return new LinkedList<QueryResult>();
	}

	
	public QueryResult query(Context context, Query query) throws IOException {
		if(query==null){
			throw new IllegalArgumentException("Query can't be null");
		}
		
		List<Query> tmpQueryList = new LinkedList<>();
		tmpQueryList.add(query);
		
		List<QueryResult> resultList = query(context, tmpQueryList);
		if(!resultList.isEmpty()){
			QueryResult result = resultList.get(0);
			for(DocumentScore score: result.getOrderedResults()){
				score.setDocumentName(this.getDocumentNameForID(context, score.getDocumentID()));
			}
			return result;
		}
		
		return null;
	}

	
	

	public boolean index(Context context) throws IOException{
		return this.index(context, context.getDocuments());
	}
	
	
	public boolean index(Context context, List<Document> documents) throws IOException{
		String indexingID=String.valueOf(System.currentTimeMillis());
		
		String tmpConfigurationFilename = "sri_groupe1_search_config_"+indexingID+".properties";
		String tmpDocumentsFilename = "sri_groupe1_search_documents_"+indexingID+".txt";

		File tmpConfigurationFile = new File(ControlServer.tmpPath, tmpConfigurationFilename);
		File tmpDocumentsFile = new File(ControlServer.tmpPath, tmpDocumentsFilename);


		PrintWriter configFileWriter = new PrintWriter(tmpConfigurationFile);
		writeConfig(context, configFileWriter);
		configFileWriter.flush();
		configFileWriter.close();
		
		PrintWriter queriesFileWriter = new PrintWriter(tmpDocumentsFile);
		for(Document document : documents){
			queriesFileWriter.println(document.getFilename());
		}
		queriesFileWriter.flush();
		queriesFileWriter.close();

		
        String s;
		Process p = Runtime.getRuntime().exec(this.indexingProgramPath + " " + tmpConfigurationFile.getAbsolutePath() + " " + tmpDocumentsFile.getAbsolutePath());
        
        BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
        BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
       

        System.out.println("Stdout: ");
        while ((s = stdInput.readLine()) != null) {
            System.out.println(s);
        }

		
        System.out.println("Stderr: ");
        while ((s = stdError.readLine()) != null) {
            System.err.println(s);
        }
		return true;
	}

	
	public boolean indexSingleDocument(Context context, Document document) throws IOException{
		if(document==null){
			throw new IllegalArgumentException("Document can't be null");
		}


		List<Document> tmpDocumentList = new LinkedList<>();
		tmpDocumentList.add(document);
		
		return index(context, tmpDocumentList);
	
	}
	
	
	public String getDocumentNameForID(Context context, String id) throws IOException{
		if(id==null){
			throw new IllegalArgumentException("Document id can't be null");
		}
		
		String indexPath = System.getProperty("pfrPath", ".") + "/indexes/" + context.getParameter("index") + "/sri_rs/";
		
		Scanner associationScanner;

		associationScanner = new Scanner(new File(indexPath + "/img_association.txt"));
		while(associationScanner.hasNextLine()){
			String[] association = associationScanner.nextLine().split("\\=");
			if(association.length == 2){
				if(association[0].equals(id)){
					associationScanner.close();
					return association[1];
				}
			}
		}
		associationScanner.close();
		
		associationScanner = new Scanner(new File(indexPath + "/snd_association.txt"));
		while(associationScanner.hasNextLine()){
			String[] association = associationScanner.nextLine().split("\\=");
			if(association.length == 2){
				if(association[0].equals(id)){
					associationScanner.close();
					return association[1];
				}
			}
		}
		associationScanner.close();
		
		associationScanner = new Scanner(new File(indexPath + "/txt_association.txt"));
		while(associationScanner.hasNextLine()){
			String[] association = associationScanner.nextLine().split("\\=");
			if(association.length == 2){
				if(association[0].trim().equals(id)){
					associationScanner.close();
					return association[1];
				}
			}
			
		}
		associationScanner.close();
		
		return null;
	}





}
