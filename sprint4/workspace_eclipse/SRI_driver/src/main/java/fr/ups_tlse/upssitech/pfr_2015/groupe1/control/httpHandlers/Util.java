package fr.ups_tlse.upssitech.pfr_2015.groupe1.control.httpHandlers;

import java.io.EOFException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.GZIPInputStream;

import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;

public class Util {



	public static String getBody(Request request) throws IOException{
		try{
			StringBuilder bodyBuilder = new StringBuilder();
			
	    	Scanner bodyScanner = new Scanner(new GZIPInputStream(request.getInputStream()));
	    	while(bodyScanner.hasNextLine()){
	    		String line=bodyScanner.nextLine();
	    		bodyBuilder.append(line+ "\n");
	    	}
	    	bodyScanner.close();
	
	    	return bodyBuilder.toString().trim();
		} catch(EOFException e){
			return "";
		}
	}

	public static void buildCompressedResponse(Response response, String content) throws IOException{

		response.setContentType("application/octet-stream");
	    
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "DELETE, GET, POST, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With");
		
		Deflater deflater=new Deflater(9);

	    DeflaterOutputStream compresser = new DeflaterOutputStream(response.getOutputStream(), deflater) ;
	    compresser.write(content.getBytes());
	    compresser.close();
	    deflater.finish();
	    response.setContentLength(deflater.getTotalOut());
	    
	    
	}
}
