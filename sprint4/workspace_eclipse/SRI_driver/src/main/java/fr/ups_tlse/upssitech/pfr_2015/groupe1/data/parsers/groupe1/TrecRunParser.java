package fr.ups_tlse.upssitech.pfr_2015.groupe1.data.parsers.groupe1;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.DocumentScore;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.QueryResult;

public class TrecRunParser {
	public static List<QueryResult> parseRunFile(File runFile) throws FileNotFoundException {
		List<QueryResult> result = new LinkedList<>();

		Map<String, QueryResult> tmpResults = new HashMap<>();

		Scanner lecteurResult = new Scanner(runFile);

		while (lecteurResult.hasNextLine()) {
			String line = lecteurResult.nextLine().trim();
			if (line.isEmpty()) {
				break;
			}

			try {
				DocumentScore tmpScore = DocumentScore.fromString(line);
				QueryResult tmpResult = tmpResults.get(tmpScore.getQueryID());
				if (tmpResult == null) {
					tmpResult = new QueryResult(tmpScore.getQueryID());
					tmpResults.put(tmpResult.getQueryID(), tmpResult);
				}
				tmpResult.add(tmpScore);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		lecteurResult.close();
		
		result.addAll(tmpResults.values());

		return result;
	}
}
