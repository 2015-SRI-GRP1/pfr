package fr.ups_tlse.upssitech.pfr_2015.groupe1.data;

import java.util.HashMap;
import java.util.UUID;

import javax.management.InvalidAttributeValueException;

public class ContextManager {
	private static class ContextManagerHolder{
		private static ContextManager instance = new ContextManager();
	}
	

	private HashMap<String, Context> contexts;
	
	
	private ContextManager(){
		super();
		this.contexts = new HashMap<>();
		
	}
	
	public static synchronized ContextManager getInstance(){
		return ContextManagerHolder.instance;
	}
	
	public String createContext(){
		String id = UUID.randomUUID().toString();
		
		this.contexts.put(id, new Context(id));
		
		return id;
	}
	
	public Context getContext(String id) throws InvalidAttributeValueException{
		Context result = this.contexts.get(id);
		if(result == null){
			throw new InvalidAttributeValueException("Context " + id + " doesn't exist");
		}
		return result;
	}

	public void deleteContext(String contextID) {
		this.contexts.remove(contextID);
	}

}
