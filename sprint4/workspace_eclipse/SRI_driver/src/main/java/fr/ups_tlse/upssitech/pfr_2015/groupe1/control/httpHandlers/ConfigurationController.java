package fr.ups_tlse.upssitech.pfr_2015.groupe1.control.httpHandlers;

import javax.management.InvalidAttributeValueException;

import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.Context;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.ContextManager;

public class ConfigurationController extends HttpHandler {


	@Override
	public void service(Request request, Response response) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		mapper.enable(SerializationFeature.INDENT_OUTPUT);

		ObjectNode responseNode = mapper.createObjectNode();

		String contextID = request.getParameter("context-id");
		Context currentContext = null;
		try {
			currentContext = ContextManager.getInstance().getContext(contextID);
		}catch(InvalidAttributeValueException e){
			e.printStackTrace();
		}
    	if(contextID == null || contextID.isEmpty() || currentContext == null){
    		responseNode.put("error", "Invalid context id, you may need to use 'http://server-adress/context?action=create' to create one");
    		
    	} else {
			switch (request.getParameter("action")) {
	
			case "set": {
				String key = request.getParameter("key");
				String value = request.getParameter("value");

				if (key == null || key.isEmpty()) {
					responseNode.put("error", "Invalid parameter key");
					break;
				}
				
				if (value == null) {
					responseNode.put("error", "Invalid parameter value");
					break;
				}
				if(value.isEmpty() ){
					ContextManager.getInstance().getContext(contextID).removeParameter(key);
					responseNode.put("message", "Parameter deleted");
					break;
				}
				
				ContextManager.getInstance().getContext(contextID).setParameter(key, value);
				responseNode.put("message", "Parameter set");
				
				break;
			}
			case "remove": {
				String key = request.getParameter("key");

				if (key == null || key.isEmpty()) {
					responseNode.put("error", "Invalid parameter key");
					break;
				}
				
				ContextManager.getInstance().getContext(contextID).removeParameter(key);
				responseNode.put("message", "Parameter deleted");
				
				break;
			}
			
			case "get": {
				String key = request.getParameter("key");

				if (key == null || key.isEmpty()) {
					responseNode.put("error", "Invalid parameter key");
					break;
				}
				
				Object value = ContextManager.getInstance().getContext(contextID).getParameter(key);
				responseNode.put("value", value == null ? "" : value.toString());
				
				break;
			}
			
			default :
				responseNode.put("error", "Unknown parameter '" + request.getParameter("action") + "'");
				
			}
		}

    	Util.buildCompressedResponse(response, mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseNode));

	}

}
