package fr.ups_tlse.upssitech.pfr_2015.groupe1.control.httpHandlers;

import javax.management.InvalidAttributeValueException;

import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.Context;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.ContextManager;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.Document;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.drivers.groupe1.SRIDriver;

public class IndexingController extends HttpHandler {
		
		
		
		@Override
		public void service(Request request, Response response) throws Exception {
			ObjectMapper mapper = new ObjectMapper();
	    	mapper.enable(SerializationFeature.INDENT_OUTPUT);
	    	
	    	ObjectNode responseNode = mapper.createObjectNode();

    		String contextID = request.getParameter("context-id");
    		Context currentContext = null;

    		try {
    			currentContext = ContextManager.getInstance().getContext(contextID);
    		}catch(InvalidAttributeValueException e){
    			e.printStackTrace();
    		}
	    	if(contextID == null || contextID.isEmpty() || currentContext == null){
	    		responseNode.put("error", "Invalid context id, you may need to use 'http://server-adress/context?action=create' to create one");
	    		
	    	} else {
		    	String body = Util.getBody(request);
		    	ObjectNode bodyAsJson = null;
		    	
		    	if(!body.isEmpty()){
			    	bodyAsJson = (ObjectNode) mapper.readTree(body);
		    	}
		    	
		    	
		    	
		    	
		    	switch(request.getParameter("action")){
		    	case "add":
		    		for(JsonNode query : bodyAsJson.get("documents")){
		    			currentContext.addDocument(Document.fromJson(query));
		    		}
		    		responseNode.put("message", "Documents added");
		    		
		    		break;

		    	case "indexAll": {
		    		try{
			    		SRIDriver.getInstance().index(currentContext);
			    		responseNode.put("message", "Documents indexed");
			    	}catch(Exception e){
			    		responseNode.put("error", "Error while indexing (" + e.getMessage() + ")");
				    	e.printStackTrace();
			    	}
		    	
		    		break;
		    	}

		    	case "removeDocuments": {
			    	currentContext.removeDocuments();
		    	
		    		break;
		    	}
		    	case "removeDocument": {
			    	currentContext.removeDocument((String)request.getAttribute("document"));
		    	
		    		break;
		    	}
		    		
		    	case "indexSingleDocument": {
		    		if(bodyAsJson!=null){
				    	try{
				    		//SRIDriver.getInstance().indexSingleDocument(currentContext, Document.fromJson(bodyAsJson));
				    		currentContext.addDocument(Document.fromJson(bodyAsJson));
				    		SRIDriver.getInstance().index(currentContext);
				    		responseNode.put("message", "Document indexed");
			    		}catch(Exception e){
				    		responseNode.put("error", "Invalid document (" + e.getMessage() + ")");
					    	e.printStackTrace();
				    	}
				    	
		    		}
		    		break;
		    	}
		    		
	    		default: 
		    		responseNode.put("error", "Invalid mod parameter");
		    		
		    	}
	    	}

	    	Util.buildCompressedResponse(response, mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseNode));

		}
		
		
		
}
