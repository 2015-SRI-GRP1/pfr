package fr.ups_tlse.upssitech.pfr_2015.groupe1;

import java.io.File;

import org.glassfish.grizzly.http.server.HttpServer;

import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.httpHandlers.ConfigurationController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.httpHandlers.ContextController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.httpHandlers.DocumentController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.httpHandlers.IndexingController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.httpHandlers.QueryingController;

public class ControlServer  {
	private static int PORT=8080;
	public static File tmpPath;
	
	
	public static void main(String[] args) {
		ControlServer.tmpPath=new File(System.getProperty("java.io.tmpdir"));
		
		
		
		HttpServer server = HttpServer.createSimpleServer("./www/",  PORT);
		server.getServerConfiguration().setMaxBufferedPostSize(-1);
		server.getServerConfiguration().setMaxFormPostSize(-1);
		server.getServerConfiguration().setMaxPostSize(-1);

		server.getServerConfiguration().addHttpHandler(new QueryingController(), "/querying");
		server.getServerConfiguration().addHttpHandler(new IndexingController(), "/indexing");
		server.getServerConfiguration().addHttpHandler(new ContextController(), "/context");
		server.getServerConfiguration().addHttpHandler(new DocumentController(), "/documents");
		server.getServerConfiguration().addHttpHandler(new ConfigurationController(), "/configuration");
		
		try {
		    server.start();
		    System.out.println("Press any key to stop the server...");
		    System.in.read();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}