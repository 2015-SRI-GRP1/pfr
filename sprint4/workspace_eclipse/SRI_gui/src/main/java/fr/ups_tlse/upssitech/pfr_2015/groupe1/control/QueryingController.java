package fr.ups_tlse.upssitech.pfr_2015.groupe1.control;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.client.HttpClient;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.impl.client.HttpClientBuilder;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.DocumentScore;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.Query;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.QueryResult;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.util.Util;

public class QueryingController {
	
	
	static HttpClient httpClient = HttpClientBuilder.create().build();
	static ObjectMapper mapper = new ObjectMapper();
	private String serverAdress;
	
	
	
	public QueryingController(String serverAdress){
		this.serverAdress = serverAdress;
	}
	
	

	public String addSearchByCriteria(String contextID, String queryID, String criteria) throws URISyntaxException, HttpHostConnectException, IOException{		
		URIBuilder urlBuilder = new URIBuilder();
		urlBuilder.setScheme("http").setHost(serverAdress).setPath("/querying");
		urlBuilder.setParameter("action", "add");
		urlBuilder.setParameter("context-id", contextID);
		
		
		ObjectNode queryObject = mapper.createObjectNode();
		ArrayNode queriesArray = queryObject.putArray("queries");
		Query httpQuery= new Query(queryID, criteria);
		httpQuery.toJson(queriesArray.addObject());
		
		
		
		ObjectNode queryResponse = Util.requete(urlBuilder.build(), "POST", queryObject);

		if(queryResponse.get("error") != null){
			System.out.println(queryResponse.get("error"));
		}
		
		

		JsonNode message = queryResponse.get("message");
		return message != null ? message.asText() : "";
	}
	
	
	public String addSearchByExample(String contextID, String queryID, String exampleDocument) throws URISyntaxException, HttpHostConnectException, IOException{		
		URIBuilder urlBuilder = new URIBuilder();
		urlBuilder.setScheme("http").setHost(serverAdress).setPath("/querying");
		urlBuilder.setParameter("action", "add");
		urlBuilder.setParameter("context-id", contextID);
		
		
		ObjectNode queryObject = mapper.createObjectNode();
		ArrayNode queriesArray = queryObject.putArray("queries");
		Query httpQuery= new Query(queryID, new File(exampleDocument));
		httpQuery.toJson(queriesArray.addObject());
		
		
		
		ObjectNode queryResponse = Util.requete(urlBuilder.build(), "POST", queryObject);

		if(queryResponse.get("error") != null){
			System.out.println(queryResponse.get("error"));
		}
		
		

		JsonNode message = queryResponse.get("message");
		return message != null ? message.asText() : "";
	}
	
	

	public List<QueryResult> executeAll(String contextID) throws URISyntaxException, HttpHostConnectException{		
		URIBuilder urlBuilder = new URIBuilder();
		urlBuilder.setScheme("http").setHost(serverAdress).setPath("/querying");
		urlBuilder.setParameter("action", "executeAll");
		urlBuilder.setParameter("context-id", contextID);
		
		
		
		
		
		ObjectNode queryResponse = Util.requete(urlBuilder.build(), "GET", null);

		if(queryResponse.get("error") != null){
			System.out.println(queryResponse.get("error"));
		}
		
		
		List<QueryResult> searchResults= new LinkedList<QueryResult>();
		
		Iterator<String> ids = queryResponse.get("results").fieldNames();
		while(ids.hasNext()){
			String queryID = ids.next();
			searchResults.add(QueryResult.fromJson(queryID, (ArrayNode)queryResponse.get("results").get(queryID)));
		}
		
		return searchResults;
	}
	

	
	
	public QueryResult singleSearchByExample(String contextID, String queryID, String exampleDocument) throws URISyntaxException, HttpHostConnectException, IOException{		
		URIBuilder urlBuilder = new URIBuilder();
		urlBuilder.setScheme("http").setHost(serverAdress).setPath("/querying");
		urlBuilder.setParameter("action", "executeSingleQuery");
		urlBuilder.setParameter("context-id", contextID);
		
		
		ObjectNode queryObject = mapper.createObjectNode();
		Query httpQuery= new File(exampleDocument).exists() ? new Query(queryID, new File(exampleDocument)) : new Query(queryID, exampleDocument);
		httpQuery.toJson(queryObject);
		
		
		
		ObjectNode queryResponse = Util.requete(urlBuilder.build(), "POST", queryObject);

		if(queryResponse.get("error") != null){
			System.out.println(queryResponse.get("error"));
		} else {
			if(queryResponse.get("results")==null){
				System.out.println("Pas de resultats de recherche");
			} else {

				QueryResult searchResult= new QueryResult(queryID);
				JsonNode scores = queryResponse.get("results").get(queryID);
				if(scores!=null){
					for(JsonNode score : scores){
						searchResult.add(DocumentScore.fromJson(score));
					}
				}
				return searchResult;
			}
		}
		return null;
	}

	public QueryResult singleTextSearchByCriteria(String contextID, String queryID, String critere) throws URISyntaxException, HttpHostConnectException, IOException{		
		URIBuilder urlBuilder = new URIBuilder();
		urlBuilder.setScheme("http").setHost(serverAdress).setPath("/querying");
		urlBuilder.setParameter("action", "executeSingleQuery");
		urlBuilder.setParameter("context-id", contextID);
		
		
		ObjectNode queryObject = mapper.createObjectNode();
		Query httpQuery= new Query(queryID, critere + ":criterion:txt" );
		httpQuery.toJson(queryObject);
		
		
		
		ObjectNode queryResponse = Util.requete(urlBuilder.build(), "POST", queryObject);

		if(queryResponse.get("error") != null){
			System.out.println(queryResponse.get("error"));
		} else {
			if(queryResponse.get("results")==null){
				System.out.println("Pas de resultats de recherche");
			} else {

				QueryResult searchResult= new QueryResult(queryID);
				
				JsonNode scores = queryResponse.get("results").get(queryID);
				if(scores!=null){
					for(JsonNode score : scores){
						searchResult.add(DocumentScore.fromJson(score));
					}
				}
				return searchResult;
			}
		}
		return null;
		
	}
	
	public QueryResult singleImageSearchByCriteria(String contextID, String queryID, String critere) throws URISyntaxException, HttpHostConnectException, IOException{		
		URIBuilder urlBuilder = new URIBuilder();
		urlBuilder.setScheme("http").setHost(serverAdress).setPath("/querying");
		urlBuilder.setParameter("action", "executeSingleQuery");
		urlBuilder.setParameter("context-id", contextID);
		
		
		ObjectNode queryObject = mapper.createObjectNode();
		Query httpQuery= new Query(queryID, critere + ":criterion:img" );
		httpQuery.toJson(queryObject);
		
		
		
		ObjectNode queryResponse = Util.requete(urlBuilder.build(), "POST", queryObject);

		if(queryResponse.get("error") != null){
			System.out.println(queryResponse.get("error"));
		} else {
			QueryResult searchResult = new QueryResult(queryID);
			if(queryResponse.get("results")==null){
				System.out.println("Pas de resultats de recherche");
			} else {
				JsonNode scores = queryResponse.get("results").get(queryID);
				if(scores!=null){
					for(JsonNode score : scores){
						searchResult.add(DocumentScore.fromJson(score));
					}
				}
			}
			return searchResult;
		}
		return null;
	}

	public String clearQueries(String contextID) throws URISyntaxException, HttpHostConnectException {		
			URIBuilder urlBuilder = new URIBuilder();
			urlBuilder.setScheme("http").setHost(serverAdress).setPath("/querying");
			urlBuilder.setParameter("action", "removeQueries");
			urlBuilder.setParameter("context-id", contextID);
			
			
			
			ObjectNode queryResponse = Util.requete(urlBuilder.build(), "DELETE", null);

			if(queryResponse.get("error") != null){
				System.out.println(queryResponse.get("error"));
			}
			
			
			JsonNode message = queryResponse.get("message");
			return message != null ? message.asText() : "";
		
	}
	
	
	public String removeQuery(String contextID, String queryID) throws URISyntaxException, HttpHostConnectException {		
		URIBuilder urlBuilder = new URIBuilder();
		urlBuilder.setScheme("http").setHost(serverAdress).setPath("/querying");
		urlBuilder.setParameter("action", "removeQuery");
		urlBuilder.setParameter("context-id", contextID);
		urlBuilder.setParameter("query-id", queryID);
		
		
		
		ObjectNode queryResponse = Util.requete(urlBuilder.build(), "DELETE", null);

		if(queryResponse.get("error") != null){
			System.out.println(queryResponse.get("error"));
		}
		
		
		JsonNode message = queryResponse.get("message");
		return message != null ? message.asText() : "";
	}
	
}
