package fr.ups_tlse.upssitech.pfr_2015.groupe1.graphicalView;

import java.awt.Color;
import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableModel;

import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.ContextController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.DocumentController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.Document;

@SuppressWarnings("serial")
public class DocumentDisplayAction extends AbstractAction {
	private TableModel resultTableModel;
	private ContextController contextController;
	private DocumentController documentController;


	public DocumentDisplayAction(TableModel resultTableModel, ContextController contextController, DocumentController documentController){
		this.resultTableModel = resultTableModel;
		this.contextController = contextController;
		this.documentController = documentController;
	}


    public void actionPerformed(ActionEvent event){
        JTable table = (JTable)event.getSource();
        int rowIndex = Integer.valueOf( event.getActionCommand() );
        String documentID = (String) table.getModel().getValueAt(rowIndex, resultTableModel.getColumnCount()-2);
        try {
			
			switch(documentID.substring(0, 3)){
				case "txt": {
					Document document = documentController.getDocumentForID(contextController.getContextID(), documentID);
					
					StringBuilder fileContent = new StringBuilder();
					Scanner fileScanner = new Scanner(document.getContentStream());
					boolean inText = false;
					while(fileScanner.hasNextLine()){
						String line = fileScanner.nextLine().trim();
						if(line.toLowerCase().contains("<texte>")){
							inText = true;
							line = line.replace("<texte>", "").replace("<TEXTE>", "").trim();
						} 
						if(inText && !line.isEmpty()){

							if(line.toLowerCase().contains("</texte>")){
								inText = false;
								line = line.replace("</texte>", "").replace("</TEXTE>", "").trim();
							}
							if(!line.isEmpty()){
								fileContent.append( line);
							}
						}
					}
					fileScanner.close();
					
					String fileContentAsString = fileContent.toString().replace("<phrase>", "<p style='margin-bottom: 1em;'>").replace("<PHRASE>", "<p style='margin-bottom: 20px;'>").replace("</phrase>", "</p>").replace("</PHRASE>", "</p>");
					
					JDialog documentDisplayDialog = new JDialog(null, (String) table.getModel().getValueAt(rowIndex, 3) + "(" + documentID + ")", ModalityType.MODELESS);
					documentDisplayDialog.setMinimumSize(new Dimension(680, 480));

					JEditorPane documentContent=new JEditorPane("text/html","<html><body style='font-family: Helvetica, Arial, Sans-Serif;' >"+fileContentAsString+"</body></html>");
					documentContent.setEditable(false);
					documentContent.setCaretPosition(0);
					
					JScrollPane documentContentWrapper = new JScrollPane(documentContent);
					documentContentWrapper.getViewport().setBackground(Color.BLACK);

					
					documentDisplayDialog.add(documentContentWrapper);
					
					documentDisplayDialog.setLocationRelativeTo(null);
					documentDisplayDialog.setVisible(true);
					
					
					//JOptionPane.showMessageDialog(null, "<html><body style='width: 480px;' >"+fileContentAsString+"</body></html>", (String) table.getModel().getValueAt(rowIndex, 2), JOptionPane.DEFAULT_OPTION);
				}
				break;

				case "img": {
					Document document = documentController.getDocumentForID(contextController.getContextID(), documentID);
					BufferedImage image = ImageIO.read(document.getContentStream());
					
					JDialog documentDisplayDialog = new JDialog(null, (String) table.getModel().getValueAt(rowIndex, 3), JDialog.DEFAULT_MODALITY_TYPE);


					
					JLabel documentContent=new JLabel(new ImageIcon(image));

					JScrollPane documentContentWrapper = new JScrollPane(documentContent);
					documentContentWrapper.getViewport().setBackground(Color.BLACK);
					
					documentDisplayDialog.add(documentContentWrapper);

					documentDisplayDialog.pack();
					documentDisplayDialog.setLocationRelativeTo(null);
					documentDisplayDialog.setVisible(true);
					
				}
				break;
				
				default:
					JOptionPane.showMessageDialog(null, "Preview not implemented for this type of document", (String) table.getModel().getValueAt(rowIndex, 2), JOptionPane.INFORMATION_MESSAGE);
			}
		} catch (URISyntaxException | IOException e) {
			JOptionPane.showMessageDialog(table, "Error: Can't get document from server", "Error: Can't get document", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
    }
}
