package fr.ups_tlse.upssitech.pfr_2015.groupe1.graphicalView.listeners;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import com.fasterxml.jackson.core.JsonParseException;

import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.ContextController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.DocumentController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.QueryingController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.QueryResult;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.QueryResultManager;

public class SearchListener implements ActionListener{
	private QueryingController queryingController;
	private DocumentController documentController;
	private JTextField searchField;
	private JLabel logoLabel;
	private JComponent searchResultsComponent;
	private JLabel centerPanel;
	private ContextController contextController;

	public SearchListener(QueryingController queryingController, DocumentController documentController, ContextController contextController, JTextField searchField, JLabel logoLabel, JComponent searchResultsPanel, JLabel centerPanel) {

		this.queryingController = queryingController;
		this.documentController = documentController;
		this.contextController = contextController;
		this.searchField = searchField;
		this.logoLabel = logoLabel;
		this.searchResultsComponent = searchResultsPanel;
		this.centerPanel = centerPanel;
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		String query = this.searchField.getText().trim();
		if(query.isEmpty()){
			return;
		}

		
		searchResultsComponent.setBackground(new Color(0, 0, 0, 200));
		searchResultsComponent.setVisible(true);
		
		SwingUtilities.invokeLater(new Runnable(){
			long startTime = System.currentTimeMillis();
			double targetTime = 500;
			int initialHeight = logoLabel.getHeight();
			int initialBorder = 64;
			
			public void run(){
				double size = Math.cos((System.currentTimeMillis()-startTime)/targetTime*Math.PI/2);
				if(logoLabel.getHeight()>0){
					logoLabel.setPreferredSize(new Dimension(logoLabel.getPreferredSize().width, (int)(size*initialHeight) ));
					logoLabel.revalidate();

					centerPanel.setBorder(BorderFactory.createEmptyBorder(Math.max(8, (int)(size*initialBorder)), 0, 0, 0));
					centerPanel.revalidate();
					
					
					SwingUtilities.invokeLater(this);
				}
			}
		});
		
		
		QueryResult result = null;
		try{
			if(query.startsWith("file://")){
				String filePath = query.replace("file://", "");
				try {
					result = queryingController.singleSearchByExample(contextController.getContextID(), "0", filePath);
				} catch (URISyntaxException | IOException searchByExampleException) {
					searchByExampleException.printStackTrace();
					QueryResultManager.getInstance().setQueryResults(null);
				}
			} else {
				if(query.startsWith("similar:")){
					String id = query.replace("similar:", "");
					try {
						String filePath = documentController.getNameForID(contextController.getContextID(), id);
						result = queryingController.singleSearchByExample(contextController.getContextID(), "0", filePath);
					} catch (URISyntaxException | IOException searchByExampleException) {
						searchByExampleException.printStackTrace();
						QueryResultManager.getInstance().setQueryResults(null);
					}
				} else {
					String[] words = query.split(" ");
					if(words.length == 3){
						try{
							Integer.parseInt(words[0]);
							Integer.parseInt(words[1]);
							Integer.parseInt(words[2]);
							try {
								result = queryingController.singleImageSearchByCriteria(contextController.getContextID(), "0", query);
							} catch (URISyntaxException | IOException imageSearchException) {
								imageSearchException.printStackTrace();
								QueryResultManager.getInstance().setQueryResults(null);
							}
						}catch(NumberFormatException e){
							try {
								result = queryingController.singleTextSearchByCriteria(contextController.getContextID(), "0", query);
							} catch (URISyntaxException | IOException textSearchException) {
								textSearchException.printStackTrace();
								QueryResultManager.getInstance().setQueryResults(null);
							}
						}
					} else {
						try {
							result = queryingController.singleTextSearchByCriteria(contextController.getContextID(), "0", query);
						} catch (URISyntaxException | IOException textSearchException) {
							textSearchException.printStackTrace();
							QueryResultManager.getInstance().setQueryResults(null);
						}
					}
				}
				
			}
		} catch (Exception e){
			e.printStackTrace();
		}

		if(result == null || result.isEmpty()){
			JOptionPane.showMessageDialog(null, "No search results");
			QueryResultManager.getInstance().removeQueryResults();
		} else {
			QueryResultManager.getInstance().setQueryResults(result);
		}
		
		
		

		
	}

}
