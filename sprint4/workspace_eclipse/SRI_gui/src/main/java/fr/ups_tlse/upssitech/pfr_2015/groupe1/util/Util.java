package fr.ups_tlse.upssitech.pfr_2015.groupe1.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Scanner;
import java.util.zip.GZIPInputStream;
import java.util.zip.InflaterInputStream;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.GzipCompressingEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpTrace;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.DocumentController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.Document;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.DocumentScore;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.QueryResult;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.textualView.TextView;

public class Util {
	// http://stackoverflow.com/a/5762502
	public static final String ANSI_RESET = "\u001B[0m";

	public static final String ANSI_BLACK = "\u001B[30m";
	public static final String ANSI_BLACK_BRIGHT = "\u001B[30;1m";
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_RED_BRIGHT = "\u001B[31;1m";
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_GREEN_BRIGHT = "\u001B[32;1m";
	public static final String ANSI_YELLOW = "\u001B[33m";
	public static final String ANSI_YELLOW_BRIGHT = "\u001B[33;1m";
	public static final String ANSI_BLUE = "\u001B[34m";
	public static final String ANSI_BLUE_BRIGHT = "\u001B[34;1m";
	public static final String ANSI_PURPLE = "\u001B[35m";
	public static final String ANSI_PURPLE_BRIGHT = "\u001B[35;1m";
	public static final String ANSI_CYAN = "\u001B[36m";
	public static final String ANSI_CYAN_BRIGHT = "\u001B[36;1m";
	public static final String ANSI_WHITE = "\u001B[37m";
	public static final String ANSI_WHITE_BRIGHT = "\u001B[37;1m";
	

	static HttpClient httpClient = HttpClientBuilder.create().build();
	static ObjectMapper mapper = new ObjectMapper();
	
	
	public static ObjectNode requete(URI uri, String type, ObjectNode contenu) throws HttpHostConnectException{
		try {
			HttpUriRequest request = null;
			switch(type.trim().toUpperCase()){
			case "HEAD":
				request = new HttpHead(uri);
				break;
				
			case "POST":
				request = new HttpPost(uri);
				break;
				
			case "OPTIONS":
				request = new HttpOptions(uri);
				break;
				
			case "CONNECT":
				assert(false);
				break;
				
			case "TRACE":
				request = new HttpTrace(uri);
				break;
				
			case "PUT":
				request = new HttpPut(uri);
				break;
				
			case "PATCH":
				request = new HttpPatch(uri);break;
				
			case "DELETE":
				request = new HttpDelete(uri);
				break;
				
			default:
				request = new HttpGet(uri);
				break;
			}
			request.addHeader("content-type", "application/json");
			
			if(request instanceof HttpEntityEnclosingRequest){
				((HttpEntityEnclosingRequest)request).setEntity(new GzipCompressingEntity(new StringEntity(mapper.writer().writeValueAsString(contenu))));
			}
			HttpResponse response = httpClient.execute(request);
			
			String responseContent = decompressResponse(response);
			try {
				return (ObjectNode) mapper.readTree( responseContent );
			}catch(JsonParseException e){
				e.printStackTrace();
				try {
					return (ObjectNode) mapper.readTree(responseContent + "}");
				}catch(JsonParseException e1){
					try {
						return (ObjectNode) mapper.readTree(responseContent + "}}");
					}catch(JsonParseException e2){
						try {
							return (ObjectNode) mapper.readTree(responseContent + "}}");
						}catch(JsonParseException e3){
							try {
								return (ObjectNode) mapper.readTree(responseContent + "]}}");
							}catch(JsonParseException e4){
								e.printStackTrace();
								return (ObjectNode) mapper.readTree(responseContent + "}]}}");
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return  mapper.createObjectNode();
	}
	
	
	public static void attendreUtilisateur(String message) {
		Util.println(Util.ANSI_YELLOW, message);
		try {
			System.in.read();
		} catch (Exception e) {
			// RAF
		}
	}
	
	
	public static void println(String color, Object message){
		System.out.println(color + message + ANSI_RESET);
	}

	// http://stackoverflow.com/a/33379766
	public static void clearScreen() {
		try {
			final String os = System.getProperty("os.name");

			if (os.toLowerCase().contains("windows")) {
				Runtime.getRuntime().exec(new String[]{"cmd", "/c", "cls"});
			} else {
				System.out.print("\033[H\033[2J");
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}
	
	public static URL getFile(final String filename) throws FileNotFoundException {
		URL fileURL = Thread.currentThread().getContextClassLoader().getResource(filename);
		if(fileURL == null){
			throw new FileNotFoundException("Impossible de trouver " + filename + " dans les resources");
		}
	    return fileURL;
	}


	public static void showQueryResult(String contextID, DocumentController documentController, QueryResult result) throws URISyntaxException, IOException {
		while(true){
			Util.clearScreen();
			for(DocumentScore documentScore : result.getOrderedResults()){
				System.out.println(documentScore.getRank() + ":" + documentScore.getDocumentName());
			}
			
			Util.println(Util.ANSI_PURPLE_BRIGHT, "Entrez le numéro d'un document à afficher (q pour quitter)");
			String reponse;

			do {
				reponse = TextView.keyboardScanner.nextLine();
			} while ( reponse.isEmpty() );
			
			if(reponse.toLowerCase().equals("q")){
				break;
			}
			int documentRank = Integer.valueOf(reponse);
			int currentRank = 1;
			
			for(DocumentScore documentScore : result.getOrderedResults()){
				if(currentRank == documentRank){
					Document doc = documentController.getDocumentForID(contextID, documentScore.getDocumentID());
					Scanner documentContentScanner = new Scanner(doc.getContentStream());
					
					while(documentContentScanner.hasNextLine()){
						System.out.println(documentContentScanner.nextLine());
					}
					documentContentScanner.close();
					Util.attendreUtilisateur("Appuyez sur une touche pour continuer...");
					break;
				}
				currentRank++;
			}
	
			
		}
		
		
	
		}
	
	public static String decompressResponse(HttpResponse response) throws IOException{
		StringBuilder result = new StringBuilder();

	    InflaterInputStream decompressor = new InflaterInputStream(response.getEntity().getContent());
	    Scanner decompressionScanner = new Scanner(decompressor);
	  	while(decompressionScanner.hasNext()){
	  		result.append(decompressionScanner.next());
	    }
	  	decompressionScanner.close();
		return result.toString();
	}
	
	public static byte[] decompress(byte[] data) throws IOException {
	    ByteArrayOutputStream out = new ByteArrayOutputStream();
	
	    try {
	        IOUtils.copy(new GZIPInputStream(new ByteArrayInputStream(data)), out);
	    } catch (IOException e) {
	        throw new RuntimeException(e);
	    }
	    return out.toByteArray();
	}
}
