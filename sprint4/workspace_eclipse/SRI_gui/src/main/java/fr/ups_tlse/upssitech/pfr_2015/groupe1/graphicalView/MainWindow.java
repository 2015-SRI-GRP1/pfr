package fr.ups_tlse.upssitech.pfr_2015.groupe1.graphicalView;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.ConfigurationController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.ContextController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.DocumentController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.IndexingController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.QueryingController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.QueryResultManager;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.ResultTableModel;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.graphicalView.listeners.SearchListener;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.util.Util;

@SuppressWarnings("serial")
public class MainWindow extends JFrame {

	// Dimensions de la fenêtre
	private final int largeur = 960;
	private final int longueur = 640;

	private JPanel topPanel;
	private JLabel centerPanel;
	private JPanel searchBarPanel;

	private Image administrationIcon;
	private JButton administrationButton;
	private Image helpIcon;
	private JButton helpButton;

	private BufferedImage logo;
	private JPanel logoPanel;
	private JLabel logoLabel;

	private BufferedImage colorPickerIcon;
	private JButton colorPickerButton;
	private BufferedImage fileChooserIcon;
	private JButton fileChooserButton;
	private BufferedImage searchIcon;
	private JButton searchButton;

	private JTextField saisieTF;
	private JScrollPane searchResultsPanel;
	private JPanel searchPanel;;

	// Constructor
	public MainWindow(final ContextController contextController, final ConfigurationController configurationController, final IndexingController indexingController, final QueryingController queryingController, final DocumentController documentController){
		super("Pruit!");

		// ///Top Panel creation
		topPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));

		// Bouton Options
		try {
			administrationIcon = ImageIO.read(Util.getFile("images/optionIcon.png")).getScaledInstance(16, 16, Image.SCALE_SMOOTH);
			administrationButton = new JButton(new ImageIcon(administrationIcon));
			administrationButton.setBackground(Color.DARK_GRAY);
			administrationButton.setForeground(Color.WHITE);
		} catch (IOException e) {
			System.out.println("Options icon not found!");
			administrationButton = new JButton("Options");
			administrationButton.setBackground(Color.DARK_GRAY);
			administrationButton.setForeground(Color.WHITE);
		}
		administrationButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				new AdministrationDialog(configurationController, indexingController, contextController.getContextID()).setVisible(true);
			}
		});
		topPanel.add(administrationButton);

		// Bouton Aide
		try {
			helpIcon = ImageIO.read(new File("helpIcon.png"));
			helpButton = new JButton(new ImageIcon(helpIcon));
		} catch (IOException e) {
			System.out.println("Help icon not found!");
			helpButton = new JButton("Aide");
			helpButton.setBackground(Color.DARK_GRAY);
			helpButton.setForeground(Color.WHITE);
		}
		//topPanel.add(helpButton);

		// ///Top Panel Ajout
		topPanel.setBackground(Color.BLACK);
		this.add(topPanel, BorderLayout.NORTH);

		// ///Mid Panel creation
		centerPanel = new JLabel(){
			
		    protected void paintComponent(Graphics g) {
		        int width = getWidth();
		        int height = getHeight();
		        int imageW = this.getIcon().getIconWidth();
		        int imageH = this.getIcon().getIconHeight();
		  
		        // Tile the image to fill our area.
		        for (int x = 0; x < width; x += imageW) {
		            for (int y = 0; y < height; y += imageH) {
		                this.getIcon().paintIcon(this, g, x, y);
		            }
		        }
		        ((Graphics2D) g ).setPaint(new GradientPaint(0, height-100, new Color(128, 128, 128, 32), 0, height, new Color(0, 0, 0, 200)));
		        //g.setColor(new Color(0, 0, 0, 64));
		        g.fillRect(0, 0, width, height);
		        
		    }
		};
		
		try {
			ImageIcon backgroundIcon = new ImageIcon(Util.getFile("images/cats.gif"));
			centerPanel.setIcon(backgroundIcon);
		} catch (IOException e) {
			e.printStackTrace();
		}
		centerPanel.setLayout(new BorderLayout());
		centerPanel.setBorder(BorderFactory.createEmptyBorder(64, 0, 0, 0));
		

		// Logo
		logoLabel = new JLabel();
		try {
			logo = ImageIO.read(Util.getFile("images/google_break.jpg"));
			logoLabel.setIcon(new ImageIcon(logo.getScaledInstance(360, 360*logo.getHeight()/logo.getWidth(), Image.SCALE_SMOOTH)));
		} catch (IOException e) {
			logoLabel.setText("Pruit!");
			logoLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 80));
			logoLabel.setForeground(Color.WHITE);
			logoLabel.setOpaque(true);
			logoLabel.setBackground(new Color(0, 0, 0, 200));
			System.out.println("Logo not found!");
		}
		logoLabel.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED, Color.CYAN, Color.MAGENTA));
		
		logoPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));

		logoPanel.setOpaque(false);
		logoPanel.add(logoLabel);

		buildSearchPanel(contextController, configurationController, indexingController, queryingController, documentController);
		

		centerPanel.add(logoPanel, BorderLayout.NORTH);
		centerPanel.add(searchPanel, BorderLayout.CENTER);
		centerPanel.add(new SearchOptionsPanel(contextController, configurationController), BorderLayout.SOUTH);

		
		JScrollPane centerPanelContainer = new JScrollPane(centerPanel);
		this.add(centerPanelContainer, BorderLayout.CENTER);

		// Fullscreen
		// this.setExtendedState(JFrame.MAXIMIZED_BOTH);

		// Définition du mode de fermeture
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Défintion de la taille de l'objet
		this.setSize(largeur, longueur);

		// Définition de la position d'apparition de la fenêtre
		this.setLocationRelativeTo(null);

	}

	private void buildSearchPanel(final ContextController contextController, final ConfigurationController configurationController, final IndexingController indexingController, final QueryingController queryingController, final DocumentController documentController){

		final ResultTableModel resultTableModel = new ResultTableModel();
		QueryResultManager.getInstance().addObserver(resultTableModel);
		JTable resultTable = new JTable(0, 6);
		resultTable.setModel(resultTableModel);
		
		resultTable.setRowHeight(32);
		resultTable.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
		
		resultTable.setDefaultRenderer(Object.class, new  DefaultTableCellRenderer() {
		    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		        Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		        c.setBackground(row%2 == 0 ? Color.WHITE : Color.LIGHT_GRAY);
		        return c;
		    }
		});
		
		resultTable.setDefaultRenderer(Double.class, resultTable.getDefaultRenderer(Object.class));
		
		resultTable.getColumnModel().getColumn(0).setMaxWidth(64);
		resultTable.getColumnModel().getColumn(1).setMaxWidth(64);
		resultTable.getColumnModel().getColumn(2).setMaxWidth(64);
		resultTable.getColumnModel().getColumn(4).setMaxWidth(128);
		resultTable.getColumnModel().getColumn(5).setMaxWidth(128);
		resultTable.getColumnModel().getColumn(6).setPreferredWidth(128);
		resultTable.getColumnModel().getColumn(6).setMaxWidth(256);
		
		resultTable.setAutoCreateRowSorter(true);
		((TableRowSorter<? extends TableModel>)resultTable.getRowSorter()).setSortable(resultTableModel.getColumnCount()-2, false);
		((TableRowSorter<? extends TableModel>)resultTable.getRowSorter()).setSortable(resultTableModel.getColumnCount()-1, false);
		
		searchResultsPanel = new JScrollPane(resultTable);
		searchResultsPanel.setBackground(new Color(64, 64, 64, 0));
		//searchResultsPanel.setOpaque(false);
		searchResultsPanel.getViewport().setOpaque(false);
		searchResultsPanel.setVisible(false);
		
		
		// Colorpicker
		colorPickerButton = new JButton("Colorpicker");
		colorPickerButton.setBackground(Color.DARK_GRAY);
		colorPickerButton.setForeground(Color.WHITE);
		colorPickerButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Color c = JColorChooser.showDialog(null, "Choose a Color",	Color.BLACK);
				if (c != null) {
					saisieTF.requestFocus();
					saisieTF.setText(c.getRed() + " " + c.getGreen() + " " + c.getBlue());
				}
			}
		});
		
		try {
			colorPickerIcon = ImageIO.read(Util.getFile("images/colorPickerIcon.png"));
			colorPickerButton.setIcon(new ImageIcon(colorPickerIcon));
			colorPickerButton.setText("");
			
		} catch (IOException e) {
			System.out.println("Colorpicker icon not found!");
		}

		// Bouton File Browser
		fileChooserButton = new JButton("Browse");
		fileChooserButton.setBackground(Color.DARK_GRAY);
		fileChooserButton.setForeground(Color.WHITE);
		{
			final JFrame mainWindowInstance = this;
			fileChooserButton.addActionListener(new ActionListener() {
				JFileChooser fileChooser = new JFileChooser();
				public void actionPerformed(ActionEvent e) {
					if(fileChooser.showOpenDialog(mainWindowInstance) == JFileChooser.APPROVE_OPTION){
						saisieTF.setText("file://" + fileChooser.getSelectedFile().getAbsolutePath());
					}
				}
			});
		}
		try {
			fileChooserIcon = ImageIO.read(Util.getFile("images/fileChooserIcon.png"));
			fileChooserButton.setIcon(new ImageIcon(fileChooserIcon));
			fileChooserButton.setText("");
		} catch (IOException e) {
			System.out.println("Folder icon not found!");
		}


		// Barre de recherche
		
		saisieTF = new JTextField();
		
		saisieTF.setPreferredSize(new Dimension(0, 25));
		saisieTF.setForeground(Color.WHITE);
		saisieTF.setBackground(Color.BLACK);
		saisieTF.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.GRAY, 1), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		saisieTF.setCaretColor(Color.WHITE);
		SearchListener searchListener = new SearchListener(queryingController, documentController, contextController, saisieTF, logoLabel, searchResultsPanel, centerPanel);
		saisieTF.addActionListener(searchListener);
		
		new PlaceHolderManager(saisieTF, "Ex: '255 0 0', 'sport -ligue', 'similar:img91' ");
 

		// Bouton Rechercher
		searchButton = new JButton("Search!");
		searchButton.setBackground(Color.DARK_GRAY);
		searchButton.setForeground(Color.WHITE);
		try {
			searchIcon = ImageIO.read(Util.getFile("images/searchIcon.png"));
			searchButton.setIcon(new ImageIcon(searchIcon));
			searchButton.setText("");
		} catch (IOException e) {
			System.out.println("Search icon not found!");
		}
		searchButton.addActionListener(searchListener);
		

		// Inner Middle Pannel creation
		searchBarPanel = new JPanel(new GridBagLayout());
		//searchPanel.setOpaque(false);
		searchBarPanel.setBackground(new Color(0, 0, 0, 200));

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.BASELINE_LEADING;
		gbc.insets = new Insets(5, 5, 5, 5);
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;

		// Inner Middle Pannel Ajout
		gbc.weightx = 0;
		searchBarPanel.add(colorPickerButton, gbc);

		gbc.gridx += 1;
		gbc.weightx = 0;
		searchBarPanel.add(fileChooserButton, gbc);

		gbc.gridx += 1;
		gbc.weightx = 3;
		searchBarPanel.add(saisieTF, gbc);

		gbc.gridx += 1;
		gbc.weightx = 0;
		searchBarPanel.add(searchButton, gbc);
		searchBarPanel.setBorder(BorderFactory.createEmptyBorder(8, 32, 8, 32));
		


		new ButtonColumn(resultTable, new DocumentDisplayAction(resultTableModel, contextController, documentController), resultTableModel.getColumnCount()-2, "Show");

		new ButtonColumn(resultTable, new SearchSimilarAction(resultTableModel, searchListener, saisieTF), resultTableModel.getColumnCount()-1, "Similar");
		
		
		
		
		searchPanel = new JPanel(new BorderLayout());
		searchPanel.setOpaque(false);
		searchPanel.add(searchBarPanel, BorderLayout.NORTH);
		searchPanel.add(searchResultsPanel, BorderLayout.CENTER);
	}

}