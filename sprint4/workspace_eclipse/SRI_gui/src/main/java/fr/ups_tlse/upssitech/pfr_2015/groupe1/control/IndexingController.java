package fr.ups_tlse.upssitech.pfr_2015.groupe1.control;

import java.io.File;

import org.apache.http.client.HttpClient;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.Document;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.util.Util;

public class IndexingController {
	static HttpClient httpClient = HttpClientBuilder.create().build();
	static ObjectMapper mapper = new ObjectMapper();
	private String serverAdress;
	
	
	
	public IndexingController(String serverAdress){
		this.serverAdress = serverAdress;
	}
	

	public String ajoutDocument(String contextID, String document) throws Exception{		
		return this.ajoutDocuments(contextID, new String[]{document});
	}
	

	public String ajoutDocuments(String contextID, String[] documents) throws Exception{		
		URIBuilder urlBuilder = new URIBuilder();
		urlBuilder.setScheme("http").setHost(serverAdress).setPath("/indexing");
		urlBuilder.setParameter("action", "add");
		urlBuilder.setParameter("context-id", contextID);
		
		
		ObjectNode queryObject = mapper.createObjectNode();
		ArrayNode documentsArray = queryObject.putArray("documents");
		for(String documentName : documents){
			Document searchQuery = new Document("", new File(documentName));
			searchQuery.toJson(documentsArray.addObject());
		}
		
		
		
		ObjectNode queryResponse = Util.requete(urlBuilder.build(), "POST", queryObject);

		if(queryResponse.get("error") != null){
			throw new Exception(queryResponse.get("error").asText());
		}
		

		JsonNode message = queryResponse.get("message");
		return message != null ? message.asText() : "";
	}

	

	
	public String indexationSimple(String contextID, String nomDocument) throws Exception{		
		URIBuilder urlBuilder = new URIBuilder();
		urlBuilder.setScheme("http").setHost(serverAdress).setPath("/indexing");
		urlBuilder.setParameter("action", "indexSingleDocument");
		urlBuilder.setParameter("context-id", contextID);
		
		ObjectNode queryObject = mapper.createObjectNode();
		Document searchQuery= new Document("", new File(nomDocument));
		searchQuery.toJson(queryObject);
		
		ObjectNode queryResponse = Util.requete(urlBuilder.build(), "POST", queryObject);

		if(queryResponse.get("error") != null){
			throw new Exception(queryResponse.get("error").asText());
		}

		JsonNode message = queryResponse.get("message");
		return message != null ? message.asText() : "";
	}
	
	

	public String indexAll(String contextID) throws Exception{		
		URIBuilder urlBuilder = new URIBuilder();
		urlBuilder.setScheme("http").setHost(serverAdress).setPath("/indexing");
		urlBuilder.setParameter("action", "indexAll");
		urlBuilder.setParameter("context-id", contextID);
		
		ObjectNode queryResponse = Util.requete(urlBuilder.build(), "GET", null);

		if(queryResponse.get("error") != null){
			throw new Exception(queryResponse.get("error").asText());
		}
		
		JsonNode message = queryResponse.get("message");
		return message != null ? message.asText() : "";
	}
	

	public String removeDocuments(String contextID) throws Exception {		
			URIBuilder urlBuilder = new URIBuilder();
			urlBuilder.setScheme("http").setHost(serverAdress).setPath("/indexing");
			urlBuilder.setParameter("action", "removeDocuments");
			urlBuilder.setParameter("context-id", contextID);
			
			ObjectNode queryResponse = Util.requete(urlBuilder.build(), "DELETE", null);

			if(queryResponse.get("error") != null){
				throw new Exception(queryResponse.get("error").asText());
			}
			
			JsonNode message = queryResponse.get("message");
			return message != null ? message.asText() : "";
		
	}
	
	
	public String removeDocument(String contextID, String documentID) throws Exception {		
		URIBuilder urlBuilder = new URIBuilder();
		urlBuilder.setScheme("http").setHost(serverAdress).setPath("/indexing");
		urlBuilder.setParameter("action", "removeDocument");
		urlBuilder.setParameter("context-id", contextID);
		urlBuilder.setParameter("document", documentID);
		
		ObjectNode queryResponse = Util.requete(urlBuilder.build(), "DELETE", null);

		if(queryResponse.get("error") != null){
			throw new Exception(queryResponse.get("error").asText());
		} 
		
		JsonNode message = queryResponse.get("message");
		return message != null ? message.asText() : "";
	}
	
}
