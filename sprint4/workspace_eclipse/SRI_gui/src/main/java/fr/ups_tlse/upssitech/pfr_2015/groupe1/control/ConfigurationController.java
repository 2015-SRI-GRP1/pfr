package fr.ups_tlse.upssitech.pfr_2015.groupe1.control;

import java.net.URISyntaxException;

import org.apache.http.client.HttpClient;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.impl.client.HttpClientBuilder;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.ups_tlse.upssitech.pfr_2015.groupe1.util.Util;

public class ConfigurationController {
	static HttpClient httpClient = HttpClientBuilder.create().build();
	static ObjectMapper mapper = new ObjectMapper();
	
	private String serverAdress;
	
	
	
	public ConfigurationController(String serverAdress) {
		this.serverAdress = serverAdress;
	}
	

	public String setParameter(String contextID, String key, String value) throws URISyntaxException, HttpHostConnectException {

		URIBuilder urlBuilder = new URIBuilder();
		urlBuilder.setScheme("http").setHost(serverAdress).setPath("/configuration");
		urlBuilder.setParameter("context-id", contextID);
		urlBuilder.setParameter("action", "set");
		urlBuilder.setParameter("key", key);
		urlBuilder.setParameter("value", value);
		
		
		ObjectNode queryResponse = Util.requete(urlBuilder.build(), "GET", null);
		
		if(queryResponse.get("error")!=null){
			System.out.println(queryResponse.get("error"));
		}

		JsonNode message = queryResponse.get("message");
		return message != null ? message.asText() : "";
	}
	public String getParameter(String contextID, String key) throws URISyntaxException, HttpHostConnectException {

		URIBuilder urlBuilder = new URIBuilder();
		urlBuilder.setScheme("http").setHost(serverAdress).setPath("/configuration");
		urlBuilder.setParameter("context-id", contextID);
		urlBuilder.setParameter("action", "get");
		urlBuilder.setParameter("key", key);
		
		
		ObjectNode queryResponse = Util.requete(urlBuilder.build(), "GET", null);
		
		if(queryResponse.get("error")!=null){
			System.out.println(queryResponse.get("error"));
		}

		JsonNode message = queryResponse.get("value");
		return message != null ? message.asText() : "";
	}
	
	
	public String removeParameter(String contextID, String key) throws URISyntaxException, HttpHostConnectException {

		URIBuilder urlBuilder = new URIBuilder();
		urlBuilder.setScheme("http").setHost(serverAdress).setPath("/configuration");
		urlBuilder.setParameter("context-id", contextID);
		urlBuilder.setParameter("action", "remove");
		urlBuilder.setParameter("key", key);
		
		
		ObjectNode queryResponse = Util.requete(urlBuilder.build(), "GET", null);
		
		if(queryResponse.get("error")!=null){
			System.out.println(queryResponse.get("error"));
		}

		JsonNode message = queryResponse.get("message");
		return message != null ? message.asText() : "";
	}
}
