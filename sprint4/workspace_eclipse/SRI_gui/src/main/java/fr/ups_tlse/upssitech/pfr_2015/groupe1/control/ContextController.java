package fr.ups_tlse.upssitech.pfr_2015.groupe1.control;

import java.net.URISyntaxException;

import org.apache.http.client.HttpClient;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.impl.client.HttpClientBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.ups_tlse.upssitech.pfr_2015.groupe1.util.Util;

public class ContextController {
	static HttpClient httpClient = HttpClientBuilder.create().build();
	static ObjectMapper mapper = new ObjectMapper();
	private String serverAdress;
	private String contextID;
	
	
	
	public ContextController(String serverAdress) throws URISyntaxException, HttpHostConnectException{
		this(serverAdress, null);
	}
	public ContextController(String serverAdress, String contextID) throws URISyntaxException, HttpHostConnectException{
		this.serverAdress = serverAdress;
		this.contextID = contextID == null ? this.creerContexte() : contextID;
	}
	
	public String getContextID(){
		return this.contextID;
	}
	
	
	public String creerContexte() throws URISyntaxException, HttpHostConnectException{
		URIBuilder createurAdresse = new URIBuilder();
		createurAdresse.setScheme("http").setHost(serverAdress).setPath("/context");
		createurAdresse.setParameter("action", "create");
		
		
		ObjectNode reponseCreationContexte = Util.requete(createurAdresse.build(), "GET", null);
		
		if(reponseCreationContexte.get("error")!=null){
			System.out.println(reponseCreationContexte.get("error"));
		}
		
		return reponseCreationContexte.get("context-id").asText();
	}
}
