package fr.ups_tlse.upssitech.pfr_2015.groupe1;

import java.net.URISyntaxException;

import org.apache.http.conn.HttpHostConnectException;

import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.ConfigurationController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.ContextController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.DocumentController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.IndexingController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.QueryingController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.graphicalView.MainWindow;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.textualView.TextView;

public class Launcher {
	//Attributs
	static String serverAdress;
	
	//Méthodes
	
	
	
	//Main
	public static void main(String args[]) throws URISyntaxException, HttpHostConnectException{

		serverAdress = System.getProperty("adress", "pfr.kloumpt.net") + ":" + System.getProperty("port", "8080");
		ContextController contextController = new ContextController(serverAdress);
		ConfigurationController configurationController = new ConfigurationController(serverAdress);
		IndexingController indexingController = new IndexingController(serverAdress);
		QueryingController queryingController = new QueryingController(serverAdress);
		DocumentController documentController = new DocumentController(serverAdress);
		
		String message;
		try {
			String indexPath = System.getProperty("index");
			if(indexPath != null){
				message = configurationController.setParameter(contextController.getContextID(), "index", indexPath);
				System.out.println(message);		
			}
			
		} catch (HttpHostConnectException e) {
			System.out.println("Erreur dans la connexion au serveur avec l'adresse " + serverAdress + "(" + e + ")");
			System.exit(1);
		}
		if(System.getProperty("useTextualView") != null) {
			TextView mainTextView = new TextView(contextController, configurationController, indexingController, queryingController, documentController);
			mainTextView.draw();
		} else {
			MainWindow view = new MainWindow(contextController, configurationController, indexingController, queryingController, documentController);
			view.setVisible(true);
		}
	}
}
