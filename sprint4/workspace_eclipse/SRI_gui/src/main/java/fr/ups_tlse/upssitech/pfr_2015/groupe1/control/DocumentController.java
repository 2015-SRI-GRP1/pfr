package fr.ups_tlse.upssitech.pfr_2015.groupe1.control;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.http.client.HttpClient;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.impl.client.HttpClientBuilder;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.Document;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.util.Util;

public class DocumentController {
	static HttpClient httpClient = HttpClientBuilder.create().build();
	static ObjectMapper mapper = new ObjectMapper();
	private String serverAdress;
	
	
	
	public DocumentController(String serverAdress){
		this.serverAdress = serverAdress;
	}
	

	public String getNameForID(String contextID, String id) throws URISyntaxException, HttpHostConnectException {

		URIBuilder urlBuilder = new URIBuilder();
		urlBuilder.setScheme("http").setHost(serverAdress).setPath("/documents");
		urlBuilder.setParameter("context-id", contextID);
		urlBuilder.setParameter("action", "get-name");
		urlBuilder.setParameter("document-id", id);
		
		
		ObjectNode queryResponse = Util.requete(urlBuilder.build(), "GET", null);
		
		if(queryResponse.get("error")!=null){
			System.out.println(queryResponse.get("error"));
		}

		JsonNode documentName = queryResponse.get("value");
		return documentName != null ? documentName.asText() : "";
	}
	
	public Document getDocumentForID(String contextID, String id) throws URISyntaxException, IOException {

		URIBuilder urlBuilder = new URIBuilder();
		urlBuilder.setScheme("http").setHost(serverAdress).setPath("/documents");
		urlBuilder.setParameter("context-id", contextID);
		urlBuilder.setParameter("action", "get-content");
		urlBuilder.setParameter("document-id", id);
		
		
		ObjectNode queryResponse = Util.requete(urlBuilder.build(), "GET", null);
		
		if(queryResponse.get("error")!=null){
			System.out.println(queryResponse.get("error"));
		}

		JsonNode data = queryResponse.get("document");
		return data != null ?  Document.fromJson(data) : null;
	}
}
