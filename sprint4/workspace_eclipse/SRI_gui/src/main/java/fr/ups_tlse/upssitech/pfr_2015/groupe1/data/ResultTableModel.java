package fr.ups_tlse.upssitech.pfr_2015.groupe1.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class ResultTableModel extends AbstractTableModel implements Observer{
	
	//Attributes
	private enum ResultsTableColumns{
		Rank("Rank", String.class, false),
		DocID("DocID", String.class, false),
		Score("Score", Double.class, false),
		Document("Document", String.class, false),
		Engine("Engine", String.class, false),
		Show("", String.class, true),
		SearchSimilar("", String.class, true);
		
		public final String name;
		public final Class<?> columnClass;
		public final boolean editable;
		
		ResultsTableColumns(String name, Class<?> columnClass, boolean editable){
			this.name = name;
			this.columnClass = columnClass;
			this.editable = editable;
		}
		
	}
	
	
	private List<DocumentScore> results;
	
	
	
	//Constructors
	public ResultTableModel() {
		this.results = new ArrayList<DocumentScore>();
		
		
	}
	

	
	//Methods
	@Override
	public int getColumnCount() {
		return ResultsTableColumns.values().length;
	}

	
	@Override
	public int getRowCount() {
		return this.results.size();
	}
	
	
	@Override
    public String getColumnName(int columnIndex) {
        return ResultsTableColumns.values()[columnIndex].name;
    }
	
	
	@Override
    public Class<?> getColumnClass(int columnIndex) {
        if (results.isEmpty()) {
            return Object.class;
        }
        return ResultsTableColumns.values()[columnIndex].columnClass;
    }

	
	@Override
	public Object getValueAt(int row, int col) {
		switch(ResultsTableColumns.values()[col]) {
		case Document:
			String documentName = this.results.get(row).getDocumentName();
			String baseFilename = documentName.substring(Math.max(documentName.lastIndexOf('/'), documentName.lastIndexOf('\\')) + 1);
			return baseFilename;
			
		case Engine:
			return this.results.get(row).getEngine();
			
		case Rank:
			return this.results.get(row).getRank();

		case Score:
			return this.results.get(row).getScore();

		case Show:
			return this.results.get(row).getDocumentID();
			
		case SearchSimilar:
			return this.results.get(row).getDocumentID();
			
		case DocID:
			return this.results.get(row).getDocumentID();
			
		default:
			break;

		}
		return null;
		
	}

	public boolean isCellEditable(int row, int col) {
		return ResultsTableColumns.values()[col].editable;
	}
	

	
	@Override
	public void update(Observable obsData, Object queryResults) {
		this.results.clear();
		if ((queryResults != null) && ((queryResults instanceof QueryResult))) {
			for(DocumentScore score : ((QueryResult)queryResults).getOrderedResults()){
				if(score.getScore() > 0 ){
					this.results.add(score);
				}
			}
		}
		this.fireTableDataChanged();
	}

}
