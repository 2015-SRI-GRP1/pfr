package fr.ups_tlse.upssitech.pfr_2015.groupe1.graphicalView;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableModel;

import fr.ups_tlse.upssitech.pfr_2015.groupe1.graphicalView.listeners.SearchListener;

@SuppressWarnings("serial")
public class SearchSimilarAction extends AbstractAction {
	private TableModel resultTableModel;


    private JTextField saisieTF;
	private SearchListener searchListener;


	public SearchSimilarAction(TableModel resultTableModel, SearchListener searchListener, JTextField saisieTF) {
		this.resultTableModel = resultTableModel;
		this.saisieTF = saisieTF;
		this.searchListener = searchListener;
	}


	public void actionPerformed(ActionEvent event){
        JTable table = (JTable)event.getSource();
        int rowIndex = Integer.valueOf( event.getActionCommand() );
        String documentID = (String) table.getModel().getValueAt(rowIndex, resultTableModel.getColumnCount()-2);
        saisieTF.setText("similar:" + documentID);
        
        searchListener.actionPerformed(new ActionEvent(table, ActionEvent.ACTION_PERFORMED, ""));
        
    }
}
