package fr.ups_tlse.upssitech.pfr_2015.groupe1.data;

import java.util.Observable;

import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.QueryResult;

public class QueryResultManager extends Observable{

	private QueryResult currentQueryResults = null;
	
	
	private static class ResultManagerHolder{
		private static QueryResultManager instance = new QueryResultManager();
	}
	
	
	public static synchronized QueryResultManager getInstance(){
		return ResultManagerHolder.instance;
	}
	
	
	private QueryResultManager(){
		super();
	}	
	
	
	public void setQueryResults(QueryResult queryResults){
		this.currentQueryResults = queryResults;
		this.setChanged();
		this.notifyObservers(queryResults);
	}
	
	
	public void removeQueryResults(){
		this.setQueryResults(null);
	}
	
	
	public QueryResult getCurrenResults(){
		return this.currentQueryResults;
	}
	
	
}