package fr.ups_tlse.upssitech.pfr_2015.groupe1.textualView;

import java.util.ArrayList;
import java.util.List;

import fr.ups_tlse.upssitech.pfr_2015.groupe1.util.Util;

public class MenuComposite extends MenuComponent {
	// Attributs
	List<MenuComponent> menu;
	String title;

	// Constructeur(s)
	public MenuComposite(String pTitle) {
		this.menu = new ArrayList<MenuComponent>();
		this.title = pTitle;
	}

	// Methodes
	public void addMenuComponent(MenuComponent menuComponent) {
		this.menu.add(menuComponent);
	}

	public void draw() {
		String choix;
		int numChoix;

		while (true) {
			Util.clearScreen();
			Util.println(Util.ANSI_PURPLE_BRIGHT, " - " + this.title + " :");

			for (int i = 0; i < this.menu.size(); i++) {
				System.out.println("\t" + (i + 1) + " - " + this.menu.get(i).getLabel());
			}

			Util.println(Util.ANSI_RED_BRIGHT, "\tq - Quitter");

			choix = TextView.keyboardScanner.nextLine();
			if(choix.toLowerCase().equals("q")){
				break;
			}
			
			
			numChoix = Integer.parseInt(choix) - 1;

			if (numChoix < this.menu.size()) {
				if (this.menu.get(numChoix) instanceof MenuLeaf) {
					((MenuLeaf) this.menu.get(numChoix)).execute();
				} else {
					this.menu.get(numChoix).draw();
				}
			}
		}

	}


	public String getLabel() {
		return this.title;
	}
}
