package fr.ups_tlse.upssitech.pfr_2015.groupe1.graphicalView;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class PlaceHolderManager implements DocumentListener, FocusListener{

	private JTextField textField;
	private String placeHolder;
	private boolean textEntered;
	private boolean focused;

	public PlaceHolderManager(JTextField textField, String placeHolder) {
		this.textField=textField;
		this.placeHolder = placeHolder;
		this.textEntered = textField.getText().isEmpty();
		
		updatePlaceHolder();
		
		textField.getDocument().addDocumentListener(this);
		textField.addFocusListener(this);
	}

    @Override
    public void insertUpdate(DocumentEvent e) {
    	updatePlaceHolder();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        updatePlaceHolder();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        updatePlaceHolder();
    }

    public void updatePlaceHolder() {
    	this.textEntered = !textField.getText().isEmpty();
    	this.focused = textField.isFocusOwner();
    	textField.setForeground(textEntered ? Color.WHITE : Color.LIGHT_GRAY);

    	if(this.textEntered || focused){
        	textField.setForeground(Color.WHITE);
    		
    	} else {
    		try{
    			textField.setText(focused ? "" : this.placeHolder);
    		}catch(IllegalStateException e){
    			// RAF car texte en cours d'insertion
    		}
        	textField.setForeground(Color.LIGHT_GRAY);
        	this.textEntered = false;
    	}
    }

    @Override
    public void focusGained(FocusEvent e) {
	    if(!this.textEntered){
	    	textField.setText("");
	    }
       updatePlaceHolder();

    }

    @Override
    public void focusLost(FocusEvent e) {
        updatePlaceHolder();
    }
    

}
