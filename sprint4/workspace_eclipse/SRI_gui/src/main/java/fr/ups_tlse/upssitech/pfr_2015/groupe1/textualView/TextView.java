package fr.ups_tlse.upssitech.pfr_2015.groupe1.textualView;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Scanner;

import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.ConfigurationController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.ContextController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.DocumentController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.IndexingController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.QueryingController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.data.QueryResult;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.util.Util;

public class TextView {
	
	public static final Scanner keyboardScanner  = new Scanner(System.in);
	
	private MenuComposite mainMenu;
	
	public TextView(final ContextController contextController, final ConfigurationController configurationController, final IndexingController indexingController, final QueryingController queryingController, final DocumentController documentController){
		this.mainMenu = new MenuComposite("Menu principal");

		buildQueryingMenu(contextController.getContextID(), queryingController, documentController);
		buildIndexingMenu(contextController.getContextID(), indexingController);
		buildAdministrationMenu(contextController.getContextID(), configurationController);
		
	}
	
	
	
	private void buildAdministrationMenu(String contextID, ConfigurationController configurationController) {
		MenuComposite  administrationMenu = new MenuComposite("Administration");
		
		this.mainMenu.addMenuComponent(administrationMenu);
	}



	private void buildIndexingMenu(final String contextID, final IndexingController indexingController) {
		MenuComposite indexingMenu = new MenuComposite("Indexation");

		MenuLeaf launchIndexation = new MenuLeaf("Lancer l'indexation", new Runnable() {
			@Override
			public void run() {				
				try {
					String message = indexingController.indexAll(contextID);
					System.out.println(message);
					
				} catch (URISyntaxException | IOException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
				Util.attendreUtilisateur("Appuyez sur une touche pour continuer...");
				
			}
			
		});
		MenuLeaf addDocToIndexingList =  new MenuLeaf("Ajouter un fichier à la liste des documents à indexer", new Runnable() {
			@Override
			public void run() {
				String filePath;
				
				System.out.println("Veuillez entrer le chemin vers le fichier à ajouter à la liste d'indexation : ");
				
				do {
					filePath = keyboardScanner.nextLine();
				} while ( filePath.isEmpty() );
				
				
				try {
					String message = indexingController.ajoutDocument(contextID, filePath);
					System.out.println(message);
					
				} catch (URISyntaxException | IOException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
		});
		
		
		MenuLeaf addDocToIndex =  new MenuLeaf("Indexer un fichier", new Runnable() {
			@Override
			public void run() {
				String filePath;
				
				System.out.println("Veuillez entrer le chemin vers le fichier à indexer : ");
				
				do {
					filePath = keyboardScanner.nextLine();
				} while ( filePath.isEmpty() );
				
				
				try {
					String message = indexingController.indexationSimple(contextID, filePath);
					System.out.println(message);
					
				} catch (URISyntaxException | IOException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
		});

		indexingMenu.addMenuComponent(launchIndexation);
		indexingMenu.addMenuComponent(addDocToIndexingList);
		indexingMenu.addMenuComponent(addDocToIndex);

		this.mainMenu.addMenuComponent(indexingMenu);
	}



	private void buildQueryingMenu(final String contextID, final QueryingController queryingController, final DocumentController documentController) {
		MenuComposite searchMenu = new MenuComposite("Recherche");

		MenuLeaf exampleSearch = new MenuLeaf("Recherche par exemple", new Runnable() {
			@Override
			public void run() {
				String filePath;
				QueryResult result;
				
				System.out.println("Veuillez entrer le chemin vers le fichier d'exemple : ");
				
				do {
					filePath = keyboardScanner.nextLine();
				} while ( filePath.isEmpty() );
				
				
				try {
					result = queryingController.singleSearchByExample(contextID, "0", filePath);

					if(result == null){
						Util.attendreUtilisateur("Pas de resultat, appuyez sur une touche pour continuer...");
					}else{
						Util.showQueryResult(contextID, documentController, result);
					}
				} catch (URISyntaxException | IOException e) {
					e.printStackTrace();
				}
				
			}
			
		});
		
		MenuComposite  criteriaSearch = new MenuComposite("Recherche par critere");
		
		
		MenuLeaf criteriaSearchImage = new MenuLeaf("Recherche d'image par critere", new Runnable() {
			@Override
			public void run() {
				String criteria;
				QueryResult result;

				System.out.println("Veuillez entrer une couleur pour la recherche d'images : ");
				do {
					criteria = keyboardScanner.nextLine();
				} while ( criteria.isEmpty() );
				
				try {
					result = queryingController.singleImageSearchByCriteria(contextID, "0", criteria);

					if(result == null){
						Util.attendreUtilisateur("Pas de resultat, appuyez sur une touche pour continuer...");
					}else{
						Util.showQueryResult(contextID, documentController, result);
					}
				} catch (URISyntaxException | IOException e) {
					e.printStackTrace();
				}
				
			}
			
		});
		
		
		MenuLeaf criteriaSearchText = new MenuLeaf("Recherche de texte par critere", new Runnable() {
			@Override
			public void run() {
				String criteria;
				QueryResult result;

				System.out.println("Veuillez entrer des mots pour la recherche de textes : ");
				do {
					criteria = keyboardScanner.nextLine();
				} while ( criteria.isEmpty() );
				
				try {
					result = queryingController.singleTextSearchByCriteria(contextID, "0", criteria);
					
					if(result == null){
						Util.attendreUtilisateur("Pas de resultat, appuyez sur une touche pour continuer...");
					}else{
						Util.showQueryResult(contextID, documentController, result);
					}
					
				} catch (URISyntaxException | IOException e) {
					e.printStackTrace();
				}
				
			}
			
		});
		
		criteriaSearch.addMenuComponent(criteriaSearchImage);
		criteriaSearch.addMenuComponent(criteriaSearchText);
		

		searchMenu.addMenuComponent(exampleSearch);
		searchMenu.addMenuComponent(criteriaSearch);
		

		this.mainMenu.addMenuComponent(searchMenu);
	}


	public void draw(){
		this.mainMenu.draw();
	}
	
}
