package fr.ups_tlse.upssitech.pfr_2015.groupe1.graphicalView;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.net.URISyntaxException;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.http.conn.HttpHostConnectException;

import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.ConfigurationController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.ContextController;

@SuppressWarnings("serial")
public class SearchOptionsPanel extends JPanel implements ItemListener {

	private JLabel choix;
	private JCheckBox selectionSRIrs;
	private JCheckBox selectionSRIrsAlt;
	private JCheckBox selectionAskAgnan;
	private ContextController contextController;
	private ConfigurationController configurationController;

	public SearchOptionsPanel(ContextController contextController, ConfigurationController configurationController) {
		super(new FlowLayout(FlowLayout.CENTER));
		this.contextController = contextController;
		this.configurationController = configurationController;
		
		choix = new JLabel(new String("Choix Moteur: "));
		choix.setForeground(Color.LIGHT_GRAY);
		selectionSRIrs = new JCheckBox(new String("sri_rs"));
		selectionSRIrs.setForeground(Color.WHITE);
		selectionSRIrs.setBackground(Color.DARK_GRAY);
		selectionSRIrs.addItemListener(this);
		
		selectionSRIrsAlt = new JCheckBox(new String("sri_rs_alt"));
		selectionSRIrsAlt.setForeground(Color.WHITE);
		selectionSRIrsAlt.setBackground(Color.DARK_GRAY);
		selectionSRIrsAlt.addItemListener(this);
		
		selectionAskAgnan = new JCheckBox(new String("AskAgnan"));
		selectionAskAgnan.setForeground(Color.WHITE);
		selectionAskAgnan.setBackground(Color.DARK_GRAY);
		selectionAskAgnan.addItemListener(this);
		selectionAskAgnan.setEnabled(false);
		

		this.add(choix);
		this.add(selectionSRIrs);
		this.add(selectionSRIrsAlt);
		this.add(selectionAskAgnan);
		this.setBackground(Color.BLACK);
		
		selectionSRIrs.setSelected(true);
		/*try {
			configurationController.setParameter(contextController.getContextID(), "use_sri_rs", "1");
		} catch (HttpHostConnectException | URISyntaxException e) {
			e.printStackTrace();
		}*/
	}

	@Override
	public void itemStateChanged(ItemEvent event) {
		String engingeName = ((JCheckBox)event.getSource()).getText();

		try {
			configurationController.setParameter(contextController.getContextID(), "use_"+engingeName, ((JCheckBox)event.getSource()).isSelected() ? "1" : "0");
		} catch (HttpHostConnectException | URISyntaxException e) {
			e.printStackTrace();
		}
		
	}

}
