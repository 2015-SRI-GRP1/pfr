package fr.ups_tlse.upssitech.pfr_2015.groupe1.textualView;

public class MenuLeaf extends MenuComponent {
	//Attributs
	Runnable action;
	String title;
	
	//Constructeur(s)
	public MenuLeaf(String string, Runnable actionP){
		this.title = string;
		this.action = actionP;
	}
	public MenuLeaf(String string){
		this.title = string;
	}
	
	//Methodes

	public void setAction(Runnable actionP) {
		this.action = actionP;
	}

	public String getText() {
		return title;
	}

	public void setText(String text) {
		this.title = text;
	}
	public void draw() {
		System.out.println(this.title);
	}
	
	public String getLabel(){
		return this.title;
	}
	public void execute() {
		this.action.run();
	}
}
