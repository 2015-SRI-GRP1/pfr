package fr.ups_tlse.upssitech.pfr_2015.groupe1.graphicalView;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URISyntaxException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import org.apache.http.conn.HttpHostConnectException;

import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.ConfigurationController;
import fr.ups_tlse.upssitech.pfr_2015.groupe1.control.IndexingController;

@SuppressWarnings("serial")
public class AdministrationDialog extends JDialog implements ActionListener{
	
	//Attributs
	
	public ConfigurationController configurationController;
	
	public IndexingController indexingController;
	
	private String contextID;
	
	
	private JPanel optionsPanel;
	
	
	private JButton fileChooserButton;
	private JTextField filenameField;

	private JLabel labelSeuil;
	private JTextField fieldSeuil;
	
	private JLabel labelIndex;
	private JTextField fieldIndex;

	private JLabel labelTailleF;
	private JTextField fieldTailleF;
	
	private JLabel labelTailleM;
	private JTextField fieldTailleM;
	
	private JLabel labelMotsMax;
	private JTextField fieldMotsMax;
	
	
	private JButton indexingButton;
	private JButton boutonOk;
	private JButton boutonAnnuler;

	private JPanel indexingPanel;

	private JPanel buttonsPanel;
	
	//Constructeur
	
	public AdministrationDialog(ConfigurationController configurationController, IndexingController indexingController, String contextID){
		super();
	
		this.configurationController = configurationController;
		this.indexingController = indexingController;
		this.contextID = contextID;
		
		this.setTitle("Options administrateur");
		this.setMinimumSize(new Dimension(480,  320));
		this.build();
		this.setResizable(true);
		
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		

		try {
			this.fieldIndex.setText(this.configurationController.getParameter(this.contextID, "index"));
		} catch (HttpHostConnectException | URISyntaxException e1) {
			e1.printStackTrace();
		}
		try {
			this.fieldSeuil.setText(this.configurationController.getParameter(this.contextID, "window_levels"));
		} catch (HttpHostConnectException | URISyntaxException e1) {
			e1.printStackTrace();
		}
		try {
			this.fieldTailleF.setText(this.configurationController.getParameter(this.contextID, "window_size"));
		} catch (HttpHostConnectException | URISyntaxException e1) {
			e1.printStackTrace();
		}
		try {
			this.fieldTailleM.setText(this.configurationController.getParameter(this.contextID, "min_word_size"));
		} catch (HttpHostConnectException | URISyntaxException e1) {
			e1.printStackTrace();
		}
		try {
			this.fieldMotsMax.setText(this.configurationController.getParameter(this.contextID, "max_word_per_text"));
		} catch (HttpHostConnectException | URISyntaxException e1) {
			e1.printStackTrace();
		}
		
		
		
	}
	
	
	//Methodes
	private void build(){
		indexingPanel = new JPanel(new GridLayout(1, 0));

		fileChooserButton = new JButton("Choose a document");
		fileChooserButton.setOpaque(true);
		fileChooserButton.setBackground(Color.DARK_GRAY);
		fileChooserButton.setForeground(Color.WHITE);
		
		
		filenameField = new JTextField();
		filenameField.setForeground(Color.WHITE);
		filenameField.setBackground(Color.BLACK);
		filenameField.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.GRAY, 1), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		filenameField.setCaretColor(Color.WHITE);
		
		indexingButton = new JButton("Index document");
		indexingButton.setBackground(Color.DARK_GRAY);
		indexingButton.setForeground(Color.WHITE);
		
		{
			final AdministrationDialog mainWindowInstance = this;
			fileChooserButton.addActionListener(new ActionListener() {
				JFileChooser fileChooser = new JFileChooser();
				public void actionPerformed(ActionEvent e) {
					if(fileChooser.showOpenDialog(mainWindowInstance) == JFileChooser.APPROVE_OPTION){
						filenameField.setText("file://" + fileChooser.getSelectedFile().getAbsolutePath());
					}
				}
			});
		}
		
		indexingButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String indexingResponse = indexingController.indexationSimple(contextID, filenameField.getText().replace("file://", ""));
					JOptionPane.showMessageDialog(indexingButton, indexingResponse);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		
		indexingPanel.add(fileChooserButton);
		indexingPanel.add(filenameField);
		indexingPanel.add(indexingButton);
		
		
		optionsPanel = new JPanel();
		optionsPanel.setLayout(new GridLayout(0, 2));
		


		
		

		labelIndex= new JLabel("index");
		fieldIndex = new JTextField();
		optionsPanel.add(labelIndex);
		optionsPanel.add(fieldIndex);
		
		
		labelSeuil = new JLabel("window_levels");
		fieldSeuil = new JTextField();
		optionsPanel.add(labelSeuil);
		optionsPanel.add(fieldSeuil);
		

		labelTailleF = new JLabel("window_size");
		fieldTailleF = new JTextField();
		optionsPanel.add(labelTailleF);
		optionsPanel.add(fieldTailleF);

		
		
		//Creation et ajout du JLabel taille mots
		labelTailleM = new JLabel("min_word_size");
		fieldTailleM = new JTextField();
		optionsPanel.add(labelTailleM);
		optionsPanel.add(fieldTailleM);
	
		
		
		//Creation et ajout du JLabel motsmax
		labelMotsMax = new JLabel("max_word_per_text");
		fieldMotsMax = new JTextField();
		optionsPanel.add(labelMotsMax);
		optionsPanel.add(fieldMotsMax);

		
		
		

		buttonsPanel = new JPanel(new GridLayout(1, 0));

		boutonOk = new JButton("Ok");
		boutonOk.setBackground(Color.DARK_GRAY);
		boutonOk.setForeground(Color.WHITE);
		boutonOk.addActionListener(this);
		
		boutonAnnuler = new JButton("Close");
		boutonAnnuler.setBackground(Color.DARK_GRAY);
		boutonAnnuler.setForeground(Color.WHITE);
		boutonAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
			}
		});

		buttonsPanel.add(boutonAnnuler);
		buttonsPanel.add(boutonOk);
		
		JPanel optionsWrapper = new JPanel();
		optionsWrapper.setLayout(new BorderLayout());
		optionsWrapper.add(optionsPanel, BorderLayout.NORTH);
		
		this.setLayout(new BorderLayout());
		this.add(indexingPanel, BorderLayout.NORTH);
		this.add(new JScrollPane(optionsWrapper), BorderLayout.CENTER);
		this.add(buttonsPanel, BorderLayout.SOUTH);
		
		//Affichage du JPanel
		this.pack();
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		
	}
	
	
	public void actionPerformed(ActionEvent e){
		Object source = e.getSource();
		
		if(source == boutonOk){

			try {
				this.configurationController.setParameter(this.contextID, "index", this.fieldIndex.getText());
			} catch (HttpHostConnectException | URISyntaxException e1) {
				e1.printStackTrace();
			}
			try {
				this.configurationController.setParameter(this.contextID, "window_levels", this.fieldSeuil.getText());
			} catch (HttpHostConnectException | URISyntaxException e1) {
				e1.printStackTrace();
			}
			try {
				this.configurationController.setParameter(this.contextID, "window_size", this.fieldTailleF.getText());
			} catch (HttpHostConnectException | URISyntaxException e1) {
				e1.printStackTrace();
			}
			try {
				this.configurationController.setParameter(this.contextID, "min_word_size", this.fieldTailleM.getText());
			} catch (HttpHostConnectException | URISyntaxException e1) {
				e1.printStackTrace();
			}
			try {
				this.configurationController.setParameter(this.contextID, "max_word_per_text", this.fieldMotsMax.getText());
			} catch (HttpHostConnectException | URISyntaxException e1) {
				e1.printStackTrace();
			}
			
			this.setVisible(false);
			dispose();
		}
	}
}
