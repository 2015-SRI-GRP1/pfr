#!/bin/bash
CWD=`pwd`

PFR_PATH="CWD"
PFR_PATH="/var/pfr/"

if ! mkdir -p "$PFR_PATH/" ; then
	echo "-------------------------------------------------------------------------------------"
	echo "Superuser rights needed to create $PFR_PATH, may be asked to login through sudo"
	if sudo mkdir -p "$PFR_PATH/" ; then
		sudo chmod 0777 "$PFR_PATH"
		sudo chown "$USER" "$PFR_PATH"
		sudo chgrp `id -g -n $USER` "$PFR_PATH"
		find /var/pfr/ -type d -not -path "$PFR_PATH" -exec chmod 777 {} +
		find /var/pfr/ -type f -exec chmod 666 {} +
	else 
		echo "Can't continue without superuser rights"
		exit 1;
	fi
	echo "$PFR_PATH created"
        echo "-------------------------------------------------------------------------------------"

fi

mkdir -p "$PFR_PATH/indexes"
mkdir -p "$PFR_PATH/tmp"
mkdir -p "$PFR_PATH/tmp/config"
mkdir -p "$PFR_PATH/tmp/queries"


java \
	-DsriGroupe1IndexingProgram="$CWD/sri_rs/indexing" \
	-DsriGroupe1QueryingProgram="$CWD/sri_rs/querying" \
	-DpfrPath="$PFR_PATH" \
	-jar sri_driver/sri_driver.jar

